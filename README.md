# excel2json

#### 介绍
excel转换json作为各类开发工具配置表。现支持Java, Laya(Typescript)。将来支持Unity，Egret，Python等.. 

适合类似游戏项目，多端数据表一致，数据表需要给Excel配置进行频繁更新更改的使用。

#### 主要功能

- 软件端生成器excel2json.py（Python代码，pyinstall编译后可独立当软件使用)
- 导入excel文件导出json文件，供读取端使用。
- 根据excel配置导出配置架构json文件，供读取端使用。
- 生成读取端用的实体类，快捷读取类。
- 读取端以最简便方式获得数据。(Demo中有Java读取完整框架代码)


##### 版本计划(按优先度从上至下排列)：
1. 表变化关联表自动重新加载 (已完成)

1. 导出关联字段有效值检测
1. 软件端一键发布数据表至svn或git。
1. 软件端用传输包发布至java后端
1. 后端通知前端重新加载
1. Unity读取端实现



#### 软件架构

转换软件:

- Python 3.6
- Pyside2 + Qt Designer 制作窗体
- xlrd 读取Excel文档

Java读取框架：

- JDK 8
- Maven2或3
- fastjson 1.2x
- spring-core 5.2.9.RELEASE(主要生成md5用)
- slf4j-api 1.7.36 (日志用)
    
#### 安装教程

##### 软件端生成器:
Pyhton下使用pip3安装

PS:xlrd1.2.0之后的版本读取不了.xlsx  
```
pip install xlrd==1.2.0
```
PS: pyside2在3.x下要用pip3才能顺利安装
```
pip3 install -U PySide2 -i  http://mirrors.aliyun.com/pypi/simple --trusted-host mirrors.aliyun.com
```
安装后，在python的安装目录下的插件目录那里，进入到%PYTHON_HOME%\Lib\site-packages\PySide2目录下后，会有一个designer.exe的文件，双击打开后，就可以图形化页面的设计工具，可以进行页面拖拽设计

```
pip install pyinstaller
```
如果提示pip版本旧需要升级pip：
```
python -m pip install --upgrade pip
```
成功安装后执行根目录“_pyinstall_build.bat”即可打包成exe文件(exe文件放在"dist/"目录下)

#### 使用说明



#### 参与贡献

Seraph.Yang

