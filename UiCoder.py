# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'coder.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_CoderDialog(object):
    def setupUi(self, CoderDialog):
        if not CoderDialog.objectName():
            CoderDialog.setObjectName(u"CoderDialog")
        CoderDialog.setWindowModality(Qt.WindowModal)
        CoderDialog.resize(1000, 497)
        self.verticalLayout_4 = QVBoxLayout(CoderDialog)
        self.verticalLayout_4.setObjectName(u"verticalLayout_4")
        self.groupBox = QGroupBox(CoderDialog)
        self.groupBox.setObjectName(u"groupBox")
        font = QFont()
        font.setFamily(u"\u5fae\u8f6f\u96c5\u9ed1")
        self.groupBox.setFont(font)
        self.verticalLayout_2 = QVBoxLayout(self.groupBox)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.label_3 = QLabel(self.groupBox)
        self.label_3.setObjectName(u"label_3")
        font1 = QFont()
        font1.setFamily(u"\u5fae\u8f6f\u96c5\u9ed1")
        font1.setPointSize(10)
        font1.setBold(True)
        font1.setWeight(75)
        self.label_3.setFont(font1)
        self.label_3.setAlignment(Qt.AlignCenter)

        self.verticalLayout_2.addWidget(self.label_3)

        self.gridLayout_2 = QGridLayout()
        self.gridLayout_2.setObjectName(u"gridLayout_2")
        self.txtDirJavaFile = QLineEdit(self.groupBox)
        self.txtDirJavaFile.setObjectName(u"txtDirJavaFile")
        self.txtDirJavaFile.setFont(font)
        self.txtDirJavaFile.setReadOnly(True)

        self.gridLayout_2.addWidget(self.txtDirJavaFile, 0, 1, 1, 1)

        self.btnSelectDirJavaFile = QPushButton(self.groupBox)
        self.btnSelectDirJavaFile.setObjectName(u"btnSelectDirJavaFile")
        self.btnSelectDirJavaFile.setMaximumSize(QSize(75, 16777215))
        self.btnSelectDirJavaFile.setFont(font)

        self.gridLayout_2.addWidget(self.btnSelectDirJavaFile, 0, 4, 1, 1)

        self.txtDirJavaFileCopy = QLineEdit(self.groupBox)
        self.txtDirJavaFileCopy.setObjectName(u"txtDirJavaFileCopy")
        self.txtDirJavaFileCopy.setFont(font)

        self.gridLayout_2.addWidget(self.txtDirJavaFileCopy, 1, 1, 1, 1)

        self.label_4 = QLabel(self.groupBox)
        self.label_4.setObjectName(u"label_4")
        self.label_4.setMinimumSize(QSize(120, 0))
        self.label_4.setFont(font)
        self.label_4.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout_2.addWidget(self.label_4, 1, 0, 1, 1)

        self.label = QLabel(self.groupBox)
        self.label.setObjectName(u"label")
        self.label.setMinimumSize(QSize(120, 0))
        self.label.setFont(font)
        self.label.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout_2.addWidget(self.label, 0, 0, 1, 1)

        self.btnSelectDirJavaFileCopy = QPushButton(self.groupBox)
        self.btnSelectDirJavaFileCopy.setObjectName(u"btnSelectDirJavaFileCopy")
        self.btnSelectDirJavaFileCopy.setMaximumSize(QSize(75, 16777215))
        self.btnSelectDirJavaFileCopy.setFont(font)

        self.gridLayout_2.addWidget(self.btnSelectDirJavaFileCopy, 1, 4, 1, 1)

        self.btnOpenDirJavaFileCopy = QPushButton(self.groupBox)
        self.btnOpenDirJavaFileCopy.setObjectName(u"btnOpenDirJavaFileCopy")
        self.btnOpenDirJavaFileCopy.setMaximumSize(QSize(75, 16777215))
        self.btnOpenDirJavaFileCopy.setFont(font)

        self.gridLayout_2.addWidget(self.btnOpenDirJavaFileCopy, 1, 3, 1, 1)

        self.btnOpenDirJavaFile = QPushButton(self.groupBox)
        self.btnOpenDirJavaFile.setObjectName(u"btnOpenDirJavaFile")
        self.btnOpenDirJavaFile.setMaximumSize(QSize(75, 16777215))
        self.btnOpenDirJavaFile.setSizeIncrement(QSize(0, 0))
        self.btnOpenDirJavaFile.setFont(font)

        self.gridLayout_2.addWidget(self.btnOpenDirJavaFile, 0, 3, 1, 1)


        self.verticalLayout_2.addLayout(self.gridLayout_2)

        self.label_8 = QLabel(self.groupBox)
        self.label_8.setObjectName(u"label_8")
        self.label_8.setFont(font1)
        self.label_8.setAlignment(Qt.AlignCenter)

        self.verticalLayout_2.addWidget(self.label_8)

        self.gridLayout = QGridLayout()
        self.gridLayout.setObjectName(u"gridLayout")
        self.txtDirLayaFileCopy = QLineEdit(self.groupBox)
        self.txtDirLayaFileCopy.setObjectName(u"txtDirLayaFileCopy")
        self.txtDirLayaFileCopy.setFont(font)

        self.gridLayout.addWidget(self.txtDirLayaFileCopy, 1, 1, 1, 1)

        self.btnSelectDirLayaFileCopy = QPushButton(self.groupBox)
        self.btnSelectDirLayaFileCopy.setObjectName(u"btnSelectDirLayaFileCopy")
        self.btnSelectDirLayaFileCopy.setMaximumSize(QSize(75, 16777215))
        self.btnSelectDirLayaFileCopy.setFont(font)

        self.gridLayout.addWidget(self.btnSelectDirLayaFileCopy, 1, 3, 1, 1)

        self.label_2 = QLabel(self.groupBox)
        self.label_2.setObjectName(u"label_2")
        self.label_2.setMinimumSize(QSize(120, 0))
        self.label_2.setFont(font)
        self.label_2.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.label_2, 0, 0, 1, 1)

        self.btnOpenDirLayaFileCopy = QPushButton(self.groupBox)
        self.btnOpenDirLayaFileCopy.setObjectName(u"btnOpenDirLayaFileCopy")
        self.btnOpenDirLayaFileCopy.setMaximumSize(QSize(75, 16777215))
        self.btnOpenDirLayaFileCopy.setFont(font)

        self.gridLayout.addWidget(self.btnOpenDirLayaFileCopy, 1, 2, 1, 1)

        self.txtDirLayaFile = QLineEdit(self.groupBox)
        self.txtDirLayaFile.setObjectName(u"txtDirLayaFile")
        self.txtDirLayaFile.setFont(font)
        self.txtDirLayaFile.setReadOnly(True)

        self.gridLayout.addWidget(self.txtDirLayaFile, 0, 1, 1, 1)

        self.label_5 = QLabel(self.groupBox)
        self.label_5.setObjectName(u"label_5")
        self.label_5.setMinimumSize(QSize(120, 0))
        self.label_5.setFont(font)
        self.label_5.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.label_5, 1, 0, 1, 1)

        self.btnSelectDirLayaFile = QPushButton(self.groupBox)
        self.btnSelectDirLayaFile.setObjectName(u"btnSelectDirLayaFile")
        self.btnSelectDirLayaFile.setMaximumSize(QSize(75, 16777215))
        self.btnSelectDirLayaFile.setFont(font)

        self.gridLayout.addWidget(self.btnSelectDirLayaFile, 0, 3, 1, 1)

        self.btnOpenDirLayaFile = QPushButton(self.groupBox)
        self.btnOpenDirLayaFile.setObjectName(u"btnOpenDirLayaFile")
        self.btnOpenDirLayaFile.setMaximumSize(QSize(75, 16777215))
        self.btnOpenDirLayaFile.setFont(font)

        self.gridLayout.addWidget(self.btnOpenDirLayaFile, 0, 2, 1, 1)


        self.verticalLayout_2.addLayout(self.gridLayout)

        self.verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_2.addItem(self.verticalSpacer)


        self.verticalLayout_4.addWidget(self.groupBox)

        self.horizontalLayout_3 = QHBoxLayout()
        self.horizontalLayout_3.setObjectName(u"horizontalLayout_3")
        self.horizontalLayout_3.setContentsMargins(0, 0, -1, -1)
        self.groupBox_2 = QGroupBox(CoderDialog)
        self.groupBox_2.setObjectName(u"groupBox_2")
        self.groupBox_2.setMinimumSize(QSize(0, 120))
        self.groupBox_2.setFont(font)
        self.verticalLayout = QVBoxLayout(self.groupBox_2)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.horizontalLayout_5 = QHBoxLayout()
        self.horizontalLayout_5.setObjectName(u"horizontalLayout_5")
        self.chkGenSvn = QCheckBox(self.groupBox_2)
        self.chkGenSvn.setObjectName(u"chkGenSvn")
        self.chkGenSvn.setFont(font)

        self.horizontalLayout_5.addWidget(self.chkGenSvn)

        self.chkDelOld = QCheckBox(self.groupBox_2)
        self.chkDelOld.setObjectName(u"chkDelOld")
        self.chkDelOld.setFont(font)

        self.horizontalLayout_5.addWidget(self.chkDelOld)

        self.chkGenCopy = QCheckBox(self.groupBox_2)
        self.chkGenCopy.setObjectName(u"chkGenCopy")
        self.chkGenCopy.setFont(font)

        self.horizontalLayout_5.addWidget(self.chkGenCopy)


        self.verticalLayout.addLayout(self.horizontalLayout_5)

        self.horizontalLayout_6 = QHBoxLayout()
        self.horizontalLayout_6.setObjectName(u"horizontalLayout_6")
        self.btnGenJava = QPushButton(self.groupBox_2)
        self.btnGenJava.setObjectName(u"btnGenJava")
        self.btnGenJava.setMaximumSize(QSize(250, 16777215))
        font2 = QFont()
        font2.setFamily(u"\u5fae\u8f6f\u96c5\u9ed1")
        font2.setBold(True)
        font2.setWeight(75)
        self.btnGenJava.setFont(font2)

        self.horizontalLayout_6.addWidget(self.btnGenJava)

        self.btnGenLaya = QPushButton(self.groupBox_2)
        self.btnGenLaya.setObjectName(u"btnGenLaya")
        self.btnGenLaya.setFont(font2)

        self.horizontalLayout_6.addWidget(self.btnGenLaya)


        self.verticalLayout.addLayout(self.horizontalLayout_6)


        self.horizontalLayout_3.addWidget(self.groupBox_2)

        self.groupBox_3 = QGroupBox(CoderDialog)
        self.groupBox_3.setObjectName(u"groupBox_3")
        self.groupBox_3.setFont(font)
        self.verticalLayout_5 = QVBoxLayout(self.groupBox_3)
        self.verticalLayout_5.setObjectName(u"verticalLayout_5")
        self.horizontalLayout_4 = QHBoxLayout()
        self.horizontalLayout_4.setObjectName(u"horizontalLayout_4")
        self.chkUpdateCopy = QCheckBox(self.groupBox_3)
        self.chkUpdateCopy.setObjectName(u"chkUpdateCopy")
        self.chkUpdateCopy.setFont(font)

        self.horizontalLayout_4.addWidget(self.chkUpdateCopy)


        self.verticalLayout_5.addLayout(self.horizontalLayout_4)

        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.btnUpdateJava = QPushButton(self.groupBox_3)
        self.btnUpdateJava.setObjectName(u"btnUpdateJava")
        self.btnUpdateJava.setFont(font2)

        self.horizontalLayout.addWidget(self.btnUpdateJava)

        self.btnUpdateLaya = QPushButton(self.groupBox_3)
        self.btnUpdateLaya.setObjectName(u"btnUpdateLaya")
        self.btnUpdateLaya.setFont(font2)

        self.horizontalLayout.addWidget(self.btnUpdateLaya)


        self.verticalLayout_5.addLayout(self.horizontalLayout)


        self.horizontalLayout_3.addWidget(self.groupBox_3)


        self.verticalLayout_4.addLayout(self.horizontalLayout_3)

        self.widget = QWidget(CoderDialog)
        self.widget.setObjectName(u"widget")
        sizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.widget.sizePolicy().hasHeightForWidth())
        self.widget.setSizePolicy(sizePolicy)
        self.widget.setMinimumSize(QSize(0, 50))
        self.widget.setFont(font)
        self.horizontalLayout_2 = QHBoxLayout(self.widget)
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.chkClearDirGen = QCheckBox(self.widget)
        self.chkClearDirGen.setObjectName(u"chkClearDirGen")
        self.chkClearDirGen.setFont(font)

        self.horizontalLayout_2.addWidget(self.chkClearDirGen)

        self.chkClearDirCopy = QCheckBox(self.widget)
        self.chkClearDirCopy.setObjectName(u"chkClearDirCopy")
        self.chkClearDirCopy.setFont(font)

        self.horizontalLayout_2.addWidget(self.chkClearDirCopy)

        self.chkAutoOpenFolderGenCode = QCheckBox(self.widget)
        self.chkAutoOpenFolderGenCode.setObjectName(u"chkAutoOpenFolderGenCode")
        self.chkAutoOpenFolderGenCode.setFont(font)

        self.horizontalLayout_2.addWidget(self.chkAutoOpenFolderGenCode)

        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_2.addItem(self.horizontalSpacer)

        self.btnClose = QPushButton(self.widget)
        self.btnClose.setObjectName(u"btnClose")
        self.btnClose.setFont(font)

        self.horizontalLayout_2.addWidget(self.btnClose)


        self.verticalLayout_4.addWidget(self.widget)


        self.retranslateUi(CoderDialog)

        QMetaObject.connectSlotsByName(CoderDialog)
    # setupUi

    def retranslateUi(self, CoderDialog):
        CoderDialog.setWindowTitle(QCoreApplication.translate("CoderDialog", u"\u4ee3\u7801\u751f\u6210", None))
        self.groupBox.setTitle(QCoreApplication.translate("CoderDialog", u"\u751f\u6210\u4ee3\u7801\u914d\u7f6e", None))
        self.label_3.setText(QCoreApplication.translate("CoderDialog", u"JAVA\u76f8\u5173\u914d\u7f6e", None))
        self.btnSelectDirJavaFile.setText(QCoreApplication.translate("CoderDialog", u"\u9009\u62e9\u76ee\u5f55", None))
        self.label_4.setText(QCoreApplication.translate("CoderDialog", u"\u540c\u6b65\u62f7\u8d1d\u76ee\u5f55\uff1a", None))
        self.label.setText(QCoreApplication.translate("CoderDialog", u"\u4ee3\u7801\u751f\u6210\u76ee\u5f55\uff1a", None))
        self.btnSelectDirJavaFileCopy.setText(QCoreApplication.translate("CoderDialog", u"\u9009\u62e9\u76ee\u5f55", None))
        self.btnOpenDirJavaFileCopy.setText(QCoreApplication.translate("CoderDialog", u"\u6253\u5f00\u76ee\u5f55", None))
        self.btnOpenDirJavaFile.setText(QCoreApplication.translate("CoderDialog", u"\u6253\u5f00\u76ee\u5f55", None))
        self.label_8.setText(QCoreApplication.translate("CoderDialog", u"Laya\u76ee\u5f55\u914d\u7f6e", None))
        self.btnSelectDirLayaFileCopy.setText(QCoreApplication.translate("CoderDialog", u"\u9009\u62e9\u76ee\u5f55", None))
        self.label_2.setText(QCoreApplication.translate("CoderDialog", u"\u4ee3\u7801\u751f\u6210\u76ee\u5f55\uff1a", None))
        self.btnOpenDirLayaFileCopy.setText(QCoreApplication.translate("CoderDialog", u"\u6253\u5f00\u76ee\u5f55", None))
        self.label_5.setText(QCoreApplication.translate("CoderDialog", u"\u540c\u6b65\u62f7\u8d1d\u76ee\u5f55\uff1a", None))
        self.btnSelectDirLayaFile.setText(QCoreApplication.translate("CoderDialog", u"\u9009\u62e9\u76ee\u5f55", None))
        self.btnOpenDirLayaFile.setText(QCoreApplication.translate("CoderDialog", u"\u6253\u5f00\u76ee\u5f55", None))
        self.groupBox_2.setTitle(QCoreApplication.translate("CoderDialog", u"\u3010\u751f\u6210\u4ee3\u7801\u3011->\u3010\u63d0\u4ea4SVN\u3011", None))
        self.chkGenSvn.setText(QCoreApplication.translate("CoderDialog", u"\u540c\u6b65\u63d0\u4ea4SVN", None))
        self.chkDelOld.setText(QCoreApplication.translate("CoderDialog", u"SVN\u5220\u9664\u65e7\u7c7b", None))
        self.chkGenCopy.setText(QCoreApplication.translate("CoderDialog", u"\u751f\u6210\u540e\u62f7\u8d1d\u81f3\u540c\u6b65\u4ee3\u7801\u76ee\u5f55", None))
        self.btnGenJava.setText(QCoreApplication.translate("CoderDialog", u"\u6267\u884cJava\u4ee3\u7801\u751f\u6210", None))
        self.btnGenLaya.setText(QCoreApplication.translate("CoderDialog", u"\u6267\u884cLaya\u4ee3\u7801\u751f\u6210", None))
        self.groupBox_3.setTitle(QCoreApplication.translate("CoderDialog", u"\u3010SVN\u4e0b\u8f7d\u3011->\u3010\u540c\u6b65\u4ee3\u7801\u76ee\u5f55\u3011", None))
        self.chkUpdateCopy.setText(QCoreApplication.translate("CoderDialog", u"\u6267\u884c\u540e\u62f7\u8d1d\u81f3\u540c\u6b65\u4ee3\u7801\u76ee\u5f55", None))
        self.btnUpdateJava.setText(QCoreApplication.translate("CoderDialog", u"\u4eceSVN\u540c\u6b65Java\u4ee3\u7801", None))
        self.btnUpdateLaya.setText(QCoreApplication.translate("CoderDialog", u"\u4eceSVN\u540c\u6b65Laya\u4ee3\u7801", None))
        self.chkClearDirGen.setText(QCoreApplication.translate("CoderDialog", u"SVN\u62c9\u53d6\u6216\u751f\u6210\u524d\u6e05\u7406\u76ee\u5f55\u65e7\u4e34\u65f6\u6587\u4ef6", None))
        self.chkClearDirCopy.setText(QCoreApplication.translate("CoderDialog", u"\u76ee\u6807\u62f7\u8d1d\u76ee\u5f55\u6e05\u7406\u6240\u6709\u6587\u4ef6", None))
        self.chkAutoOpenFolderGenCode.setText(QCoreApplication.translate("CoderDialog", u"\u4ee3\u7801\u751f\u6210\u540e\u81ea\u52a8\u6253\u5f00\u76ee\u5f55", None))
        self.btnClose.setText(QCoreApplication.translate("CoderDialog", u"\u5173\u95ed", None))
    # retranslateUi

