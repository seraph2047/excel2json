import EventSystem from "./event/EventSystem";
import GameConfig from "./GameConfig";
import CompatibleCRC32 from "./lib/CompatibleCRC32";
import ExcelLib from "./lib/ExcelLib";
import ExcelLibData from "./lib/ExcelLibData";
import ExcelLibDefinition from "./lib/ExcelLibDefinition";
import ExcelLibConstant from "./lib/vo/ExcelLibConstant";
import ExcelLibDataEasyAccess from "./lib/vo/ExcelLibDataEasyAccess";
class Main {

	private static CFG_SERVER_HOST: string = "http://127.0.0.1:8080/";

	constructor() {
		//根据IDE设置初始化引擎		
		if (window["Laya3D"]) Laya3D.init(GameConfig.width, GameConfig.height);
		else Laya.init(GameConfig.width, GameConfig.height, Laya["WebGL"]);
		Laya["Physics"] && Laya["Physics"].enable();
		Laya["DebugPanel"] && Laya["DebugPanel"].enable();
		Laya.stage.scaleMode = GameConfig.scaleMode;
		Laya.stage.screenMode = GameConfig.screenMode;
		Laya.stage.alignV = GameConfig.alignV;
		Laya.stage.alignH = GameConfig.alignH;
		//兼容微信不支持加载scene后缀场景
		Laya.URL.exportSceneToJson = GameConfig.exportSceneToJson;

		//打开调试面板（通过IDE设置调试模式，或者url地址增加debug=true参数，均可打开调试面板）
		if (GameConfig.debug || Laya.Utils.getQueryString("debug") == "true") Laya.enableDebugPanel();
		if (GameConfig.physicsDebug && Laya["PhysicsDebugDraw"]) Laya["PhysicsDebugDraw"].enable();
		if (GameConfig.stat) Laya.Stat.show();
		Laya.alertGlobalError(true);

		//激活资源版本控制，version.json由IDE发布功能自动生成，如果没有也不影响后续流程
		Laya.ResourceVersion.enable("version.json", Laya.Handler.create(this, this.onVersionLoaded), Laya.ResourceVersion.FILENAME_VERSION);
	}

	onVersionLoaded(): void {
		//激活大小图映射，加载小图的时候，如果发现小图在大图合集里面，则优先加载大图合集，而不是小图		
		Laya.AtlasInfoManager.enable("fileconfig.json", Laya.Handler.create(this, this.onConfigLoaded));
	}

	onConfigLoaded(): void {
		//加载IDE指定的场景
		GameConfig.startScene && Laya.Scene.open(GameConfig.startScene);
		console.log("开始")
		EventSystem.getInstance().addEventListener(ExcelLib.EVENT_LIB_DEFINITION_DOWNLOADED, this, this.onLibDefinitionDownloaded);
		EventSystem.getInstance().addEventListener(ExcelLib.EVENT_LIB_DEFINITION_COMPLETED, this, this.onLibDefinitionCompleted);
		EventSystem.getInstance().addEventListener(ExcelLib.EVENT_LIB_LOAD_START, this, this.onLibLoadStart);
		EventSystem.getInstance().addEventListener(ExcelLib.EVENT_LIB_LOAD_ALL_START, this, this.onLibLoadAllStart);
		EventSystem.getInstance().addEventListener(ExcelLib.EVENT_LIB_LOAD_GROUP_START, this, this.onLibLoadGroupStart);
		EventSystem.getInstance().addEventListener(ExcelLib.EVENT_LIB_LOAD_PROCESS, this, this.onLibLoadProcess);
		EventSystem.getInstance().addEventListener(ExcelLib.EVENT_LIB_LOAD_DOWNLOADED, this, this.onLibLoadDownloaded);
		EventSystem.getInstance().addEventListener(ExcelLib.EVENT_LIB_LOAD_COMPLETED, this, this.onLibLoadCompleted);
		EventSystem.getInstance().addEventListener(ExcelLib.EVENT_LIB_LOAD_FAIL, this, this.onLibLoadFail);
		EventSystem.getInstance().addEventListener(ExcelLib.EVENT_LIB_LOAD_ALL_COMPLETED, this, this.onLibLoadAllCompleted);

		let lib: ExcelLib = ExcelLib.getInstance();
		//从网络下载数据必须先配置服务器ip
		// lib.setLoadFromConfigServer(Main.CFG_SERVER_HOST);
		lib.setLoadFromResServer(); //使用本地加载
		//定义可加载文件一定要首次读取
		lib.reloadDefinition();
	}

	onLibDefinitionDownloaded(): void {
		console.log("Event: 下载定义文件_definition.json完毕!")
	}
	onLibDefinitionCompleted(): void {
		console.log("Event: 解析定义文件_definition.json完毕!")
		console.log("ExcelLib.getInstance().isLoadedDefinition:" + ExcelLib.getInstance().isLoadedDefinition())
		ExcelLib.getInstance().loadAllData();
		Laya.timer.once(500, this, () => {
			ExcelLib.getInstance().loadFromGroup(ExcelLibConstant.GROUP_CHARACTERS);
		});
		Laya.timer.once(1000, this, () => {
			ExcelLib.getInstance().loadFromFileByName(ExcelLibConstant.TB_STAR_MAP_ID, false);
		});
		Laya.timer.once(1500, this, () => {
			ExcelLib.getInstance().loadFromFileByName(ExcelLibConstant.TB_LEVEL_MAP_LV, true);
		});
		Laya.timer.once(2000, this, () => {
			ExcelLib.getInstance().loadFromFileByName(ExcelLibConstant.TB_STAR_MAP_ID, true);
		});
		Laya.timer.once(2500, this, () => {
			ExcelLib.getInstance().loadFromFileByName(ExcelLibConstant.TB_CUSTOMERLEVEL_MKMAP_CUSTOMERLV_UPGRADETYPE, true);
		});

	}

	onLibLoadStart(loadList: Array<ExcelLibDefinition>, count: number): void {
		console.log("Event: 开始按文件名下载Lib文件，总数：" + count)
		for (let df of loadList) {
			console.log("Event: 开始按文件名下载Lib文件: [" + df.getComment() + "] -> " + df.getDataFullName())
		}
	}

	onLibLoadAllStart(loadList: Array<ExcelLibDefinition>, count: number): void {
		console.log("Event: 开始下载所有Lib文件，总数：" + count)
	}

	onLibLoadGroupStart(loadList: Array<ExcelLibDefinition>, count: number): void {
		console.log("Event: 开始按组下载Lib文件，总数：" + count)
		for (let df of loadList) {
			console.log("Event: 开始按组下载Lib文件: [" + df.getComment() + "] -> " + df.getDataFullName())
		}
	}

	onLibLoadDownloaded(loadList: Array<ExcelLibDefinition>, isSuccess: boolean): void {
		for (let df of loadList) {
			console.log("Event: 下载完毕Lib文件：" + df.getDataFullName() + " 是否成功：" + isSuccess)
		}
	}

	onLibLoadCompleted(dataFullName: string, table: any): void {
		console.log("Event: 解析Lib文件成功：" + dataFullName)
		console.log(table)
	}

	onLibLoadFail(dataFullName: string): void {
		console.log("Event: 解析Lib文件失败!! " + dataFullName)
	}

	onLibLoadProcess(process: number): void {
		console.log("Event: 下载进度：" + (process * 100) + "%")
	}

	onLibLoadAllCompleted(loadList: Array<ExcelLibDefinition>, isAllSuccess: boolean): void {
		console.log("Event: 解析Lib文件全部完成, 是否全部成功：" + isAllSuccess)
	}


	// public static CRC32(source: Array<number>, offset: number, length: number): number {
	// 	let wCRCin: number = 0xFFFFFFFF;
	// 	let wCPoly: number = 0xEDB88320;
	// 	for (let i = offset, cnt = offset + length; i < cnt; i++) {
	// 		wCRCin ^= (source[i] & 0x000000FF);
	// 		for (let j = 0; j < 8; j++) {
	// 			if ((wCRCin & 0x00000001) != 0) {
	// 				wCRCin >>= 1;
	// 				wCRCin ^= wCPoly;
	// 			} else {
	// 				wCRCin >>= 1;
	// 			}
	// 		}
	// 	}
	// 	return wCRCin ^= 0xFFFFFFFF;
	// }



}
//激活启动类
new Main();
