export default class EventSystem{
    private static eventDispatcher: Laya.EventDispatcher = new Laya.EventDispatcher();
    
    private static _instance: EventSystem;
    
    public static getInstance(): EventSystem {
        if (EventSystem._instance == null) {
            EventSystem._instance = new EventSystem();
        }
        return EventSystem._instance;
    }

    constructor() {
    }

    // 注册事件
    public dispatch(eventName:string, agv?) {
        // 派发事件
        EventSystem.eventDispatcher.event(eventName, agv);
    }

    /**
     * 使用 EventDispatcher 对象注册指定类型的事件侦听器对象，以使侦听器能够接收事件通知。
     * @param eventName     事件的类型。
     * @param caller    事件侦听函数的执行域。
     * @param listener  事件侦听函数。
     * @param arg       （可选）事件侦听函数的回调参数。
     */
    public addEventListener(eventName: string, caller, listener, arg?: any[]) {
        EventSystem.eventDispatcher.on(eventName, caller, listener, (arg == null) ? null : ([arg]));
    }

    /**
     * 使用 EventDispatcher 对象注册指定类型的事件侦听器对象，以使侦听器能够接收事件通知，此侦听事件响应一次后自动移除。
     * @param eventName     事件的类型。
     * @param caller    事件侦听函数的执行域。
     * @param listener  事件侦听函数。
     * @param arg       （可选）事件侦听函数的回调参数。
     */
    public addEventListenerOnce(eventName: string, caller, listener, arg?: any[]) {
        EventSystem.eventDispatcher.once(eventName, caller, listener, (arg == null) ? null : ([arg]));
    }

    /**
     * 从 EventDispatcher 对象中删除侦听器。
     * @param eventName     事件的类型。
     * @param caller    事件侦听函数的执行域。
     * @param listener  事件侦听函数。
     * @param onceOnly  （可选）如果值为 true ,则只移除通过 once 方法添加的侦听器。
     */
    public removeListener(eventName: string, caller: any, listener: Function, onceOnly?: boolean) {
        EventSystem.eventDispatcher.off(eventName, caller, listener, onceOnly);
    }

    /**
     * 从 EventDispatcher 对象中删除指定事件类型的所有侦听器。
     * @param eventName （可选）事件类型，如果值为 null，则移除本对象所有类型的侦听器。
     */
    public removeAll(eventName: string) {
        EventSystem.eventDispatcher.offAll(eventName);
    }
}