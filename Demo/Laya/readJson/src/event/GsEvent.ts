export default class GsEvent{

    /** 事件：主状态变更前 */
    public static EVENT_STATUS_TRANS_PREPARE: string = "EVENT_STATUS_TRANS_PREPARE";
    /** 事件：主状态变更执行后 */
    public static EVENT_STATUS_TRANS_AFTER: string = "EVENT_STATUS_TRANS_AFTER";

    /** 事件：敌人消亡 */
    public static EVENT_BOX_DEAD: string = "EVENT_BOX_DEAD";

    /** 事件：子弹消亡 */
    public static EVENT_BULLET_DEAD: string = "EVENT_BULLET_DEAD";
    /** 事件：子弹击中目标 */
    public static EVENT_BULLET_HIT: string = "EVENT_BULLET_HIT";
    /** 事件：子弹飞出屏幕 */
    public static EVENT_BULLET_OUT_SCREEN: string = "EVENT_BULLET_OUT_SCREEN";

    /** 事件：子弹击中地面 */
    public static EVENT_HIT_GROUND: string = "EVENT_HIT_GROUND";

}