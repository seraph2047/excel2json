

export default class HeaderData{
    private version:string;
    private date:string;
    private author:string;
    private count:number;
    private md5:string;


    public static parseFromJson(json:JSON):HeaderData{
        let e = new HeaderData();
        e.version = json["version"]
        e.date = json["date"]
        e.author = json["author"]
        e.count = json["count"]
        e.md5 = json["md5"]       
        return e;
    }

    public getVersion():string{
        return this.version;
    }
    public getDate():string{
        return this.date;
    }
    public getAuthor():string{
        return this.author;
    }
    public getCount():number{
        return this.count;
    }
    public getMd5():string{
        return this.md5;
    }




}