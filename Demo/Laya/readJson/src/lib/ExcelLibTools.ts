import ExcelLibDefinition from "./ExcelLibDefinition";
import ILibItem from "./ILibItem";
import ExcelLibDataEasyAccess from "./vo/ExcelLibDataEasyAccess";



export default class ExcelLibTools{

    
    public static loadJsonList(jsonData:JSON, df: ExcelLibDefinition):any{
        let list:Array<ILibItem> = new Array();
        let jsonArr:Array<JSON> = jsonData["data"]        
        for(let json of jsonArr){
            let vo:ILibItem = ExcelLibDataEasyAccess.voFactory(df.getClassName())
            vo.parseFromJson(json)
            let mainKey = vo.__getMainKey(df.getDataFullName())
            list.push(vo)            
        }
        return list;
    }

    public static loadJsonIntMap(jsonData:JSON, df: ExcelLibDefinition):any{
        let map:Map<number, ILibItem> = new Map();
        let jsonArr:Array<JSON> = jsonData["data"]
        for(let json of jsonArr){
            let vo:ILibItem = ExcelLibDataEasyAccess.voFactory(df.getClassName())
            vo.parseFromJson(json)
            let mainKey = vo.__getMainKey(df.getDataFullName())
            map.set(mainKey, vo)
        }
        return map;
    }

    public static loadJsonStringMap(jsonData:JSON, df: ExcelLibDefinition):any{
        let map:Map<string, ILibItem> = new Map();
        let jsonArr:Array<JSON> = jsonData["data"]
        for(let json of jsonArr){
            let vo:ILibItem = ExcelLibDataEasyAccess.voFactory(df.getClassName())
            vo.parseFromJson(json)
            let mainKey = vo.__getMainKey(df.getDataFullName())
            map.set(mainKey, vo)
        }
        return map;
    }

    public static loadJsonMapSetByIntegerKey(jsonData:JSON, df: ExcelLibDefinition):any{
        let map:Map<number, Array<ILibItem>> = new Map();
        let jsonArr:Array<JSON> = jsonData["data"]        
        for(let json of jsonArr){
            let vo:ILibItem = ExcelLibDataEasyAccess.voFactory(df.getClassName())
            vo.parseFromJson(json)
            let mainKey = vo.__getMainKey(df.getDataFullName())
            let keyList:Array<ILibItem>
            if(map.has(mainKey)){
                keyList = map.get(mainKey)
            }else{
                keyList = new Array()
                map.set(mainKey, keyList)
            }
            keyList.push(vo)            
        }
        return map;
    }

    public static loadJsonMapSetByStringKey(jsonData:JSON, df: ExcelLibDefinition):any{
        let map:Map<string, Array<ILibItem>> = new Map();
        let jsonArr:Array<JSON> = jsonData["data"]        
        for(let json of jsonArr){
            let vo:ILibItem = ExcelLibDataEasyAccess.voFactory(df.getClassName())
            vo.parseFromJson(json)
            let mainKey = vo.__getMainKey(df.getDataFullName())
            let keyList:Array<ILibItem>
            if(map.has(mainKey)){
                keyList = map.get(mainKey)
            }else{
                keyList = new Array()
                map.set(mainKey, keyList)
            }
            keyList.push(vo)            
        }
        return map;
    }

    public static loadJsonMultKeyMap(jsonData:JSON, df: ExcelLibDefinition):any{
        let map:Map<string, ILibItem> = new Map();
        let jsonArr:Array<JSON> = jsonData["data"]        
        for(let json of jsonArr){
            let vo:ILibItem = ExcelLibDataEasyAccess.voFactory(df.getClassName())
            vo.parseFromJson(json)            
            let keyStr:string = ""
            for(let keySet of  df.getKeys()){
                for(let [keyName,keyType] of keySet){
                    let key = json[keyName]
                    keyStr += key.toString() + "_";
                }
            }
            map.set(keyStr, vo)
        }
        return map;
    }



}