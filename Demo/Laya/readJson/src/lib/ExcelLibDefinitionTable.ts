


export default class ExcelLibDefinitionTable {

    private jsonName:string;
    private comment:string;
    private md5:string;
    private date:string;
    private version:string;
    private author:string;

    public static parseFromJson(json:JSON):ExcelLibDefinitionTable{
        let e = new ExcelLibDefinitionTable();
        e.jsonName = json["jsonName"]
        e.comment = json["comment"]
        e.md5 = json["md5"] 
        e.date = json["date"]
        e.version = json["version"]
        e.author = json["author"]
        return e;
    }



    public getJsonName() :string{
        return this.jsonName;
    }

    public setJsonName(jsonName) {
        this.jsonName = jsonName;
    }

    public getComment():string {
        return this.comment;
    }

    public setComment(comment) {
        this.comment = comment;
    }

    public getMd5():string {
        return this.md5;
    }

    public setMd5(md5) {
        this.md5 = md5;
    }

    public getDate():string {
        return this.date;
    }

    public setDate(date: string) {
        this.date = date;
    }

    public getVersion() :string{
        return this.version;
    }

    public setVersion(version) {
        this.version = version;
    }

    public getAuthor() :string{
        return this.author;
    }

    public setAuthor(author) {
        this.author = author;
    }
}
