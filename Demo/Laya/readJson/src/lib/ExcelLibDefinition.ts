export default class ExcelLibDefinition{

    
    //数据类型判断常量
    public static FIELD_TYPE_INTEGER:string = "Integer";
    public static FIELD_TYPE_LONG:string = "Long";
    public static FIELD_TYPE_STRING:string = "String";
    public static FIELD_TYPE_FLOAT:string = "Float";
    public static FIELD_TYPE_DOUBLE:string = "Double";
    public static FIELD_TYPE_BOOL:string = "Boolean";
    //加载索引方式类型判断常量
    public static LOAD_TYPE_LIST:string = "List";
    public static LOAD_TYPE_MAP:string = "Map";
    public static LOAD_TYPE_MKMAP:string = "MultKeyMap";
    public static LOAD_TYPE_MAP2LIST:string = "Map2List";

    
    /**
     * 主键
     * Map<键名,主键类型(ExcelLibDefinition.FIELD_TYPE_*)>
     */
    private keys:Array<Map<string,string>>;

    /**
     * 索引主键名
     */
    private mainKey:string;

    /**
     * 主键类型，对应 ExcelLibDefinition.FIELD_TYPE_*
     */
    private mainKeyType:string;

    /**
     * 加载索引方式类型，对应 ExcelLibDefinition.LOAD_TYPE_*
     */
    private type:string;

    /**
     * 本包工具生成时支持的平台类型（展示用，在开发代码中无实际逻辑）
     */
    private platforms:Array<string>;

    /**
     * 加载权重，数值越小加载越早，相同权重随机排列
     */
    private loadWeight:number;

    /**
     * 需要加载的表格
     */
    private loadJson:string;

    /**
     * 需要加载的类名
     */
    private className:string;

    /**
     * 注释
     */
    private comment:string;

    /**
     * 数据表名，含索引方式命名
     */
    private dataName:string;

    /**
     * 组别名
     */
    private group:string;

    /**
     * 关联加载
     */
    private loadRelations:Array<string>;

    /**
     * 数据表名，含索引方式命名+索引字段命名
     */
    private dataFullName:string;

    private mainKeys:string[] = null;


    public static parseFromJson(json:JSON):ExcelLibDefinition{
        let e = new ExcelLibDefinition();
        e.className = json["className"]
        e.comment = json["comment"]
        e.dataFullName = json["dataFullName"]
        e.dataName = json["dataName"]
        e.group = json["group"]
        e.loadJson = json["loadJson"]
        e.loadWeight = json["loadWeight"]
        e.mainKey = json["mainKey"]
        e.mainKeyType = json["mainKeyType"]
        e.type = json["type"]
        e.keys = new Array(); //Array<Map<string,string>>
        let keysJson:Array<JSON> = json["keys"]
        for(let keyJson of keysJson){
            let name = keyJson["name"]
            let type = keyJson["type"]
            let keyMap:Map<string,string> = new Map()
            keyMap.set(name,type)
            e.keys.push(keyMap)
        }
        e.loadRelations = new Array()
        let relJson:Array<string> = json["loadRelations"]
        for(let relation of relJson){
            e.loadRelations.push(relation);
        }
        e.platforms = new Array()
        let platformsJson:Array<string> = json["platforms"]
        for(let platform of platformsJson){
            e.platforms.push(platform);
        }        
        return e;
    }



    /**
     * 多个主键调用这个方法提取
     * @param forceLoad 是否强制更新
     * @return
     */
    public getMainKeys(forceLoad:boolean= false):string[]  {
        if(this.mainKeys==null || forceLoad) {
            this.mainKeys = new String[this.keys.length];
            
            for (let i:number = 0; i < this.keys.length; i++) {
                this.mainKeys[i] = this.keys[i].get("name");
            }
        }
        return this.mainKeys;
    }

    public getMainKey():string {
        return this.mainKey;
    }

    public setMainKey(mainKey:string) {
        this.mainKey = mainKey;
    }

    public getKeys():Array<Map<string,string>> {
        return this.keys;
    }

    public setKeys(keys:Array<Map<string,string>>) {
        this.keys = keys;
    }

    public getMainKeyType():string {
        return this.mainKeyType;
    }

    public setMainKeyType(mainKeyType:string) {
        this.mainKeyType = mainKeyType;
    }

    public getType():string {
        return this.type;
    }

    public setType(type:string) {
        this.type = type;
    }

    public getPlatforms():Array<string> {
        return this.platforms;
    }

    public setPlatforms(platforms:Array<string>) {        
        this.platforms = platforms;
    }

    public getLoadWeight():number {
        return this.loadWeight;
    }

    public setLoadWeight(loadWeight:number) {
        this.loadWeight = loadWeight;
    }

    public getLoadJson():string {
        return this.loadJson;
    }

    public setLoadJson(loadJson:string) {
        this.loadJson = loadJson;
    }

    public getComment():string {
        return this.comment;
    }

    public setComment(comment:string) {
        this.comment = comment;
    }

    public getClassName():string {
        return this.className;
    }

    public setClassName(className:string) {
        this.className = className;
    }

    public getDataName():string {
        return this.dataName;
    }

    public getGroup():string {
        return this.group;
    }

    public setGroup(group:string) {
        this.group = group;
    }

    public getLoadRelations():Array<string>  {
        return this.loadRelations;
    }

    public setLoadRelations(loadRelations:Array<string>) {
        this.loadRelations = loadRelations;
    }

    public setDataName(dataName:string) {
        this.dataName = dataName;
    }

    public getDataFullName():string {
        return this.dataFullName;
    }

    public setDataFullName(dataFullName:string) {
        this.dataFullName = dataFullName;
    }

}