

export default interface ILibItem{

    parseFromJson(json:JSON):boolean;
    
    __getMainKey(dataFullName: string):any;
}