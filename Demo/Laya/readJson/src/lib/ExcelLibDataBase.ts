

export default class ExcelLibDataBase{
    
    protected maps:Map<String, Object>;

    public getMaps():Map<String, Object> {
        if(this.maps==null){
            this.maps = new Map();
        }
        return this.maps;
    }

}