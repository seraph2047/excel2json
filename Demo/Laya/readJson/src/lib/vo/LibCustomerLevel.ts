import ILibItem from "../ILibItem";
import LibItemBase from "../LibItemBase";



/**
 * 导表实体：顾客等级表
 * @author Generate By Excel2Json
 **/
export default class LibCustomerLevel extends LibItemBase implements ILibItem {

	constructor() { super() }

    
    /**
    * 顾客等级
    **/
    private customerLv: number;
    
    /**
    * 类型
    **/
    private upgradeType: number;
    
    /**
    * 购买力
    **/
    private purchasing: number;


    
    /**
    * 顾客等级
    **/
    public getCustomerLv(): number { return this.customerLv; }
    public setCustomerLv(customerLv: number) { this.customerLv = customerLv; }
    
    /**
    * 类型
    **/
    public getUpgradeType(): number { return this.upgradeType; }
    public setUpgradeType(upgradeType: number) { this.upgradeType = upgradeType; }
    
    /**
    * 购买力
    **/
    public getPurchasing(): number { return this.purchasing; }
    public setPurchasing(purchasing: number) { this.purchasing = purchasing; }

    /**
     * 从json中加载数据
     * @param json 
     * @returns 是否导入成功
     */
    public parseFromJson(json: JSON): boolean {
        if(json==null||json==undefined){
            return false
        }
        
        this.customerLv = json["customerLv"]
        this.upgradeType = json["upgradeType"]
        this.purchasing = json["purchasing"]
        return true
    }
    
    public __getMainKey(dataFullName: string):any {
        switch(dataFullName){
            case "customerLevelMkMapByCustomerLvUpgradeType": return this.customerLv + "_" + this.upgradeType + "_"
        }
        return null;
    }
    
    public toString(): string {
        return "LibCustomerLevel(顾客等级表){" +
                "customerLv=" + this.customerLv + 
                ", upgradeType=" + this.upgradeType + 
                ", purchasing=" + this.purchasing + 
                '}';
    }
    

}