import ExcelLibDataBase from "../ExcelLibDataBase";
import ILibItem from "../ILibItem";

import LibStar from "./LibStar";
import LibCharacter from "./LibCharacter";
import LibLevel from "./LibLevel";
import LibCustomerLevel from "./LibCustomerLevel";
import LibTeam from "./LibTeam";


/**
 * 数据表自动生成数据快捷读取类
 * @author Generate By Excel2Json
 **/
export default class ExcelLibDataEasyAccess extends ExcelLibDataBase {


    /**
    * 根据Vo类名获取实体
    * @param className 
    * @returns 
    */
    public static voFactory(className:string):ILibItem{
        switch(className){
            case "LibStar": return new LibStar();
            case "LibCharacter": return new LibCharacter();
            case "LibLevel": return new LibLevel();
            case "LibCustomerLevel": return new LibCustomerLevel();
        }
        return null
    }
    
    /**
     * 表[角色星级:star] starMapById
     * @param id
     * @return LibStar
     */
    public starMap(id: number): LibStar {
        let data:Map<number, LibStar> = this.maps.get("starMapById") as Map<number, LibStar>;
        return data == null ? null : data.get(id);
    }
    
    /**
     * 表[角色:character] characterMapByCid
     * @param cid
     * @return LibCharacter
     */
    public characterMap(cid: number): LibCharacter {
        let data:Map<number, LibCharacter> = this.maps.get("characterMapByCid") as Map<number, LibCharacter>;
        return data == null ? null : data.get(cid);
    }
    
    /**
     * 表[等级:level] levelMapByLv
     * @param lv
     * @return LibLevel
     */
    public levelMap(lv: number): LibLevel {
        let data:Map<number, LibLevel> = this.maps.get("levelMapByLv") as Map<number, LibLevel>;
        return data == null ? null : data.get(lv);
    }
    
    /**
     * 表[顾客等级表:customerLevel] customerLevelMkMapByCustomerLvUpgradeType
     * @param customerLv: number, upgradeType: number
     * @return LibCustomerLevel
     */
    public customerLevelMkMap(customerLv: number, upgradeType: number): LibCustomerLevel {
        let data: Map<string, LibCustomerLevel> = this.maps.get("customerLevelMkMapByCustomerLvUpgradeType") as Map<string, LibCustomerLevel>;
        return data == null ? null : data.get(customerLv + "_" + upgradeType + "_");
    }
    

}