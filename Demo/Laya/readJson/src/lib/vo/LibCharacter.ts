import ILibItem from "../ILibItem";
import LibItemBase from "../LibItemBase";



/**
 * 导表实体：角色
 * @author Generate By Excel2Json
 **/
export default class LibCharacter extends LibItemBase implements ILibItem {

	constructor() { super() }

    
    /**
    * 主键
    **/
    private cid: number;
    
    /**
    * 角色名
    **/
    private sname: string;
    
    /**
    * 职业
    **/
    private ocId: number;
    
    /**
    * 大类ID
    **/
    private ctype: number;
    
    /**
    * 品阶
    **/
    private grade: number;
    
    /**
    * 生命
    **/
    private hp: number;
    
    /**
    * 力量
    **/
    private atk: number;
    
    /**
    * 智力
    **/
    private mag: number;
    
    /**
    * 精神
    **/
    private mdf: number;
    
    /**
    * 防御
    **/
    private df: number;
    
    /**
    * 暴击
    **/
    private cri: number;
    
    /**
    * 射程
    **/
    private rng: number;
    
    /**
    * 攻速
    **/
    private aspd: number;
    
    /**
    * 弹道速度
    **/
    private bulletSpeed: number;
    
    /**
    * 图标
    **/
    private icon: string;
    
    /**
    * 起手延迟
    **/
    private startDelay: number;
    
    /**
    * 普攻伤害延时
    **/
    private mcDelay: number;
    
    /**
    * 模型路径
    **/
    private model: string;
    
    /**
    * 高精模型路径
    **/
    private hdModel: string;
    
    /**
    * 普攻动作特效
    **/
    private fnAtk: string;
    
    /**
    * 被普攻受击动作特效
    **/
    private fnDamage: string;
    
    /**
    * 普攻弹道
    **/
    private fnBullet: string;
    
    /**
    * 普攻爆炸点
    **/
    private fnExplo: string;
    
    /**
    * 缩放
    **/
    private scale: number;
    
    /**
    * 对话框展示形象
    **/
    private avatarModel: string;


    
    /**
    * 主键
    **/
    public getCid(): number { return this.cid; }
    public setCid(cid: number) { this.cid = cid; }
    
    /**
    * 角色名
    **/
    public getSname(): string { return this.sname; }
    public setSname(sname: string) { this.sname = sname; }
    
    /**
    * 职业
    **/
    public getOcId(): number { return this.ocId; }
    public setOcId(ocId: number) { this.ocId = ocId; }
    
    /**
    * 大类ID
    **/
    public getCtype(): number { return this.ctype; }
    public setCtype(ctype: number) { this.ctype = ctype; }
    
    /**
    * 品阶
    **/
    public getGrade(): number { return this.grade; }
    public setGrade(grade: number) { this.grade = grade; }
    
    /**
    * 生命
    **/
    public getHp(): number { return this.hp; }
    public setHp(hp: number) { this.hp = hp; }
    
    /**
    * 力量
    **/
    public getAtk(): number { return this.atk; }
    public setAtk(atk: number) { this.atk = atk; }
    
    /**
    * 智力
    **/
    public getMag(): number { return this.mag; }
    public setMag(mag: number) { this.mag = mag; }
    
    /**
    * 精神
    **/
    public getMdf(): number { return this.mdf; }
    public setMdf(mdf: number) { this.mdf = mdf; }
    
    /**
    * 防御
    **/
    public getDf(): number { return this.df; }
    public setDf(df: number) { this.df = df; }
    
    /**
    * 暴击
    **/
    public getCri(): number { return this.cri; }
    public setCri(cri: number) { this.cri = cri; }
    
    /**
    * 射程
    **/
    public getRng(): number { return this.rng; }
    public setRng(rng: number) { this.rng = rng; }
    
    /**
    * 攻速
    **/
    public getAspd(): number { return this.aspd; }
    public setAspd(aspd: number) { this.aspd = aspd; }
    
    /**
    * 弹道速度
    **/
    public getBulletSpeed(): number { return this.bulletSpeed; }
    public setBulletSpeed(bulletSpeed: number) { this.bulletSpeed = bulletSpeed; }
    
    /**
    * 图标
    **/
    public getIcon(): string { return this.icon; }
    public setIcon(icon: string) { this.icon = icon; }
    
    /**
    * 起手延迟
    **/
    public getStartDelay(): number { return this.startDelay; }
    public setStartDelay(startDelay: number) { this.startDelay = startDelay; }
    
    /**
    * 普攻伤害延时
    **/
    public getMcDelay(): number { return this.mcDelay; }
    public setMcDelay(mcDelay: number) { this.mcDelay = mcDelay; }
    
    /**
    * 模型路径
    **/
    public getModel(): string { return this.model; }
    public setModel(model: string) { this.model = model; }
    
    /**
    * 高精模型路径
    **/
    public getHdModel(): string { return this.hdModel; }
    public setHdModel(hdModel: string) { this.hdModel = hdModel; }
    
    /**
    * 普攻动作特效
    **/
    public getFnAtk(): string { return this.fnAtk; }
    public setFnAtk(fnAtk: string) { this.fnAtk = fnAtk; }
    
    /**
    * 被普攻受击动作特效
    **/
    public getFnDamage(): string { return this.fnDamage; }
    public setFnDamage(fnDamage: string) { this.fnDamage = fnDamage; }
    
    /**
    * 普攻弹道
    **/
    public getFnBullet(): string { return this.fnBullet; }
    public setFnBullet(fnBullet: string) { this.fnBullet = fnBullet; }
    
    /**
    * 普攻爆炸点
    **/
    public getFnExplo(): string { return this.fnExplo; }
    public setFnExplo(fnExplo: string) { this.fnExplo = fnExplo; }
    
    /**
    * 缩放
    **/
    public getScale(): number { return this.scale; }
    public setScale(scale: number) { this.scale = scale; }
    
    /**
    * 对话框展示形象
    **/
    public getAvatarModel(): string { return this.avatarModel; }
    public setAvatarModel(avatarModel: string) { this.avatarModel = avatarModel; }

    /**
     * 从json中加载数据
     * @param json 
     * @returns 是否导入成功
     */
    public parseFromJson(json: JSON): boolean {
        if(json==null||json==undefined){
            return false
        }
        
        this.cid = json["cid"]
        this.sname = json["sname"]
        this.ocId = json["ocId"]
        this.ctype = json["ctype"]
        this.grade = json["grade"]
        this.hp = json["hp"]
        this.atk = json["atk"]
        this.mag = json["mag"]
        this.mdf = json["mdf"]
        this.df = json["df"]
        this.cri = json["cri"]
        this.rng = json["rng"]
        this.aspd = json["aspd"]
        this.bulletSpeed = json["bulletSpeed"]
        this.icon = json["icon"]
        this.startDelay = json["startDelay"]
        this.mcDelay = json["mcDelay"]
        this.model = json["model"]
        this.hdModel = json["hdModel"]
        this.fnAtk = json["fnAtk"]
        this.fnDamage = json["fnDamage"]
        this.fnBullet = json["fnBullet"]
        this.fnExplo = json["fnExplo"]
        this.scale = json["scale"]
        this.avatarModel = json["avatarModel"]
        return true
    }
    
    public __getMainKey(dataFullName: string):any {
        switch(dataFullName){
            case "characterMapByCid": return this.cid;
        }
        return null;
    }
    
    public toString(): string {
        return "LibCharacter(角色){" +
                "cid=" + this.cid + 
                ", sname='" + this.sname + '\'' + 
                ", ocId=" + this.ocId + 
                ", ctype=" + this.ctype + 
                ", grade=" + this.grade + 
                ", hp=" + this.hp + 
                ", atk=" + this.atk + 
                ", mag=" + this.mag + 
                ", mdf=" + this.mdf + 
                ", df=" + this.df + 
                ", cri=" + this.cri + 
                ", rng=" + this.rng + 
                ", aspd=" + this.aspd + 
                ", bulletSpeed=" + this.bulletSpeed + 
                ", icon='" + this.icon + '\'' + 
                ", startDelay=" + this.startDelay + 
                ", mcDelay=" + this.mcDelay + 
                ", model='" + this.model + '\'' + 
                ", hdModel='" + this.hdModel + '\'' + 
                ", fnAtk='" + this.fnAtk + '\'' + 
                ", fnDamage='" + this.fnDamage + '\'' + 
                ", fnBullet='" + this.fnBullet + '\'' + 
                ", fnExplo='" + this.fnExplo + '\'' + 
                ", scale=" + this.scale + 
                ", avatarModel='" + this.avatarModel + '\'' + 
                '}';
    }
    

}