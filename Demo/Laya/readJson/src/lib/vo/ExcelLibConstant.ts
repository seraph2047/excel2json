

/**
 * 自动生成的数据表对应常量
 * @author Generate By Excel2Json
 **/
export default class ExcelLibConstant {


    /**
     * 组别[characters] 包含表如下：<BR/>[角色星级 LibStar] -> starMapById();<BR/>[角色 LibCharacter] -> characterMapByCid();<BR/>[等级 LibLevel] -> levelMapByLv();<BR/>[顾客等级表 LibCustomerLevel] -> customerLevelMkMapByCustomerLvUpgradeType();
     */
    public static GROUP_CHARACTERS: string = "group_characters";
    
    /**
     * 表[角色星级:star] starMapById dataFullName Relation: starMap(number id) -> LibStar
     */
    public static TB_STAR_MAP_ID: string = "starMapById";
    
    /**
     * 表[角色:character] characterMapByCid dataFullName Relation: characterMap(number cid) -> LibCharacter
     */
    public static TB_CHARACTER_MAP_CID: string = "characterMapByCid";
    
    /**
     * 表[等级:level] levelMapByLv dataFullName Relation: levelMap(number lv) -> LibLevel
     */
    public static TB_LEVEL_MAP_LV: string = "levelMapByLv";
    
    /**
     * 表[顾客等级表:customerLevel] customerLevelMkMapByCustomerLvUpgradeType dataFullName Relation: customerLevelMkMap(customerLv: number, upgradeType: number) -> LibCustomerLevel
     */
    public static TB_CUSTOMERLEVEL_MKMAP_CUSTOMERLV_UPGRADETYPE: string = "customerLevelMkMapByCustomerLvUpgradeType";
    

}
