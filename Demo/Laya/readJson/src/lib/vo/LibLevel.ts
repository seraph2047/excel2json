import ILibItem from "../ILibItem";
import LibItemBase from "../LibItemBase";



/**
 * 导表实体：等级
 * @author Generate By Excel2Json
 **/
export default class LibLevel extends LibItemBase implements ILibItem {

	constructor() { super() }

    
    /**
    * 等级
    **/
    private lv: number;
    
    /**
    * 换算等级
    **/
    private elv2: number;
    
    /**
    * 被动技能格数
    **/
    private pskillCount2: number;
    
    /**
    * 允许升职级
    **/
    private occLevel: number;


    
    /**
    * 等级
    **/
    public getLv(): number { return this.lv; }
    public setLv(lv: number) { this.lv = lv; }
    
    /**
    * 换算等级
    **/
    public getElv2(): number { return this.elv2; }
    public setElv2(elv2: number) { this.elv2 = elv2; }
    
    /**
    * 被动技能格数
    **/
    public getPskillCount2(): number { return this.pskillCount2; }
    public setPskillCount2(pskillCount2: number) { this.pskillCount2 = pskillCount2; }
    
    /**
    * 允许升职级
    **/
    public getOccLevel(): number { return this.occLevel; }
    public setOccLevel(occLevel: number) { this.occLevel = occLevel; }

    /**
     * 从json中加载数据
     * @param json 
     * @returns 是否导入成功
     */
    public parseFromJson(json: JSON): boolean {
        if(json==null||json==undefined){
            return false
        }
        
        this.lv = json["lv"]
        this.elv2 = json["elv2"]
        this.pskillCount2 = json["pskillCount2"]
        this.occLevel = json["occLevel"]
        return true
    }
    
    public __getMainKey(dataFullName: string):any {
        switch(dataFullName){
            case "levelMapByLv": return this.lv;
        }
        return null;
    }
    
    public toString(): string {
        return "LibLevel(等级){" +
                "lv=" + this.lv + 
                ", elv2=" + this.elv2 + 
                ", pskillCount2=" + this.pskillCount2 + 
                ", occLevel=" + this.occLevel + 
                '}';
    }
    

}