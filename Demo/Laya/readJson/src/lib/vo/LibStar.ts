import ILibItem from "../ILibItem";
import LibItemBase from "../LibItemBase";

import LibCharacter from "./LibCharacter";


/**
 * 导表实体：角色星级
 * @author Generate By Excel2Json
 **/
export default class LibStar extends LibItemBase implements ILibItem {

	constructor() { super() }

    
    /**
    * 主键
    **/
    private id: number;
    
    /**
    * 角色
    **/
    private cid: number;
    
    /**
    * 星级
    **/
    private lv: number;
    
    /**
    * 生命成长率
    **/
    private hp: number;
    
    /**
    * 力量成长率
    **/
    private atk: number;
    
    /**
    * 智力成长率
    **/
    private mag: number;
    
    /**
    * 技能
    **/
    private skid: number;


    
    /**
    * 主键
    **/
    public getId(): number { return this.id; }
    public setId(id: number) { this.id = id; }
    
    /**
    * 角色
    **/
    public getCid(): number { return this.cid; }
    public setCid(cid: number) { this.cid = cid; }
    
    /**
    * 星级
    **/
    public getLv(): number { return this.lv; }
    public setLv(lv: number) { this.lv = lv; }
    
    /**
    * 生命成长率
    **/
    public getHp(): number { return this.hp; }
    public setHp(hp: number) { this.hp = hp; }
    
    /**
    * 力量成长率
    **/
    public getAtk(): number { return this.atk; }
    public setAtk(atk: number) { this.atk = atk; }
    
    /**
    * 智力成长率
    **/
    public getMag(): number { return this.mag; }
    public setMag(mag: number) { this.mag = mag; }
    
    /**
    * 技能
    **/
    public getSkid(): number { return this.skid; }
    public setSkid(skid: number) { this.skid = skid; }

    /**
     * 角色  关联：cid->LibCharacter.cid (实体)
     * @return
     */
    public getLibCharacterByCid(): LibCharacter{
        return super.getLib().characterMap(this.cid);
    }
    
    /**
     * 从json中加载数据
     * @param json 
     * @returns 是否导入成功
     */
    public parseFromJson(json: JSON): boolean {
        if(json==null||json==undefined){
            return false
        }
        
        this.id = json["id"]
        this.cid = json["cid"]
        this.lv = json["lv"]
        this.hp = json["hp"]
        this.atk = json["atk"]
        this.mag = json["mag"]
        this.skid = json["skid"]
        return true
    }
    
    public __getMainKey(dataFullName: string):any {
        switch(dataFullName){
            case "starMapById": return this.id;
        }
        return null;
    }
    
    public toString(): string {
        return "LibStar(角色星级){" +
                "id=" + this.id + 
                ", cid=" + this.cid + 
                ", lv=" + this.lv + 
                ", hp=" + this.hp + 
                ", atk=" + this.atk + 
                ", mag=" + this.mag + 
                ", skid=" + this.skid + 
                '}';
    }
    

}