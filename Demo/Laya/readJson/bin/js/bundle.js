(function () {
    'use strict';

    class EventSystem {
        constructor() {
        }
        static getInstance() {
            if (EventSystem._instance == null) {
                EventSystem._instance = new EventSystem();
            }
            return EventSystem._instance;
        }
        dispatch(eventName, agv) {
            EventSystem.eventDispatcher.event(eventName, agv);
        }
        addEventListener(eventName, caller, listener, arg) {
            EventSystem.eventDispatcher.on(eventName, caller, listener, (arg == null) ? null : ([arg]));
        }
        addEventListenerOnce(eventName, caller, listener, arg) {
            EventSystem.eventDispatcher.once(eventName, caller, listener, (arg == null) ? null : ([arg]));
        }
        removeListener(eventName, caller, listener, onceOnly) {
            EventSystem.eventDispatcher.off(eventName, caller, listener, onceOnly);
        }
        removeAll(eventName) {
            EventSystem.eventDispatcher.offAll(eventName);
        }
    }
    EventSystem.eventDispatcher = new Laya.EventDispatcher();

    class GameConfig {
        constructor() { }
        static init() {
            var reg = Laya.ClassUtils.regClass;
        }
    }
    GameConfig.width = 640;
    GameConfig.height = 1136;
    GameConfig.scaleMode = "fixedwidth";
    GameConfig.screenMode = "none";
    GameConfig.alignV = "top";
    GameConfig.alignH = "left";
    GameConfig.startScene = "Test.scene";
    GameConfig.sceneRoot = "";
    GameConfig.debug = false;
    GameConfig.stat = false;
    GameConfig.physicsDebug = false;
    GameConfig.exportSceneToJson = true;
    GameConfig.init();

    class CompatibleCRC32 {
        static get32BitDec(text) {
            let bytes = this.stringToBytes(text);
            return this.getCrcByBytes(bytes);
        }
        static getCrcByBytes(datas) {
            this.buildTable();
            let crc32 = 0xFFFFFFFF;
            if (null == datas || datas.length < 1) {
                return crc32;
            }
            for (let i = 0; i < datas.length; i++) {
                let data = datas[i];
                crc32 = (crc32 << 8) ^ CompatibleCRC32.PTI_TABLE[(crc32 >>> 24) ^ (data & 0xFF)];
            }
            return crc32;
        }
        static get32BitHex(text) {
            let dec = this.get32BitDec(text);
            let sb = "";
            sb += this.HEX_MAP[(dec >>> 28) & 0xF];
            sb += this.HEX_MAP[(dec >>> 24) & 0xF];
            sb += this.HEX_MAP[(dec >>> 20) & 0xF];
            sb += this.HEX_MAP[(dec >>> 16) & 0xF];
            sb += this.HEX_MAP[(dec >>> 12) & 0xF];
            sb += this.HEX_MAP[(dec >>> 8) & 0xF];
            sb += this.HEX_MAP[(dec >>> 4) & 0xF];
            sb += this.HEX_MAP[dec & 0xF];
            return sb;
        }
        static stringToBytes(str) {
            var bytes = new Array();
            var len, c;
            len = str.length;
            for (var i = 0; i < len; i++) {
                c = str.charCodeAt(i);
                if (c >= 0x010000 && c <= 0x10FFFF) {
                    bytes.push(((c >> 18) & 0x07) | 0xF0);
                    bytes.push(((c >> 12) & 0x3F) | 0x80);
                    bytes.push(((c >> 6) & 0x3F) | 0x80);
                    bytes.push((c & 0x3F) | 0x80);
                }
                else if (c >= 0x000800 && c <= 0x00FFFF) {
                    bytes.push(((c >> 12) & 0x0F) | 0xE0);
                    bytes.push(((c >> 6) & 0x3F) | 0x80);
                    bytes.push((c & 0x3F) | 0x80);
                }
                else if (c >= 0x000080 && c <= 0x0007FF) {
                    bytes.push(((c >> 6) & 0x1F) | 0xC0);
                    bytes.push((c & 0x3F) | 0x80);
                }
                else {
                    bytes.push(c & 0xFF);
                }
            }
            return bytes;
        }
        static buildTable() {
            if (this.PTI_TABLE != null) {
                return;
            }
            this.PTI_TABLE = new Array(256);
            let nData = 0;
            let nAccum = 0;
            for (let i = 0; i < 256; i++) {
                nData = i << 24;
                nAccum = 0;
                for (let j = 0; j < 8; j++) {
                    if (0 != ((nData ^ nAccum) & 0x80000000)) {
                        nAccum = (nAccum << 1) ^ this.CN;
                    }
                    else {
                        nAccum <<= 1;
                    }
                    nData <<= 1;
                }
                CompatibleCRC32.PTI_TABLE[i] = nAccum;
            }
        }
    }
    CompatibleCRC32.CN = 0x04C11DB7;
    CompatibleCRC32.HEX_MAP = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'];
    CompatibleCRC32.PTI_TABLE = null;

    class ExcelLibDataBase {
        getMaps() {
            if (this.maps == null) {
                this.maps = new Map();
            }
            return this.maps;
        }
    }

    class LibItemBase {
        constructor() { }
        getLib() {
            return null;
        }
    }

    class LibStar extends LibItemBase {
        constructor() { super(); }
        getId() { return this.id; }
        setId(id) { this.id = id; }
        getCid() { return this.cid; }
        setCid(cid) { this.cid = cid; }
        getLv() { return this.lv; }
        setLv(lv) { this.lv = lv; }
        getHp() { return this.hp; }
        setHp(hp) { this.hp = hp; }
        getAtk() { return this.atk; }
        setAtk(atk) { this.atk = atk; }
        getMag() { return this.mag; }
        setMag(mag) { this.mag = mag; }
        getSkid() { return this.skid; }
        setSkid(skid) { this.skid = skid; }
        getLibCharacter() {
            return super.getLib().characterMap(this.cid);
        }
        parseFromJson(json) {
            if (json == null || json == undefined) {
                return false;
            }
            this.id = json["id"];
            this.cid = json["cid"];
            this.lv = json["lv"];
            this.hp = json["hp"];
            this.atk = json["atk"];
            this.mag = json["mag"];
            this.skid = json["skid"];
            return true;
        }
        __getMainKey(dataFullName) {
            switch (dataFullName) {
                case "starMapById": return this.id;
            }
            return null;
        }
        toString() {
            return "LibStar(角色星级){" +
                "id=" + this.id +
                ", cid=" + this.cid +
                ", lv=" + this.lv +
                ", hp=" + this.hp +
                ", atk=" + this.atk +
                ", mag=" + this.mag +
                ", skid=" + this.skid +
                '}';
        }
    }

    class LibCharacter extends LibItemBase {
        constructor() { super(); }
        getCid() { return this.cid; }
        setCid(cid) { this.cid = cid; }
        getSname() { return this.sname; }
        setSname(sname) { this.sname = sname; }
        getOcId() { return this.ocId; }
        setOcId(ocId) { this.ocId = ocId; }
        getCtype() { return this.ctype; }
        setCtype(ctype) { this.ctype = ctype; }
        getGrade() { return this.grade; }
        setGrade(grade) { this.grade = grade; }
        getHp() { return this.hp; }
        setHp(hp) { this.hp = hp; }
        getAtk() { return this.atk; }
        setAtk(atk) { this.atk = atk; }
        getMag() { return this.mag; }
        setMag(mag) { this.mag = mag; }
        getMdf() { return this.mdf; }
        setMdf(mdf) { this.mdf = mdf; }
        getDf() { return this.df; }
        setDf(df) { this.df = df; }
        getCri() { return this.cri; }
        setCri(cri) { this.cri = cri; }
        getRng() { return this.rng; }
        setRng(rng) { this.rng = rng; }
        getAspd() { return this.aspd; }
        setAspd(aspd) { this.aspd = aspd; }
        getBulletSpeed() { return this.bulletSpeed; }
        setBulletSpeed(bulletSpeed) { this.bulletSpeed = bulletSpeed; }
        getIcon() { return this.icon; }
        setIcon(icon) { this.icon = icon; }
        getStartDelay() { return this.startDelay; }
        setStartDelay(startDelay) { this.startDelay = startDelay; }
        getMcDelay() { return this.mcDelay; }
        setMcDelay(mcDelay) { this.mcDelay = mcDelay; }
        getModel() { return this.model; }
        setModel(model) { this.model = model; }
        getHdModel() { return this.hdModel; }
        setHdModel(hdModel) { this.hdModel = hdModel; }
        getFnAtk() { return this.fnAtk; }
        setFnAtk(fnAtk) { this.fnAtk = fnAtk; }
        getFnDamage() { return this.fnDamage; }
        setFnDamage(fnDamage) { this.fnDamage = fnDamage; }
        getFnBullet() { return this.fnBullet; }
        setFnBullet(fnBullet) { this.fnBullet = fnBullet; }
        getFnExplo() { return this.fnExplo; }
        setFnExplo(fnExplo) { this.fnExplo = fnExplo; }
        getScale() { return this.scale; }
        setScale(scale) { this.scale = scale; }
        getAvatarModel() { return this.avatarModel; }
        setAvatarModel(avatarModel) { this.avatarModel = avatarModel; }
        parseFromJson(json) {
            if (json == null || json == undefined) {
                return false;
            }
            this.cid = json["cid"];
            this.sname = json["sname"];
            this.ocId = json["ocId"];
            this.ctype = json["ctype"];
            this.grade = json["grade"];
            this.hp = json["hp"];
            this.atk = json["atk"];
            this.mag = json["mag"];
            this.mdf = json["mdf"];
            this.df = json["df"];
            this.cri = json["cri"];
            this.rng = json["rng"];
            this.aspd = json["aspd"];
            this.bulletSpeed = json["bulletSpeed"];
            this.icon = json["icon"];
            this.startDelay = json["startDelay"];
            this.mcDelay = json["mcDelay"];
            this.model = json["model"];
            this.hdModel = json["hdModel"];
            this.fnAtk = json["fnAtk"];
            this.fnDamage = json["fnDamage"];
            this.fnBullet = json["fnBullet"];
            this.fnExplo = json["fnExplo"];
            this.scale = json["scale"];
            this.avatarModel = json["avatarModel"];
            return true;
        }
        __getMainKey(dataFullName) {
            switch (dataFullName) {
                case "characterMapByCid": return this.cid;
            }
            return null;
        }
        toString() {
            return "LibCharacter(角色){" +
                "cid=" + this.cid +
                ", sname='" + this.sname + '\'' +
                ", ocId=" + this.ocId +
                ", ctype=" + this.ctype +
                ", grade=" + this.grade +
                ", hp=" + this.hp +
                ", atk=" + this.atk +
                ", mag=" + this.mag +
                ", mdf=" + this.mdf +
                ", df=" + this.df +
                ", cri=" + this.cri +
                ", rng=" + this.rng +
                ", aspd=" + this.aspd +
                ", bulletSpeed=" + this.bulletSpeed +
                ", icon='" + this.icon + '\'' +
                ", startDelay=" + this.startDelay +
                ", mcDelay=" + this.mcDelay +
                ", model='" + this.model + '\'' +
                ", hdModel='" + this.hdModel + '\'' +
                ", fnAtk='" + this.fnAtk + '\'' +
                ", fnDamage='" + this.fnDamage + '\'' +
                ", fnBullet='" + this.fnBullet + '\'' +
                ", fnExplo='" + this.fnExplo + '\'' +
                ", scale=" + this.scale +
                ", avatarModel='" + this.avatarModel + '\'' +
                '}';
        }
    }

    class LibLevel extends LibItemBase {
        constructor() { super(); }
        getLv() { return this.lv; }
        setLv(lv) { this.lv = lv; }
        getElv() { return this.elv; }
        setElv(elv) { this.elv = elv; }
        getPskillCount() { return this.pskillCount; }
        setPskillCount(pskillCount) { this.pskillCount = pskillCount; }
        getOccLevel() { return this.occLevel; }
        setOccLevel(occLevel) { this.occLevel = occLevel; }
        parseFromJson(json) {
            if (json == null || json == undefined) {
                return false;
            }
            this.lv = json["lv"];
            this.elv = json["elv"];
            this.pskillCount = json["pskillCount"];
            this.occLevel = json["occLevel"];
            return true;
        }
        __getMainKey(dataFullName) {
            switch (dataFullName) {
                case "levelMapByLv": return this.lv;
                case "levelList": return null;
            }
            return null;
        }
        toString() {
            return "LibLevel(等级){" +
                "lv=" + this.lv +
                ", elv=" + this.elv +
                ", pskillCount=" + this.pskillCount +
                ", occLevel=" + this.occLevel +
                '}';
        }
    }

    class LibCustomerLevel extends LibItemBase {
        constructor() { super(); }
        getCustomerLv() { return this.customerLv; }
        setCustomerLv(customerLv) { this.customerLv = customerLv; }
        getUpgradeType() { return this.upgradeType; }
        setUpgradeType(upgradeType) { this.upgradeType = upgradeType; }
        getPurchasing() { return this.purchasing; }
        setPurchasing(purchasing) { this.purchasing = purchasing; }
        parseFromJson(json) {
            if (json == null || json == undefined) {
                return false;
            }
            this.customerLv = json["customerLv"];
            this.upgradeType = json["upgradeType"];
            this.purchasing = json["purchasing"];
            return true;
        }
        __getMainKey(dataFullName) {
            switch (dataFullName) {
                case "customerLevelMkMapByCustomerLvUpgradeType": return this.customerLv + "_" + this.upgradeType + "_";
            }
            return null;
        }
        toString() {
            return "LibCustomerLevel(顾客等级表){" +
                "customerLv=" + this.customerLv +
                ", upgradeType=" + this.upgradeType +
                ", purchasing=" + this.purchasing +
                '}';
        }
    }

    class LibTeam extends LibItemBase {
        constructor() { super(); }
        getId() { return this.id; }
        setId(id) { this.id = id; }
        getSeat() { return this.seat; }
        setSeat(seat) { this.seat = seat; }
        getStarId() { return this.starId; }
        setStarId(starId) { this.starId = starId; }
        getStarLv() { return this.starLv; }
        setStarLv(starLv) { this.starLv = starLv; }
        getCid() { return this.cid; }
        setCid(cid) { this.cid = cid; }
        getWeight() { return this.weight; }
        setWeight(weight) { this.weight = weight; }
        getLv() { return this.lv; }
        setLv(lv) { this.lv = lv; }
        getBoss() { return this.boss; }
        setBoss(boss) { this.boss = boss; }
        getHp() { return this.hp; }
        setHp(hp) { this.hp = hp; }
        getAtk() { return this.atk; }
        setAtk(atk) { this.atk = atk; }
        getMag() { return this.mag; }
        setMag(mag) { this.mag = mag; }
        getMdf() { return this.mdf; }
        setMdf(mdf) { this.mdf = mdf; }
        getDf() { return this.df; }
        setDf(df) { this.df = df; }
        getCri() { return this.cri; }
        setCri(cri) { this.cri = cri; }
        isLelead() { return this.isLead; }
        setLead(isLead) { this.isLead = isLead; }
        isTeam() { return this.team; }
        setTeam(team) { this.team = team; }
        parseFromJson(json) {
            if (json == null || json == undefined) {
                return false;
            }
            this.id = json["id"];
            this.seat = json["seat"];
            this.starId = json["starId"];
            this.starLv = json["starLv"];
            this.cid = json["cid"];
            this.weight = json["weight"];
            this.lv = json["lv"];
            this.boss = json["boss"];
            this.hp = json["hp"];
            this.atk = json["atk"];
            this.mag = json["mag"];
            this.mdf = json["mdf"];
            this.df = json["df"];
            this.cri = json["cri"];
            this.isLead = json["isLead"];
            this.team = json["team"];
            return true;
        }
        __getMainKey(dataFullName) {
            switch (dataFullName) {
                case "teamMapListById": return this.id;
            }
            return null;
        }
        toString() {
            return "LibTeam(怪物队伍表){" +
                "id=" + this.id +
                ", seat=" + this.seat +
                ", starId=" + this.starId +
                ", starLv=" + this.starLv +
                ", cid=" + this.cid +
                ", weight=" + this.weight +
                ", lv=" + this.lv +
                ", boss=" + this.boss +
                ", hp=" + this.hp +
                ", atk=" + this.atk +
                ", mag=" + this.mag +
                ", mdf=" + this.mdf +
                ", df=" + this.df +
                ", cri=" + this.cri +
                ", isLead=" + this.isLead +
                ", team=" + this.team +
                '}';
        }
    }

    class LibSetting extends LibItemBase {
        constructor() { super(); }
        getKey() { return this.key; }
        setKey(key) { this.key = key; }
        getDesc() { return this.desc; }
        setDesc(desc) { this.desc = desc; }
        isBoolVal() { return this.boolVal; }
        setBoolVal(boolVal) { this.boolVal = boolVal; }
        getIntVal() { return this.intVal; }
        setIntVal(intVal) { this.intVal = intVal; }
        getFloatVal() { return this.floatVal; }
        setFloatVal(floatVal) { this.floatVal = floatVal; }
        getStrVal() { return this.strVal; }
        setStrVal(strVal) { this.strVal = strVal; }
        parseFromJson(json) {
            if (json == null || json == undefined) {
                return false;
            }
            this.key = json["key"];
            this.desc = json["desc"];
            this.boolVal = json["boolVal"];
            this.intVal = json["intVal"];
            this.floatVal = json["floatVal"];
            this.strVal = json["strVal"];
            return true;
        }
        __getMainKey(dataFullName) {
            switch (dataFullName) {
                case "settingMapByKey": return this.key;
            }
            return null;
        }
        toString() {
            return "LibSetting(单键值配置){" +
                "key='" + this.key + '\'' +
                ", desc='" + this.desc + '\'' +
                ", boolVal=" + this.boolVal +
                ", intVal=" + this.intVal +
                ", floatVal=" + this.floatVal +
                ", strVal='" + this.strVal + '\'' +
                '}';
        }
    }

    class LibSettingsComplex extends LibItemBase {
        constructor() { super(); }
        getKey() { return this.key; }
        setKey(key) { this.key = key; }
        getDesc() { return this.desc; }
        setDesc(desc) { this.desc = desc; }
        isB1() { return this.b1; }
        setB1(b1) { this.b1 = b1; }
        isB2() { return this.b2; }
        setB2(b2) { this.b2 = b2; }
        getI1() { return this.i1; }
        setI1(i1) { this.i1 = i1; }
        getI2() { return this.i2; }
        setI2(i2) { this.i2 = i2; }
        getF1() { return this.f1; }
        setF1(f1) { this.f1 = f1; }
        getF2() { return this.f2; }
        setF2(f2) { this.f2 = f2; }
        getS1() { return this.s1; }
        setS1(s1) { this.s1 = s1; }
        getS2() { return this.s2; }
        setS2(s2) { this.s2 = s2; }
        parseFromJson(json) {
            if (json == null || json == undefined) {
                return false;
            }
            this.key = json["key"];
            this.desc = json["desc"];
            this.b1 = json["b1"];
            this.b2 = json["b2"];
            this.i1 = json["i1"];
            this.i2 = json["i2"];
            this.f1 = json["f1"];
            this.f2 = json["f2"];
            this.s1 = json["s1"];
            this.s2 = json["s2"];
            return true;
        }
        __getMainKey(dataFullName) {
            switch (dataFullName) {
                case "settingsComplexMapListByKey": return this.key;
            }
            return null;
        }
        toString() {
            return "LibSettingsComplex(复合配置){" +
                "key='" + this.key + '\'' +
                ", desc='" + this.desc + '\'' +
                ", b1=" + this.b1 +
                ", b2=" + this.b2 +
                ", i1=" + this.i1 +
                ", i2=" + this.i2 +
                ", f1=" + this.f1 +
                ", f2=" + this.f2 +
                ", s1='" + this.s1 + '\'' +
                ", s2='" + this.s2 + '\'' +
                '}';
        }
    }

    class ExcelLibDataEasyAccess extends ExcelLibDataBase {
        static voFactory(className) {
            switch (className) {
                case "LibStar": return new LibStar();
                case "LibCharacter": return new LibCharacter();
                case "LibLevel": return new LibLevel();
                case "LibCustomerLevel": return new LibCustomerLevel();
                case "LibTeam": return new LibTeam();
                case "LibSetting": return new LibSetting();
                case "LibSettingsComplex": return new LibSettingsComplex();
            }
            return null;
        }
        starMap(id) {
            let data = this.maps.get("starMapById");
            return data == null ? null : data.get(id);
        }
        characterMap(cid) {
            let data = this.maps.get("characterMapByCid");
            return data == null ? null : data.get(cid);
        }
        levelMap(lv) {
            let data = this.maps.get("levelMapByLv");
            return data == null ? null : data.get(lv);
        }
        levelList() {
            return this.maps.get("levelList");
        }
        customerLevelMkMap(customerLv, upgradeType) {
            let data = this.maps.get("customerLevelMkMapByCustomerLvUpgradeType");
            return data == null ? null : data.get(customerLv + "_" + upgradeType + "_");
        }
        teamMapList(id) {
            let data = this.maps.get("teamMapListById");
            return data == null ? null : data.get(id);
        }
        settingMap(key) {
            let data = this.maps.get("settingMapByKey");
            return data == null ? null : data.get(key);
        }
        settingsComplexMapList(key) {
            let data = this.maps.get("settingsComplexMapListByKey");
            return data == null ? null : data.get(key);
        }
    }

    class ExcelLibData extends ExcelLibDataEasyAccess {
    }

    class ExcelLibDefinition {
        constructor() {
            this.mainKeys = null;
        }
        static parseFromJson(json) {
            let e = new ExcelLibDefinition();
            e.className = json["className"];
            e.comment = json["comment"];
            e.dataFullName = json["dataFullName"];
            e.dataName = json["dataName"];
            e.group = json["group"];
            e.loadJson = json["loadJson"];
            e.loadWeight = json["loadWeight"];
            e.mainKey = json["mainKey"];
            e.mainKeyType = json["mainKeyType"];
            e.type = json["type"];
            e.keys = new Array();
            let keysJson = json["keys"];
            for (let keyJson of keysJson) {
                let name = keyJson["name"];
                let type = keyJson["type"];
                let keyMap = new Map();
                keyMap.set(name, type);
                e.keys.push(keyMap);
            }
            e.loadRelations = new Array();
            let relJson = json["loadRelations"];
            for (let relation of relJson) {
                e.loadRelations.push(relation);
            }
            e.platforms = new Array();
            let platformsJson = json["platforms"];
            for (let platform of platformsJson) {
                e.platforms.push(platform);
            }
            return e;
        }
        getMainKeys(forceLoad = false) {
            if (this.mainKeys == null || forceLoad) {
                this.mainKeys = new String[this.keys.length];
                for (let i = 0; i < this.keys.length; i++) {
                    this.mainKeys[i] = this.keys[i].get("name");
                }
            }
            return this.mainKeys;
        }
        getMainKey() {
            return this.mainKey;
        }
        setMainKey(mainKey) {
            this.mainKey = mainKey;
        }
        getKeys() {
            return this.keys;
        }
        setKeys(keys) {
            this.keys = keys;
        }
        getMainKeyType() {
            return this.mainKeyType;
        }
        setMainKeyType(mainKeyType) {
            this.mainKeyType = mainKeyType;
        }
        getType() {
            return this.type;
        }
        setType(type) {
            this.type = type;
        }
        getPlatforms() {
            return this.platforms;
        }
        setPlatforms(platforms) {
            this.platforms = platforms;
        }
        getLoadWeight() {
            return this.loadWeight;
        }
        setLoadWeight(loadWeight) {
            this.loadWeight = loadWeight;
        }
        getLoadJson() {
            return this.loadJson;
        }
        setLoadJson(loadJson) {
            this.loadJson = loadJson;
        }
        getComment() {
            return this.comment;
        }
        setComment(comment) {
            this.comment = comment;
        }
        getClassName() {
            return this.className;
        }
        setClassName(className) {
            this.className = className;
        }
        getDataName() {
            return this.dataName;
        }
        getGroup() {
            return this.group;
        }
        setGroup(group) {
            this.group = group;
        }
        getLoadRelations() {
            return this.loadRelations;
        }
        setLoadRelations(loadRelations) {
            this.loadRelations = loadRelations;
        }
        setDataName(dataName) {
            this.dataName = dataName;
        }
        getDataFullName() {
            return this.dataFullName;
        }
        setDataFullName(dataFullName) {
            this.dataFullName = dataFullName;
        }
    }
    ExcelLibDefinition.FIELD_TYPE_INTEGER = "Integer";
    ExcelLibDefinition.FIELD_TYPE_LONG = "Long";
    ExcelLibDefinition.FIELD_TYPE_STRING = "String";
    ExcelLibDefinition.FIELD_TYPE_FLOAT = "Float";
    ExcelLibDefinition.FIELD_TYPE_DOUBLE = "Double";
    ExcelLibDefinition.FIELD_TYPE_BOOL = "Boolean";
    ExcelLibDefinition.LOAD_TYPE_LIST = "List";
    ExcelLibDefinition.LOAD_TYPE_MAP = "Map";
    ExcelLibDefinition.LOAD_TYPE_MKMAP = "MultKeyMap";
    ExcelLibDefinition.LOAD_TYPE_MAP2LIST = "Map2List";

    class ExcelLibDefinitionTable {
        static parseFromJson(json) {
            let e = new ExcelLibDefinitionTable();
            e.jsonName = json["jsonName"];
            e.comment = json["comment"];
            e.md5 = json["md5"];
            e.date = json["date"];
            e.version = json["version"];
            e.author = json["author"];
            return e;
        }
        getJsonName() {
            return this.jsonName;
        }
        setJsonName(jsonName) {
            this.jsonName = jsonName;
        }
        getComment() {
            return this.comment;
        }
        setComment(comment) {
            this.comment = comment;
        }
        getMd5() {
            return this.md5;
        }
        setMd5(md5) {
            this.md5 = md5;
        }
        getDate() {
            return this.date;
        }
        setDate(date) {
            this.date = date;
        }
        getVersion() {
            return this.version;
        }
        setVersion(version) {
            this.version = version;
        }
        getAuthor() {
            return this.author;
        }
        setAuthor(author) {
            this.author = author;
        }
    }

    class ExcelLibTools {
        static loadJsonList(jsonData, df) {
            let list = new Array();
            let jsonArr = jsonData["data"];
            for (let json of jsonArr) {
                let vo = ExcelLibDataEasyAccess.voFactory(df.getClassName());
                vo.parseFromJson(json);
                let mainKey = vo.__getMainKey(df.getDataFullName());
                list.push(vo);
            }
            return list;
        }
        static loadJsonIntMap(jsonData, df) {
            let map = new Map();
            let jsonArr = jsonData["data"];
            for (let json of jsonArr) {
                let vo = ExcelLibDataEasyAccess.voFactory(df.getClassName());
                vo.parseFromJson(json);
                let mainKey = vo.__getMainKey(df.getDataFullName());
                map.set(mainKey, vo);
            }
            return map;
        }
        static loadJsonStringMap(jsonData, df) {
            let map = new Map();
            let jsonArr = jsonData["data"];
            for (let json of jsonArr) {
                let vo = ExcelLibDataEasyAccess.voFactory(df.getClassName());
                vo.parseFromJson(json);
                let mainKey = vo.__getMainKey(df.getDataFullName());
                map.set(mainKey, vo);
            }
            return map;
        }
        static loadJsonMapSetByIntegerKey(jsonData, df) {
            let map = new Map();
            let jsonArr = jsonData["data"];
            for (let json of jsonArr) {
                let vo = ExcelLibDataEasyAccess.voFactory(df.getClassName());
                vo.parseFromJson(json);
                let mainKey = vo.__getMainKey(df.getDataFullName());
                let keyList;
                if (map.has(mainKey)) {
                    keyList = map.get(mainKey);
                }
                else {
                    keyList = new Array();
                    map.set(mainKey, keyList);
                }
                keyList.push(vo);
            }
            return map;
        }
        static loadJsonMapSetByStringKey(jsonData, df) {
            let map = new Map();
            let jsonArr = jsonData["data"];
            for (let json of jsonArr) {
                let vo = ExcelLibDataEasyAccess.voFactory(df.getClassName());
                vo.parseFromJson(json);
                let mainKey = vo.__getMainKey(df.getDataFullName());
                let keyList;
                if (map.has(mainKey)) {
                    keyList = map.get(mainKey);
                }
                else {
                    keyList = new Array();
                    map.set(mainKey, keyList);
                }
                keyList.push(vo);
            }
            return map;
        }
        static loadJsonMultKeyMap(jsonData, df) {
            let map = new Map();
            let jsonArr = jsonData["data"];
            for (let json of jsonArr) {
                let vo = ExcelLibDataEasyAccess.voFactory(df.getClassName());
                vo.parseFromJson(json);
                let keyStr = "";
                for (let keySet of df.getKeys()) {
                    for (let [keyName, keyType] of keySet) {
                        let key = json[keyName];
                        keyStr += key.toString() + "_";
                    }
                }
                map.set(keyStr, vo);
            }
            return map;
        }
    }

    class HeaderData {
        static parseFromJson(json) {
            let e = new HeaderData();
            e.version = json["version"];
            e.date = json["date"];
            e.author = json["author"];
            e.count = json["count"];
            e.md5 = json["md5"];
            return e;
        }
        getVersion() {
            return this.version;
        }
        getDate() {
            return this.date;
        }
        getAuthor() {
            return this.author;
        }
        getCount() {
            return this.count;
        }
        getMd5() {
            return this.md5;
        }
    }

    class ExcelLibReader {
        constructor(libData) {
            this.namespace = null;
            this.serverHost = "";
            this.isLoadFromCfgServer = false;
            this.libData = libData;
        }
        setLoadFromConfigServer(serverHost) {
            this.serverHost = serverHost;
            this.isLoadFromCfgServer = true;
        }
        setLoadFromResServer() {
            this.isLoadFromCfgServer = false;
        }
        loadDefinition(callBack) {
            this.loading = true;
            this.callbackDefinitionLoaded = callBack;
            Laya.loader.retryNum = 9;
            let url;
            if (this.isLoadFromCfgServer) {
                url = this.serverHost + ExcelLibReader.SERVER_DEFINITION_URL;
            }
            else {
                url = ExcelLibReader.FILE_RES_PATH + ExcelLibReader.FILE_DEFINITION;
            }
            Laya.loader.load(url, Laya.Handler.create(this, this.onDefinitionLoaded, [ExcelLibReader.FILE_DEFINITION]), null, Laya.Loader.JSON);
            return true;
        }
        onDefinitionLoaded(resourceName, json) {
            console.log("onDefinitionLoaded -> " + resourceName);
            EventSystem.getInstance().dispatch(ExcelLib.EVENT_LIB_DEFINITION_DOWNLOADED);
            this.header = HeaderData.parseFromJson(json["header"]);
            const defines = json["defines"];
            let definitions = new Map();
            let excelLibDefinitions = new Array();
            for (const define of defines) {
                let edf = ExcelLibDefinition.parseFromJson(define);
                definitions.set(edf.getDataFullName(), edf);
                excelLibDefinitions.push(edf);
            }
            excelLibDefinitions.sort((a, b) => {
                return a.getLoadWeight() - b.getLoadWeight();
            });
            const tables = json["tables"];
            let definitionTables = new Map();
            for (const table of tables) {
                let tb = ExcelLibDefinitionTable.parseFromJson(table);
                definitionTables.set(tb.getJsonName(), tb);
            }
            this.definitions = definitions;
            this.excelLibDefinitions = excelLibDefinitions;
            this.definitionTables = definitionTables;
            console.log("加载excelLibDefinitions完成。 版本：" + this.header.getVersion() + " MD5：" + this.header.getMd5());
            this.loading = false;
            if (this.callbackDefinitionLoaded != null && this.callbackDefinitionLoaded != undefined) {
                this.callbackDefinitionLoaded.runWith(true);
            }
            EventSystem.getInstance().dispatch(ExcelLib.EVENT_LIB_DEFINITION_COMPLETED);
        }
        parseFromDownloadJson(df, json) {
            let result = null;
            switch (df.getType()) {
                case ExcelLibDefinition.LOAD_TYPE_LIST:
                    result = ExcelLibTools.loadJsonList(json, df);
                    break;
                case ExcelLibDefinition.LOAD_TYPE_MAP:
                    if (df.getMainKeyType() == ExcelLibDefinition.FIELD_TYPE_INTEGER) {
                        result = ExcelLibTools.loadJsonIntMap(json, df);
                    }
                    else if (df.getMainKeyType() == ExcelLibDefinition.FIELD_TYPE_STRING) {
                        result = ExcelLibTools.loadJsonStringMap(json, df);
                    }
                    break;
                case ExcelLibDefinition.LOAD_TYPE_MAP2LIST:
                    if (df.getMainKeyType() == ExcelLibDefinition.FIELD_TYPE_INTEGER) {
                        result = ExcelLibTools.loadJsonMapSetByIntegerKey(json, df);
                    }
                    else if (df.getMainKeyType() == ExcelLibDefinition.FIELD_TYPE_STRING) {
                        result = ExcelLibTools.loadJsonMapSetByStringKey(json, df);
                    }
                    break;
                case ExcelLibDefinition.LOAD_TYPE_MKMAP:
                    result = ExcelLibTools.loadJsonMultKeyMap(json, df);
                    break;
                default:
                    console.log("配置数据包加载：加载索引方式类型错误：" + df.getType());
                    return false;
            }
            let table = this.definitionTables.get(df.getLoadJson());
            if (result != null) {
                this.libData.getMaps().set(df.getDataFullName(), result);
                console.log("加载表[" + table.getComment() + "](" + table.getJsonName() + ") -> " + df.getDataFullName() + " 完成。 版本：" + table.getVersion() + " MD5：" + table.getMd5());
                EventSystem.getInstance().dispatch(ExcelLib.EVENT_LIB_LOAD_COMPLETED, [df.getDataFullName(), result]);
                return true;
            }
            else {
                console.log("加载表[" + table.getComment() + "](" + table.getJsonName() + ") -> " + df.getDataFullName() + " 失败!! 版本：" + table.getVersion() + " MD5：" + table.getMd5());
                EventSystem.getInstance().dispatch(ExcelLib.EVENT_LIB_LOAD_FAIL, [df.getDataFullName()]);
                return false;
            }
        }
        loadAllData() {
            if (this.loading) {
                return false;
            }
            let res = new Array();
            this.loadList = new Array();
            for (let i = 0; i < this.excelLibDefinitions.length; i++) {
                let df = this.excelLibDefinitions[i];
                let questUrl;
                if (this.isLoadFromCfgServer) {
                    questUrl = this.serverHost + ExcelLibReader.SERVER_TABLE_URL + df.getLoadJson() + ".json";
                }
                else {
                    questUrl = ExcelLibReader.FILE_RES_PATH + df.getLoadJson() + ".json";
                }
                res.push({
                    url: questUrl,
                    type: Laya.Loader.JSON
                });
                this.loadList.push(df);
            }
            let count = res.length;
            this.loading = true;
            Laya.loader.load(res, Laya.Handler.create(this, this.onLoadComplete), Laya.Handler.create(this, this.onProgress, null, false));
            EventSystem.getInstance().dispatch(ExcelLib.EVENT_LIB_LOAD_ALL_START, [this.loadList, count]);
            return true;
        }
        onProgress(process) {
            EventSystem.getInstance().dispatch(ExcelLib.EVENT_LIB_LOAD_PROCESS, [process]);
        }
        onLoadComplete(isSuccess) {
            console.log("loadComplete:" + isSuccess);
            if (!isSuccess) {
                this.loadList = null;
                this.loading = false;
                EventSystem.getInstance().dispatch(ExcelLib.EVENT_LIB_LOAD_DOWNLOADED, [this.loadList, false]);
                return;
            }
            else {
                EventSystem.getInstance().dispatch(ExcelLib.EVENT_LIB_LOAD_DOWNLOADED, [this.loadList, true]);
            }
            this.loadList.sort((a, b) => {
                return a.getLoadWeight() - b.getLoadWeight();
            });
            let result = true;
            for (let i = 0; i < this.loadList.length; i++) {
                let df = this.loadList[i];
                let questUrl;
                if (this.isLoadFromCfgServer) {
                    questUrl = this.serverHost + ExcelLibReader.SERVER_TABLE_URL + df.getLoadJson() + ".json";
                }
                else {
                    questUrl = ExcelLibReader.FILE_RES_PATH + df.getLoadJson() + ".json";
                }
                let json = Laya.loader.getRes(questUrl);
                result = result && this.parseFromDownloadJson(df, json);
            }
            this.loadList = null;
            this.loading = false;
            EventSystem.getInstance().dispatch(ExcelLib.EVENT_LIB_LOAD_ALL_COMPLETED, [this.loadList, result]);
        }
        loadFromGroup(gruopName) {
            if (this.loading) {
                return -1;
            }
            this.loadList = new Array();
            for (let i = 0; i < this.excelLibDefinitions.length; i++) {
                let df = this.excelLibDefinitions[i];
                if (gruopName == df.getGroup()) {
                    this.loadList.push(df);
                }
            }
            let res = new Array();
            for (let df2 of this.loadList) {
                let questUrl;
                if (this.isLoadFromCfgServer) {
                    questUrl = this.serverHost + ExcelLibReader.SERVER_TABLE_URL + df2.getLoadJson() + ".json";
                }
                else {
                    questUrl = ExcelLibReader.FILE_RES_PATH + df2.getLoadJson() + ".json";
                }
                res.push({
                    url: questUrl,
                    type: Laya.Loader.JSON
                });
            }
            let count = res.length;
            this.loading = true;
            Laya.loader.load(res, Laya.Handler.create(this, this.onLoadComplete), Laya.Handler.create(this, this.onProgress, null, false));
            EventSystem.getInstance().dispatch(ExcelLib.EVENT_LIB_LOAD_GROUP_START, [this.loadList, count]);
            return count;
        }
        loadFromFileByName(dataFullName, isLoadRelations) {
            if (this.loading) {
                return 0;
            }
            let df = this.definitions.get(dataFullName);
            if (df == null) {
                console.log("加载错误的Excel数据，数据表全名：" + dataFullName);
                return null;
            }
            this.loadList = new Array();
            if (isLoadRelations == false) {
                this.loadList.push(df);
            }
            else {
                let loadDefinitions = new Map();
                this.recursionSearchRelation(loadDefinitions, df);
                for (let df2 of loadDefinitions.values()) {
                    this.loadList.push(df2);
                }
            }
            let res = new Array();
            for (let df2 of this.loadList) {
                let questUrl;
                if (this.isLoadFromCfgServer) {
                    questUrl = this.serverHost + ExcelLibReader.SERVER_TABLE_URL + df2.getLoadJson() + ".json";
                }
                else {
                    questUrl = ExcelLibReader.FILE_RES_PATH + df2.getLoadJson() + ".json";
                }
                res.push({
                    url: questUrl,
                    type: Laya.Loader.JSON
                });
            }
            let count = res.length;
            this.loading = true;
            Laya.loader.load(res, Laya.Handler.create(this, this.onLoadComplete), Laya.Handler.create(this, this.onProgress, null, false));
            EventSystem.getInstance().dispatch(ExcelLib.EVENT_LIB_LOAD_START, [this.loadList, count]);
        }
        recursionSearchRelation(loadDefinitions, current) {
            loadDefinitions.set(current.getDataFullName(), current);
            for (let rJsonName of current.getLoadRelations()) {
                for (let searchDefinition of this.excelLibDefinitions) {
                    if (rJsonName == searchDefinition.getLoadJson()) {
                        if (!loadDefinitions.has(searchDefinition.getDataFullName())) {
                            this.recursionSearchRelation(loadDefinitions, searchDefinition);
                        }
                    }
                }
            }
        }
        getServerHost() {
            return this.serverHost;
        }
        setServerHost(serverHost) {
            this.serverHost = serverHost;
        }
    }
    ExcelLibReader.FILE_RES_PATH = "lib/";
    ExcelLibReader.FILE_DEFINITION = "_definition.json";
    ExcelLibReader.SERVER_DEFINITION_URL = "sys/definition";
    ExcelLibReader.SERVER_TABLE_URL = "sys/getData?name=";
    ExcelLibReader.SERVER_VERSION_URL = "sys/version";

    class ExcelLib {
        constructor() {
            this.loadedDefinition = false;
            console.log("创建了一个ExcelLib");
            this.libData = new ExcelLibData();
            this.libReader = new ExcelLibReader(this.libData);
        }
        static getInstance() {
            if (ExcelLib.instance == null) {
                ExcelLib.instance = new ExcelLib();
            }
            return ExcelLib.instance;
        }
        reloadDefinition() {
            let handler = Laya.Handler.create(this, this.onLibDefinitionCompleted);
            this.libReader.loadDefinition(handler);
        }
        setLoadFromConfigServer(serverHost) {
            this.libReader.setLoadFromConfigServer(serverHost);
        }
        setLoadFromResServer() {
            this.libReader.setLoadFromResServer();
        }
        loadFromFileByName(dataFullName, isLoadRelations = false) {
            this.getLibReader().loadFromFileByName(dataFullName, isLoadRelations);
        }
        loadAllData() {
            return this.getLibReader().loadAllData();
        }
        loadFromGroup(gruopName) {
            return this.getLibReader().loadFromGroup(gruopName);
        }
        getLibReader() {
            return this.libReader;
        }
        data() {
            return this.libData;
        }
        onLibDefinitionCompleted(result) {
            ExcelLib.getInstance().setLoadedDefinition(result);
        }
        isLoadedDefinition() {
            return this.loadedDefinition;
        }
        setLoadedDefinition(state) {
            this.loadedDefinition = state;
        }
    }
    ExcelLib.EVENT_LIB_DEFINITION_DOWNLOADED = "EVENT_LIB_DEFINITION_DOWNLOADED";
    ExcelLib.EVENT_LIB_DEFINITION_COMPLETED = "EVENT_LIB_DEFINITION_COMPLETED";
    ExcelLib.EVENT_LIB_LOAD_START = "EVENT_LIB_LOAD_START";
    ExcelLib.EVENT_LIB_LOAD_ALL_START = "EVENT_LIB_LOAD_ALL_START";
    ExcelLib.EVENT_LIB_LOAD_GROUP_START = "EVENT_LIB_LOAD_GROUP_START";
    ExcelLib.EVENT_LIB_LOAD_DOWNLOADED = "EVENT_LIB_LOAD_DOWNLOADED";
    ExcelLib.EVENT_LIB_LOAD_COMPLETED = "EVENT_LIB_LOAD_COMPLETED";
    ExcelLib.EVENT_LIB_LOAD_FAIL = "EVENT_LIB_LOAD_FAIL";
    ExcelLib.EVENT_LIB_LOAD_PROCESS = "EVENT_LIB_LOAD_ALL_PROCESS";
    ExcelLib.EVENT_LIB_LOAD_ALL_COMPLETED = "EVENT_LIB_LOAD_ALL_COMPLETED";
    ExcelLib.instance = null;

    class ExcelLibConstant {
    }
    ExcelLibConstant.GROUP_CHARACTERS = "group_characters";
    ExcelLibConstant.GROUP_CHAPTER = "group_chapter";
    ExcelLibConstant.GROUP_SETTING = "group_setting";
    ExcelLibConstant.TB_STAR_MAP_ID = "starMapById";
    ExcelLibConstant.TB_CHARACTER_MAP_CID = "characterMapByCid";
    ExcelLibConstant.TB_LEVEL_MAP_LV = "levelMapByLv";
    ExcelLibConstant.TB_LEVEL_LIST = "levelList";
    ExcelLibConstant.TB_CUSTOMERLEVEL_MKMAP_CUSTOMERLV_UPGRADETYPE = "customerLevelMkMapByCustomerLvUpgradeType";
    ExcelLibConstant.TB_TEAM_MAPLIST_ID = "teamMapListById";
    ExcelLibConstant.TB_SETTING_MAP_KEY = "settingMapByKey";
    ExcelLibConstant.TB_SETTINGSCOMPLEX_MAPLIST_KEY = "settingsComplexMapListByKey";

    class Main {
        constructor() {
            if (window["Laya3D"])
                Laya3D.init(GameConfig.width, GameConfig.height);
            else
                Laya.init(GameConfig.width, GameConfig.height, Laya["WebGL"]);
            Laya["Physics"] && Laya["Physics"].enable();
            Laya["DebugPanel"] && Laya["DebugPanel"].enable();
            Laya.stage.scaleMode = GameConfig.scaleMode;
            Laya.stage.screenMode = GameConfig.screenMode;
            Laya.stage.alignV = GameConfig.alignV;
            Laya.stage.alignH = GameConfig.alignH;
            Laya.URL.exportSceneToJson = GameConfig.exportSceneToJson;
            if (GameConfig.debug || Laya.Utils.getQueryString("debug") == "true")
                Laya.enableDebugPanel();
            if (GameConfig.physicsDebug && Laya["PhysicsDebugDraw"])
                Laya["PhysicsDebugDraw"].enable();
            if (GameConfig.stat)
                Laya.Stat.show();
            Laya.alertGlobalError(true);
            Laya.ResourceVersion.enable("version.json", Laya.Handler.create(this, this.onVersionLoaded), Laya.ResourceVersion.FILENAME_VERSION);
        }
        onVersionLoaded() {
            Laya.AtlasInfoManager.enable("fileconfig.json", Laya.Handler.create(this, this.onConfigLoaded));
        }
        onConfigLoaded() {
            GameConfig.startScene && Laya.Scene.open(GameConfig.startScene);
            console.log("开始");
            EventSystem.getInstance().addEventListener(ExcelLib.EVENT_LIB_DEFINITION_DOWNLOADED, this, this.onLibDefinitionDownloaded);
            EventSystem.getInstance().addEventListener(ExcelLib.EVENT_LIB_DEFINITION_COMPLETED, this, this.onLibDefinitionCompleted);
            EventSystem.getInstance().addEventListener(ExcelLib.EVENT_LIB_LOAD_START, this, this.onLibLoadStart);
            EventSystem.getInstance().addEventListener(ExcelLib.EVENT_LIB_LOAD_ALL_START, this, this.onLibLoadAllStart);
            EventSystem.getInstance().addEventListener(ExcelLib.EVENT_LIB_LOAD_GROUP_START, this, this.onLibLoadGroupStart);
            EventSystem.getInstance().addEventListener(ExcelLib.EVENT_LIB_LOAD_PROCESS, this, this.onLibLoadProcess);
            EventSystem.getInstance().addEventListener(ExcelLib.EVENT_LIB_LOAD_DOWNLOADED, this, this.onLibLoadDownloaded);
            EventSystem.getInstance().addEventListener(ExcelLib.EVENT_LIB_LOAD_COMPLETED, this, this.onLibLoadCompleted);
            EventSystem.getInstance().addEventListener(ExcelLib.EVENT_LIB_LOAD_FAIL, this, this.onLibLoadFail);
            EventSystem.getInstance().addEventListener(ExcelLib.EVENT_LIB_LOAD_ALL_COMPLETED, this, this.onLibLoadAllCompleted);
            let testCode = "This is a Test!!";
            let crc = CompatibleCRC32.get32BitDec(testCode);
            console.log("CRC:" + crc.toString());
            console.log("CRC:" + Math.abs(crc).toString(16));
            testCode = "这是个测试Hash的例子";
            crc = CompatibleCRC32.get32BitDec(testCode);
            console.log("CRC:" + crc.toString());
            testCode = "This is a 例子!!";
            crc = CompatibleCRC32.get32BitDec(testCode);
            console.log("CRC:" + crc.toString());
            let scrc = CompatibleCRC32.get32BitHex(testCode);
            console.log(scrc);
            let lib = ExcelLib.getInstance();
            lib.setLoadFromConfigServer(Main.CFG_SERVER_HOST);
            lib.reloadDefinition();
        }
        onLibDefinitionDownloaded() {
            console.log("Event: 下载定义文件_definition.json完毕!");
        }
        onLibDefinitionCompleted() {
            console.log("Event: 解析定义文件_definition.json完毕!");
            console.log("ExcelLib.getInstance().isLoadedDefinition:" + ExcelLib.getInstance().isLoadedDefinition());
            ExcelLib.getInstance().loadAllData();
            Laya.timer.once(500, this, () => {
                ExcelLib.getInstance().loadFromGroup(ExcelLibConstant.GROUP_CHARACTERS);
            });
            Laya.timer.once(1000, this, () => {
                ExcelLib.getInstance().loadFromFileByName(ExcelLibConstant.TB_STAR_MAP_ID, false);
            });
            Laya.timer.once(1500, this, () => {
                ExcelLib.getInstance().loadFromFileByName(ExcelLibConstant.TB_LEVEL_MAP_LV, true);
            });
            Laya.timer.once(2000, this, () => {
                ExcelLib.getInstance().loadFromFileByName(ExcelLibConstant.TB_STAR_MAP_ID, true);
            });
            Laya.timer.once(2500, this, () => {
                ExcelLib.getInstance().loadFromFileByName(ExcelLibConstant.TB_CUSTOMERLEVEL_MKMAP_CUSTOMERLV_UPGRADETYPE, true);
            });
        }
        onLibLoadStart(loadList, count) {
            console.log("Event: 开始按文件名下载Lib文件，总数：" + count);
            for (let df of loadList) {
                console.log("Event: 开始按文件名下载Lib文件: [" + df.getComment() + "] -> " + df.getDataFullName());
            }
        }
        onLibLoadAllStart(loadList, count) {
            console.log("Event: 开始下载所有Lib文件，总数：" + count);
        }
        onLibLoadGroupStart(loadList, count) {
            console.log("Event: 开始按组下载Lib文件，总数：" + count);
            for (let df of loadList) {
                console.log("Event: 开始按组下载Lib文件: [" + df.getComment() + "] -> " + df.getDataFullName());
            }
        }
        onLibLoadDownloaded(loadList, isSuccess) {
            for (let df of loadList) {
                console.log("Event: 下载完毕Lib文件：" + df.getDataFullName() + " 是否成功：" + isSuccess);
            }
        }
        onLibLoadCompleted(dataFullName, table) {
            console.log("Event: 解析Lib文件成功：" + dataFullName);
            console.log(table);
        }
        onLibLoadFail(dataFullName) {
            console.log("Event: 解析Lib文件失败!! " + dataFullName);
        }
        onLibLoadProcess(process) {
            console.log("Event: 下载进度：" + (process * 100) + "%");
        }
        onLibLoadAllCompleted(loadList, isAllSuccess) {
            console.log("Event: 解析Lib文件全部完成, 是否全部成功：" + isAllSuccess);
        }
    }
    Main.CFG_SERVER_HOST = "http://127.0.0.1:8080/";
    new Main();

}());
//# sourceMappingURL=bundle.js.map
