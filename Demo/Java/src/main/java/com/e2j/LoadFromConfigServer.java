package com.e2j;

import com.e2j.lib.ExcelLib;
import com.e2j.lib.ExcelLibHttpLoader;
import com.e2j.lib.vo.*;
import com.e2j.utils.CompatibleCRC32;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Excel文件加载类 测试范例
 * @author Seraph.Yang
 * @date 2022/7/14 14:38
 **/
public class LoadFromConfigServer {
    static final Logger logger = (Logger) LoggerFactory.getLogger(LoadFromConfigServer.class);

    private static String cfgServerHost = "http://127.0.0.1:8080/";

    public static void main(String[] args) throws Exception {
        System.out.println("start project from http!");

        String headerUrl = "sys/version";
        ExcelLibHttpLoader headerLoader = new ExcelLibHttpLoader(cfgServerHost);
        String headerJsonStr = headerLoader.getUrl(headerUrl, null, null);
        System.out.println(headerJsonStr);

        String definitionUrl = "sys/definition";
        ExcelLibHttpLoader definitionLoader = new ExcelLibHttpLoader(cfgServerHost);
        String definitionJsonStr = definitionLoader.getUrl(definitionUrl, null, null);
        //lib不宜多创建，建议放到系统单例内使用
        ExcelLib excelLib = ExcelLib.getInstance();
        excelLib.setLoadFromServer(cfgServerHost);
        excelLib.reloadDefinition(true);
        excelLib.loadAllData(true);


        List<LibLevel> libLevelList = excelLib.data().levelList();
        System.out.println("获得level表，总数:" + libLevelList.size());

        LibLevel libLevel = excelLib.data().levelMap(10);
        System.out.println("获得等级为10的LibLevel: " + libLevel);

        LibStar libStar = excelLib.data().starMap(5);
        System.out.println("获得星级为5的LibStar: " + libStar);
        LibCharacter characterEntity = libStar.getLibCharacterByCid();
        System.out.println("从星级为5的LibStar中便捷获取的Character实体: " + characterEntity);

        LibCharacter libCharacter = excelLib.data().characterMap(2);
        System.out.println("获得id为2的LibCharacter: " + libCharacter);

        LibCustomerLevel libCustomerLevel = excelLib.data().customerLevelMkMap(10, 2);
        System.out.println("获得10级且升级类型为2的LibCustomerLevel: " + libCustomerLevel);

        List<LibTeam> teamlist =excelLib.data().teamMapList(1);
        System.out.println("获得队伍编号为1的LibTeam列表，总数:" + libLevelList.size());
        for (LibTeam libTeam : teamlist) {
            System.out.println(libTeam);
        }

        LibSetting cameraFollowDistanceNear = excelLib.data().settingMap("camera_follow_distance_near");
        System.out.println("从单键表中根据key为camera_follow_distance_near获得配置：" + cameraFollowDistanceNear);

        List<LibSettingsComplex> loginList = excelLib.data().settingsComplexMapList("login_list");
        System.out.println("从复合键表中根据key为login_list获得配置列表，总数为:" + loginList.size());
        for (LibSettingsComplex setting : loginList) {
            System.out.println(setting);
        }

        excelLib.loadFromGroup(ExcelLibConstant.GROUP_CHARACTERS, true);

        excelLib.loadFromFileByName(ExcelLibConstant.TB_STAR_MAP_ID,false, true);

        excelLib.loadFromFileByName(ExcelLibConstant.TB_LEVEL_MAP_LV, true, true);

        excelLib.loadFromFileByName(ExcelLibConstant.TB_STAR_MAP_ID, true, true);

        excelLib.loadFromFileByName(ExcelLibConstant.TB_CUSTOMERLEVEL_MKMAP_CUSTOMERLV_UPGRADETYPE, true, true);



    }
}
