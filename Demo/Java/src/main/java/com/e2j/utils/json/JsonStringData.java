package com.e2j.utils.json;

/**
 * fastJson 字符串读取数据表数据容器
 * @author 杨永良
 * @date 2020/12/20 13:33
 **/
public class JsonStringData extends BaseJsonData implements IJsonData {

    public JsonStringData(String jsonStr) {
        this.jsonStr = jsonStr;
        md5 = getMD5(jsonStr);
        fileSize = jsonStr.length();
    }
}
