package com.e2j.utils.json;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import org.springframework.util.DigestUtils;

/**
 * TODO
 *
 * @author 杨永良
 * @date 2020/12/20 13:13
 **/
public class BaseJsonData implements IJsonData {

    protected JSONArray jsonArr;

    protected String fileName;

    protected String md5;

    protected int fileSize;

    protected String uri;

    /**
     * json数据
     *
     * @return
     */
    @Override
    public JSONArray getJsonArr() {
        return jsonArr;
    }

    protected String jsonStr;

    @Override
    public String getJsonStr() {
        return jsonStr;
    }

    @Override
    public void setJsonStr(String jsonStr) {
        this.jsonStr = jsonStr;
    }


    /**
     * 读取成FastJson数组
     * @param readTagName 指定节点； 如果为空，则认定root就是一个数组
     * @return FastJson数组
     */
    @Override
    public JSONArray readJsonArray(String readTagName){
        if (readTagName == null || readTagName == "") {
            //根数组读取
            jsonArr = JSON.parseArray(jsonStr);
        } else {
            //指定节点数组读取
            JSONObject jobj = JSON.parseObject(jsonStr);
            Object obj = jobj.get(readTagName);
            if (obj != null) {
                jsonArr = (JSONArray) obj;
            }
        }
        return jsonArr;
    }

    public static String getMD5(String str) {
        String md5 = DigestUtils.md5DigestAsHex(str.getBytes());
        return md5;
    }

    @Override
    public String getFileName() {
        return fileName;
    }

    @Override
    public String getMd5() {
        return md5;
    }

    @Override
    public int getFileSize() {
        return fileSize;
    }

    @Override
    public String getUri() {
        return uri;
    }
}
