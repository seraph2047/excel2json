package com.e2j.lib;

import java.util.Date;

/**
 * TODO
 *
 * @author 杨永良
 * @date 2022/7/28 9:16
 **/
public class ExcelLibDefinitionTable {

    private String jsonName;
    private String comment;
    private String md5;
    private Date date;
    private String version;
    private String author;


    public String getJsonName() {
        return jsonName;
    }

    public void setJsonName(String jsonName) {
        this.jsonName = jsonName;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getMd5() {
        return md5;
    }

    public void setMd5(String md5) {
        this.md5 = md5;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }
}
