package com.e2j.lib.vo;


/**
 * 自动生成的数据表对应常量
 * @author Generate By Excel2Json
 **/
public class ExcelLibConstant {


    /**
     * 组别[characters] 包含表如下：<BR/>[角色 LibCharacter] -> characterMapByCid();<BR/>[等级 LibLevel] -> levelList();<BR/>[顾客等级表 LibCustomerLevel] -> customerLevelMkMapByCustomerLvUpgradeType();
     */
    public static final String GROUP_CHARACTERS = "group_characters";
    
    /**
     * 组别[chapter] 包含表如下：<BR/>[怪物队伍表 LibTeam] -> teamMapListById();
     */
    public static final String GROUP_CHAPTER = "group_chapter";
    
    /**
     * 表[角色:character] characterMapByCid dataFullName Relation: characterMap(int cid) -> LibCharacter
     */
    public static final String TB_CHARACTER_MAP_CID = "characterMapByCid";
    
    /**
     * 表[等级:level] levelList dataFullName Relation: levelList() -> List<LibLevel>
     */
    public static final String TB_LEVEL_LIST = "levelList";
    
    /**
     * 表[顾客等级表:customerLevel] customerLevelMkMapByCustomerLvUpgradeType dataFullName Relation: customerLevelMkMap(int customerLv, int upgradeType) -> LibCustomerLevel
     */
    public static final String TB_CUSTOMERLEVEL_MKMAP_CUSTOMERLV_UPGRADETYPE = "customerLevelMkMapByCustomerLvUpgradeType";
    
    /**
     * 表[怪物队伍表:team] teamMapListById dataFullName Relation: teamMapList(int id) -> List<LibTeam>
     */
    public static final String TB_TEAM_MAPLIST_ID = "teamMapListById";
    

}
