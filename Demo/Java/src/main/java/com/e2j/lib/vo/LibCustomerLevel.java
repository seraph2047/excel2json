package com.e2j.lib.vo;

import com.e2j.lib.*;



/**
 * 导表实体：顾客等级表
 * @author Generate By Excel2Json
 **/
public class LibCustomerLevel extends LibItemBase implements ILibItem {

    
    /**
    * 顾客等级
    **/
    private int customerLv;
    
    /**
    * 类型
    **/
    private int upgradeType;
    
    /**
    * 购买力
    **/
    private int purchasing;


    
    /**
    * 顾客等级
    **/
    public int getCustomerLv() { return customerLv; }
    public void setCustomerLv(int customerLv) { this.customerLv = customerLv; }
    
    /**
    * 类型
    **/
    public int getUpgradeType() { return upgradeType; }
    public void setUpgradeType(int upgradeType) { this.upgradeType = upgradeType; }
    
    /**
    * 购买力
    **/
    public int getPurchasing() { return purchasing; }
    public void setPurchasing(int purchasing) { this.purchasing = purchasing; }

    /**
     * 从json中加载数据
     */
    @Override
    public void preload() {        
        
    }
    
    @Override
    public String toString() {
        return "LibCustomerLevel(顾客等级表){" +
                "customerLv=" + this.customerLv + 
                ", upgradeType=" + this.upgradeType + 
                ", purchasing=" + this.purchasing + 
                '}';
    }
    

}