package com.e2j.lib;

import java.util.HashMap;
import java.util.Map;

/**
 * Excel数据配置数据基类
 * @author 杨永良
 * @date 2022/7/13 9:24
 **/
public abstract class ExcelLibDataBase {

    protected Map<String, Object> maps;

    public Map<String, Object> getMaps() {
        if(maps==null){
            maps = new HashMap();
        }
        return maps;
    }


}
