package com.e2j.lib;

import com.e2j.lib.vo.ExcelLibDataEasyAccess;

/**
 * Excel数据类，继承与工具生成的ExcelLibDataEasyAccess类。
 * 本类主要为了开发者添加工具生成不满足开发需求时，自定义的数据读取方式。
 * @author 杨永良
 * @date 2022/7/13 17:00
 **/
public class ExcelLibData extends ExcelLibDataEasyAccess {

    //=============这里添加自定义读取数据==============

}
