package com.e2j.lib.vo;

import com.e2j.lib.*;
import java.util.List;
import java.util.Map;

/**
 * 数据表自动生成数据快捷读取类
 * @author Generate By Excel2Json
 **/
public class ExcelLibDataEasyAccess extends ExcelLibDataBase {


    /**
     * 表[角色:character] characterMapByCid
     * @param cid
     * @return LibCharacter
     */
    public LibCharacter characterMap(int cid) {
        Map<Integer, LibCharacter> data = (Map<Integer, LibCharacter>) maps.get("characterMapByCid");
        return data == null ? null : data.get(cid);
    }
    /**
     * 表[等级:level] levelList
     * @return LibLevel
     */
    public List<LibLevel> levelList() {
        return (List<LibLevel>) maps.get("levelList");
    }
    /**
     * 表[顾客等级表:customerLevel] customerLevelMkMapByCustomerLvUpgradeType
     * @param int customerLv, int upgradeType
     * @return LibCustomerLevel
     */
    public LibCustomerLevel customerLevelMkMap(int customerLv, int upgradeType) {
        Map<String, LibCustomerLevel> data = (Map<String, LibCustomerLevel>) maps.get("customerLevelMkMapByCustomerLvUpgradeType");
        return data == null ? null : data.get(customerLv + "_" + upgradeType + "_");
    }
    
    /**
     * 表[怪物队伍表:team] teamMapListById
     * @param id
     * @return LibTeam
     */
    public List<LibTeam> teamMapList(int id){
        Map<Integer, List<LibTeam>> data = (Map<Integer, List<LibTeam>>) maps.get("teamMapListById");
        return data == null ? null : data.get(id);
    }

}