package com.e2j.lib.vo;

import com.e2j.lib.*;



/**
 * 导表实体：等级
 * @author Generate By Excel2Json
 **/
public class LibLevel extends LibItemBase implements ILibItem {

    
    /**
    * 等级
    **/
    private int lv;
    
    /**
    * 换算等级
    **/
    private int elv2;
    
    /**
    * 被动技能格数
    **/
    private int pskillCount2;
    
    /**
    * 允许升职级
    **/
    private int occLevel;


    
    /**
    * 等级
    **/
    public int getLv() { return lv; }
    public void setLv(int lv) { this.lv = lv; }
    
    /**
    * 换算等级
    **/
    public int getElv2() { return elv2; }
    public void setElv2(int elv2) { this.elv2 = elv2; }
    
    /**
    * 被动技能格数
    **/
    public int getPskillCount2() { return pskillCount2; }
    public void setPskillCount2(int pskillCount2) { this.pskillCount2 = pskillCount2; }
    
    /**
    * 允许升职级
    **/
    public int getOccLevel() { return occLevel; }
    public void setOccLevel(int occLevel) { this.occLevel = occLevel; }

    /**
     * 从json中加载数据
     */
    @Override
    public void preload() {        
        
    }
    
    @Override
    public String toString() {
        return "LibLevel(等级){" +
                "lv=" + this.lv + 
                ", elv2=" + this.elv2 + 
                ", pskillCount2=" + this.pskillCount2 + 
                ", occLevel=" + this.occLevel + 
                '}';
    }
    

}