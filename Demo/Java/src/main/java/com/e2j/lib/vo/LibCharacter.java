package com.e2j.lib.vo;

import com.e2j.lib.*;



/**
 * 导表实体：角色
 * @author Generate By Excel2Json
 **/
public class LibCharacter extends LibItemBase implements ILibItem {

    
    /**
    * 主键
    **/
    private int cid;
    
    /**
    * 角色名
    **/
    private String sname;
    
    /**
    * 职业
    **/
    private int ocId;
    
    /**
    * 大类ID
    **/
    private int ctype;
    
    /**
    * 品阶
    **/
    private int grade;
    
    /**
    * 生命
    **/
    private float hp;
    
    /**
    * 力量
    **/
    private float atk;
    
    /**
    * 智力
    **/
    private float mag;
    
    /**
    * 精神
    **/
    private float mdf;
    
    /**
    * 防御
    **/
    private float df;
    
    /**
    * 暴击
    **/
    private float cri;
    
    /**
    * 射程
    **/
    private float rng;
    
    /**
    * 攻速
    **/
    private float aspd;
    
    /**
    * 弹道速度
    **/
    private float bulletSpeed;
    
    /**
    * 图标
    **/
    private String icon;
    
    /**
    * 起手延迟
    **/
    private int startDelay;
    
    /**
    * 普攻伤害延时
    **/
    private int mcDelay;
    
    /**
    * 模型路径
    **/
    private String model;
    
    /**
    * 高精模型路径
    **/
    private String hdModel;
    
    /**
    * 普攻动作特效
    **/
    private String fnAtk;
    
    /**
    * 被普攻受击动作特效
    **/
    private String fnDamage;
    
    /**
    * 普攻弹道
    **/
    private String fnBullet;
    
    /**
    * 普攻爆炸点
    **/
    private String fnExplo;
    
    /**
    * 缩放
    **/
    private float scale;
    
    /**
    * 对话框展示形象
    **/
    private String avatarModel;


    
    /**
    * 主键
    **/
    public int getCid() { return cid; }
    public void setCid(int cid) { this.cid = cid; }
    
    /**
    * 角色名
    **/
    public String getSname() { return sname; }
    public void setSname(String sname) { this.sname = sname; }
    
    /**
    * 职业
    **/
    public int getOcId() { return ocId; }
    public void setOcId(int ocId) { this.ocId = ocId; }
    
    /**
    * 大类ID
    **/
    public int getCtype() { return ctype; }
    public void setCtype(int ctype) { this.ctype = ctype; }
    
    /**
    * 品阶
    **/
    public int getGrade() { return grade; }
    public void setGrade(int grade) { this.grade = grade; }
    
    /**
    * 生命
    **/
    public float getHp() { return hp; }
    public void setHp(float hp) { this.hp = hp; }
    
    /**
    * 力量
    **/
    public float getAtk() { return atk; }
    public void setAtk(float atk) { this.atk = atk; }
    
    /**
    * 智力
    **/
    public float getMag() { return mag; }
    public void setMag(float mag) { this.mag = mag; }
    
    /**
    * 精神
    **/
    public float getMdf() { return mdf; }
    public void setMdf(float mdf) { this.mdf = mdf; }
    
    /**
    * 防御
    **/
    public float getDf() { return df; }
    public void setDf(float df) { this.df = df; }
    
    /**
    * 暴击
    **/
    public float getCri() { return cri; }
    public void setCri(float cri) { this.cri = cri; }
    
    /**
    * 射程
    **/
    public float getRng() { return rng; }
    public void setRng(float rng) { this.rng = rng; }
    
    /**
    * 攻速
    **/
    public float getAspd() { return aspd; }
    public void setAspd(float aspd) { this.aspd = aspd; }
    
    /**
    * 弹道速度
    **/
    public float getBulletSpeed() { return bulletSpeed; }
    public void setBulletSpeed(float bulletSpeed) { this.bulletSpeed = bulletSpeed; }
    
    /**
    * 图标
    **/
    public String getIcon() { return icon; }
    public void setIcon(String icon) { this.icon = icon; }
    
    /**
    * 起手延迟
    **/
    public int getStartDelay() { return startDelay; }
    public void setStartDelay(int startDelay) { this.startDelay = startDelay; }
    
    /**
    * 普攻伤害延时
    **/
    public int getMcDelay() { return mcDelay; }
    public void setMcDelay(int mcDelay) { this.mcDelay = mcDelay; }
    
    /**
    * 模型路径
    **/
    public String getModel() { return model; }
    public void setModel(String model) { this.model = model; }
    
    /**
    * 高精模型路径
    **/
    public String getHdModel() { return hdModel; }
    public void setHdModel(String hdModel) { this.hdModel = hdModel; }
    
    /**
    * 普攻动作特效
    **/
    public String getFnAtk() { return fnAtk; }
    public void setFnAtk(String fnAtk) { this.fnAtk = fnAtk; }
    
    /**
    * 被普攻受击动作特效
    **/
    public String getFnDamage() { return fnDamage; }
    public void setFnDamage(String fnDamage) { this.fnDamage = fnDamage; }
    
    /**
    * 普攻弹道
    **/
    public String getFnBullet() { return fnBullet; }
    public void setFnBullet(String fnBullet) { this.fnBullet = fnBullet; }
    
    /**
    * 普攻爆炸点
    **/
    public String getFnExplo() { return fnExplo; }
    public void setFnExplo(String fnExplo) { this.fnExplo = fnExplo; }
    
    /**
    * 缩放
    **/
    public float getScale() { return scale; }
    public void setScale(float scale) { this.scale = scale; }
    
    /**
    * 对话框展示形象
    **/
    public String getAvatarModel() { return avatarModel; }
    public void setAvatarModel(String avatarModel) { this.avatarModel = avatarModel; }

    /**
     * 从json中加载数据
     */
    @Override
    public void preload() {        
        
    }
    
    @Override
    public String toString() {
        return "LibCharacter(角色){" +
                "cid=" + this.cid + 
                ", sname='" + this.sname + '\'' + 
                ", ocId=" + this.ocId + 
                ", ctype=" + this.ctype + 
                ", grade=" + this.grade + 
                ", hp=" + this.hp + 
                ", atk=" + this.atk + 
                ", mag=" + this.mag + 
                ", mdf=" + this.mdf + 
                ", df=" + this.df + 
                ", cri=" + this.cri + 
                ", rng=" + this.rng + 
                ", aspd=" + this.aspd + 
                ", bulletSpeed=" + this.bulletSpeed + 
                ", icon='" + this.icon + '\'' + 
                ", startDelay=" + this.startDelay + 
                ", mcDelay=" + this.mcDelay + 
                ", model='" + this.model + '\'' + 
                ", hdModel='" + this.hdModel + '\'' + 
                ", fnAtk='" + this.fnAtk + '\'' + 
                ", fnDamage='" + this.fnDamage + '\'' + 
                ", fnBullet='" + this.fnBullet + '\'' + 
                ", fnExplo='" + this.fnExplo + '\'' + 
                ", scale=" + this.scale + 
                ", avatarModel='" + this.avatarModel + '\'' + 
                '}';
    }
    

}