package com.e2j.lib;

import com.e2j.utils.json.IJsonData;
import com.e2j.utils.json.JsonFileData;
import com.e2j.utils.json.JsonHashMapReader;
import com.e2j.utils.json.JsonListReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Excel配置文件读取分析用工具类
 * @author 杨永良
 * @date 2022/7/13 10:02
 **/
public class ExcelLibTools {
    static final Logger logger = (Logger) LoggerFactory.getLogger(ExcelLibTools.class);

    public static List<ExcelLibDefinition> LoadDefinition(IJsonData jsonData){
        JsonListReader<ExcelLibDefinition> reader = new JsonListReader<ExcelLibDefinition>(jsonData);
        List<ExcelLibDefinition> list = reader.getList(ExcelLibDefinition.class, "defines");
        return list;
    }

    public static Map<String, ExcelLibDefinitionTable> LoadDefinitionTable(IJsonData jsonData) throws NoSuchFieldException {
        JsonHashMapReader<String, ExcelLibDefinitionTable> reader = new JsonHashMapReader<String, ExcelLibDefinitionTable>(jsonData);
        Map<String, ExcelLibDefinitionTable> map =reader.getMap(ExcelLibDefinitionTable.class,"jsonName", "tables");
        return map;
    }

    public static IJsonData getJsonData(String resourcePath){
        IJsonData jsonData;
        try {
            jsonData = new JsonFileData(resourcePath);
        }catch (Exception e){
            logger.error("Excel数据包加载：打开_definition.json失败");
            return null;
        }
        return jsonData;
    }

    public static <T> List<T> loadJsonList(IJsonData jsonData, Class clazz,  String desc)  {
        JsonListReader<T> fList = new JsonListReader<>(jsonData);
        List<T> list = fList.getList(clazz, "data");
        logger.info("加载配置List文件[" + desc + "] (" + clazz.getSimpleName() + ")， 已加载条目个数：" + list.size() + "  md5:[" + jsonData.getMd5() + "]");
        return list;
    }


    public static <T> Map<String, T> loadJsonStringMap(IJsonData jsonData, Class clazz, String keyField, String desc) throws IOException, NoSuchFieldException {
        JsonHashMapReader<String, T> fmap = new JsonHashMapReader<String, T>(jsonData);
        Map<String, T> map = fmap.getStringKeyMap(clazz, keyField, "data");
        logger.info("加载配置Map文件(string Key)[" + desc + "] (" + clazz.getSimpleName() + ")， 已加载条目个数：" + map.size() + "  md5:[" + jsonData.getMd5() + "]");
        return map;
    }

    public static <T> Map<Integer, T> loadJsonIntMap(IJsonData jsonData, Class clazz, String keyField, String desc) throws IOException, NoSuchFieldException {
        JsonHashMapReader<Integer, T> fmap = new JsonHashMapReader<Integer, T>(jsonData);
        Map<Integer, T> map = fmap.getIntegerKeyMap(clazz, keyField, "data");
        logger.info("加载配置Map文件(int Key)[" + desc + "] (" + clazz.getSimpleName() + ")， 已加载条目个数：" + map.size() + "  md5:[" + jsonData.getMd5() + "]");
         return map;
    }


    public static <T> Map<String, T> loadJsonMultKeyMap(IJsonData jsonData, Class clazz, String[] keyFields, String desc) throws IOException, NoSuchFieldException {
        JsonHashMapReader<String, T> fmap = new JsonHashMapReader<String, T>(jsonData);
        Map<String, T> map = fmap.getMultKeyMap(clazz, keyFields, "data");
        logger.info("加载配置多键值MultMap文件(Multi-Key)[" + desc + "] (" + clazz.getSimpleName() + ")， 已加载条目个数：" + map.size() + "  md5:[" + jsonData.getMd5() + "]");
        return map;
    }

    public static <T> Map<Integer, List<T>> loadJsonMapSetByIntegerKey(IJsonData jsonData,  Class clazz, String keyIntField, String desc) throws IllegalAccessException {
        JsonListReader<T> fList = new JsonListReader<T>(jsonData);
        List<T> list = fList.getList(clazz, "data");
        Map<Integer, List<T>> map = new HashMap<>();
        for (T item : list) {
            Field[] fields = clazz.getDeclaredFields();
            for (Field field : fields) {
                if (field.getName().equals(keyIntField)) {
                    field.setAccessible(true);
                    int key = field.getInt(item);
                    List<T> mapList = map.computeIfAbsent(key, k -> new ArrayList<T>());
                    mapList.add(item);
                }
            }
        }
        logger.info("加载配置Map<Integer, List<T>>文件[" + desc + "] (" + clazz.getSimpleName() + ")， 已加载条目个数：" + list.size() + ", 汇总成Map key个数：" + map.size() + "  md5:[" + jsonData.getMd5() + "]");
        return map;
    }

    public static <T> Map<String, List<T>> loadJsonMapSetByStringKey(IJsonData jsonData,  Class clazz,  String keyIntField, String desc) throws IllegalAccessException {

        JsonListReader<T> fList = new JsonListReader<T>(jsonData);
        List<T> list = fList.getList(clazz, "data");

        Map<String, List<T>> map = new HashMap<>();
        for (T item : list) {
            Field[] fields = clazz.getDeclaredFields();
            for (Field field : fields) {
                if (field.getName().equals(keyIntField)) {
                    field.setAccessible(true);
                    String key = (String) field.get(item);
                    List<T> mapList = map.get(key);
                    if (mapList == null) {
                        mapList = new ArrayList<T>();
                        map.put(key, mapList);
                    }
                    mapList.add(item);
                }
            }

        }
        logger.info("加载配置Map<String, List<T>>文件[" + desc + "] (" + clazz.getSimpleName() + ")， 已加载条目个数：" + list.size() + ", 汇总成Map key个数：" + map.size() + "  md5:[" + jsonData.getMd5() + "]");
        return map;
    }
}
