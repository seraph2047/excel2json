package com.e2j.lib.vo;

import com.e2j.lib.*;



/**
 * 导表实体：怪物队伍表
 * @author Generate By Excel2Json
 **/
public class LibTeam extends LibItemBase implements ILibItem {

    
    /**
    * 队伍ID
    **/
    private int id;
    
    /**
    * 站位
    **/
    private int seat;
    
    /**
    * 角色星级ID
    **/
    private int starId;
    
    /**
    * 星级
    **/
    private int starLv;
    
    /**
    * 角色ID
    **/
    private int cid;
    
    /**
    * 随机权重(相同站位运算)
    **/
    private float weight;
    
    /**
    * 怪物等级
    **/
    private int lv;
    
    /**
    * 0一般1小boss2大boss
    **/
    private int boss;
    
    /**
    * 生命
    **/
    private float hp;
    
    /**
    * 力量
    **/
    private float atk;
    
    /**
    * 智力
    **/
    private float mag;
    
    /**
    * 精神
    **/
    private float mdf;
    
    /**
    * 防御
    **/
    private float df;
    
    /**
    * 暴击
    **/
    private float cri;
    
    /**
    * 是否主角
    **/
    private boolean isLead;
    
    /**
    * 是否团队
    **/
    private boolean team;


    
    /**
    * 队伍ID
    **/
    public int getId() { return id; }
    public void setId(int id) { this.id = id; }
    
    /**
    * 站位
    **/
    public int getSeat() { return seat; }
    public void setSeat(int seat) { this.seat = seat; }
    
    /**
    * 角色星级ID
    **/
    public int getStarId() { return starId; }
    public void setStarId(int starId) { this.starId = starId; }
    
    /**
    * 星级
    **/
    public int getStarLv() { return starLv; }
    public void setStarLv(int starLv) { this.starLv = starLv; }
    
    /**
    * 角色ID
    **/
    public int getCid() { return cid; }
    public void setCid(int cid) { this.cid = cid; }
    
    /**
    * 随机权重(相同站位运算)
    **/
    public float getWeight() { return weight; }
    public void setWeight(float weight) { this.weight = weight; }
    
    /**
    * 怪物等级
    **/
    public int getLv() { return lv; }
    public void setLv(int lv) { this.lv = lv; }
    
    /**
    * 0一般1小boss2大boss
    **/
    public int getBoss() { return boss; }
    public void setBoss(int boss) { this.boss = boss; }
    
    /**
    * 生命
    **/
    public float getHp() { return hp; }
    public void setHp(float hp) { this.hp = hp; }
    
    /**
    * 力量
    **/
    public float getAtk() { return atk; }
    public void setAtk(float atk) { this.atk = atk; }
    
    /**
    * 智力
    **/
    public float getMag() { return mag; }
    public void setMag(float mag) { this.mag = mag; }
    
    /**
    * 精神
    **/
    public float getMdf() { return mdf; }
    public void setMdf(float mdf) { this.mdf = mdf; }
    
    /**
    * 防御
    **/
    public float getDf() { return df; }
    public void setDf(float df) { this.df = df; }
    
    /**
    * 暴击
    **/
    public float getCri() { return cri; }
    public void setCri(float cri) { this.cri = cri; }
    
    /**
    * 是否主角
    **/
    public boolean isLelead() { return isLead; }
    public void setLead(boolean isLead) { this.isLead = isLead; }
    
    /**
    * 是否团队
    **/
    public boolean isTeam() { return team; }
    public void setTeam(boolean team) { this.team = team; }

    /**
     * 从json中加载数据
     */
    @Override
    public void preload() {        
        
    }
    
    @Override
    public String toString() {
        return "LibTeam(怪物队伍表){" +
                "id=" + this.id + 
                ", seat=" + this.seat + 
                ", starId=" + this.starId + 
                ", starLv=" + this.starLv + 
                ", cid=" + this.cid + 
                ", weight=" + this.weight + 
                ", lv=" + this.lv + 
                ", boss=" + this.boss + 
                ", hp=" + this.hp + 
                ", atk=" + this.atk + 
                ", mag=" + this.mag + 
                ", mdf=" + this.mdf + 
                ", df=" + this.df + 
                ", cri=" + this.cri + 
                ", isLead=" + this.isLead + 
                ", team=" + this.team + 
                '}';
    }
    

}