package com.e2j.lib;

import java.util.List;
import java.util.Map;

/**
 * Excel数据结构配置文件加载
 * @author 杨永良
 * @date 2022/7/13 10:14
 **/
public class ExcelLibDefinition implements Comparable<ExcelLibDefinition> {
    //数据类型判断常量
    public static final String FIELD_TYPE_INTEGER = "Integer";
    public static final String FIELD_TYPE_LONG = "Long";
    public static final String FIELD_TYPE_STRING = "String";
    public static final String FIELD_TYPE_FLOAT = "Float";
    public static final String FIELD_TYPE_DOUBLE = "Double";
    public static final String FIELD_TYPE_BOOL = "Boolean";
    //加载索引方式类型判断常量
    public static final String LOAD_TYPE_LIST = "List";
    public static final String LOAD_TYPE_MAP = "Map";
    public static final String LOAD_TYPE_MKMAP = "MultKeyMap";
    public static final String LOAD_TYPE_MAP2LIST = "Map2List";

    /**
     * 主键
     * Map<键名,主键类型(ExcelLibDefinition.FIELD_TYPE_*)>
     */
    private List<Map<String,String>> keys;

    /**
     * 索引主键名
     */
    private String mainKey;

    /**
     * 主键类型，对应 ExcelLibDefinition.FIELD_TYPE_*
     */
    private String mainKeyType;

    /**
     * 加载索引方式类型，对应 ExcelLibDefinition.LOAD_TYPE_*
     */
    private String type;

    /**
     * 本包工具生成时支持的平台类型（展示用，在开发代码中无实际逻辑）
     */
    private List<String> platforms;

    /**
     * 加载权重，数值越小加载越早，相同权重随机排列
     */
    private int loadWeight;

    /**
     * 需要加载的表格
     */
    private String loadJson;

    /**
     * 需要加载的类名
     */
    private String className;

    /**
     * 注释
     */
    private String comment;

    /**
     * 数据表名，含索引方式命名
     */
    private String dataName;

    /**
     * 组别名
     */
    private String group;

    /**
     * 关联加载
     */
    private List<String> loadRelations;

    /**
     * 数据表名，含索引方式命名+索引字段命名
     */
    private String dataFullName;

    private String[] mainKeys = null;


    /**
     * 多个主键调用这个方法提取
     * @return
     */
    public String[] getMainKeys() {
        return getMainKeys(false);
    }

    /**
     * 多个主键调用这个方法提取
     * @param forceLoad 是否强制更新
     * @return
     */
    public String[] getMainKeys(boolean forceLoad) {
        if(mainKeys==null || forceLoad) {
            mainKeys = new String[keys.size()];
            for (int i = 0; i < keys.size(); i++) {
                mainKeys[i] = keys.get(i).get("name");
            }
        }
        return mainKeys;
    }

    public String getMainKey() {
        return mainKey;
    }

    public void setMainKey(String mainKey) {
        this.mainKey = mainKey;
    }

    public List<Map<String,String>> getKeys() {
        return keys;
    }

    public void setKeys(List<Map<String,String>> keys) {
        this.keys = keys;
    }

    public String getMainKeyType() {
        return mainKeyType;
    }

    public void setMainKeyType(String mainKeyType) {
        this.mainKeyType = mainKeyType;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<String> getPlatforms() {
        return platforms;
    }

    public void setPlatforms(List<String> platforms) {
        this.platforms = platforms;
    }

    public int getLoadWeight() {
        return loadWeight;
    }

    public void setLoadWeight(int loadWeight) {
        this.loadWeight = loadWeight;
    }

    public String getLoadJson() {
        return loadJson;
    }

    public void setLoadJson(String loadJson) {
        this.loadJson = loadJson;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getDataName() {
        return dataName;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public List<String> getLoadRelations() {
        return loadRelations;
    }

    public void setLoadRelations(List<String> loadRelations) {
        this.loadRelations = loadRelations;
    }

    public void setDataName(String dataName) {
        this.dataName = dataName;
    }

    public String getDataFullName() {
        return dataFullName;
    }

    public void setDataFullName(String dataFullName) {
        this.dataFullName = dataFullName;
    }

    @Override
    public int compareTo(ExcelLibDefinition o) {
        //排序，约小越前
        return this.loadWeight - o.loadWeight;
    }



}
