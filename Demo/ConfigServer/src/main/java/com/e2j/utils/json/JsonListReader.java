package com.e2j.utils.json;

import com.alibaba.fastjson2.JSONArray;

import java.util.ArrayList;
import java.util.List;

/**
 * 读取Json文件成为数组或字典。
 * 依赖FastJson
 * @author Seraph.Yang
 *
 * 		<dependency>
 * 			<groupId>com.alibaba</groupId>
 * 			<artifactId>fastjson</artifactId>
 * 			<version>1.2.73</version>
 * 		</dependency>
 */
public class JsonListReader<T> {

    private IJsonData jsonData;

    /**
     * 读取json文件成为List
     * 使用：先创建类读取文件，然后用getList
     * @throws Exception
     */
    public JsonListReader(IJsonData jsonData) {
        this.jsonData = jsonData;
    }

    /**
     * 获取指定类型ArrayList数据
     * @param clazz 反射的类如 User.class
     * @return 数据List（泛型）
     */
    public List<T> getList(Class<T> clazz) {
        return getList(clazz, null);
    }

    /**
     * 获取指定类型ArrayList数据
     * @param clazz 反射的类如 User.class
     * @param readTagName 指定节点； 如果不填，则认定root就是一个数组
     * @return 数据List（泛型）
     */
    public List<T> getList(Class<T> clazz, String readTagName){
        jsonData.readJsonArray(readTagName);
        List<T> list = new ArrayList<T>();
        JSONArray jsonArr = jsonData.getJsonArr();
        if(jsonArr==null) {
            return list;
        }
        for (int i = 0; i < jsonArr.size(); i++) {
            Object obj = jsonArr.getObject(i, clazz);
            list.add((T)obj);
        }
        return list;
    }
}
