package com.e2j.utils.json;

import com.alibaba.fastjson2.JSONArray;

/**
 * JSON读表器的接口
 * @author 杨永良
 * @date 2020/12/20 13:10
 **/
public interface IJsonData {

    public String getFileName();

    public String getMd5();

    public int getFileSize();

    public String getUri();

    public JSONArray getJsonArr();

    public String getJsonStr();

    public void setJsonStr(String jsonStr);

    /**
     * 读取成FastJson数组
     * @param readTagName 指定节点； 如果为空，则认定root就是一个数组
     * @return FastJson数组
     */
    public JSONArray readJsonArray(String readTagName);
}
