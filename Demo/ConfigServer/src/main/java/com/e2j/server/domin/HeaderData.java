package com.e2j.server.domin;

import com.alibaba.fastjson2.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 定义表definition表头
 * @author 杨永良
 * @date 2022/8/4 13:18
 **/
public class HeaderData {

    private String version;
    private Date date;
    private String author;
    private int count;
    private String md5;

    public void parseFromJson(JSONObject jsonHead){
        /*
          "header": {
            "version": "V.220802.2009.02",
            "date": "2022-08-02 20:09:02",
            "author": "SERAPH-ROG",
            "count": 8,
            "md5": "849cdb689edadd9f6dd59cca7932115f"
          },
         */
        this.version = jsonHead.getString("version");
        this.author = jsonHead.getString("author");
        this.md5 = jsonHead.getString("md5");
        this.count = Integer.parseInt(jsonHead.getString("count"));
        this.date = StrToDate(jsonHead.getString("date"));
    }

    public void setEmpty(){
        this.version = "uninitialized";
        this.md5 = "";
        this.count =0;
        this.author = "";
    }

    public String getVersion() {
        return version;
    }

    public Date getDate() {
        return date;
    }

    public String getAuthor() {
        return author;
    }

    public int getCount() {
        return count;
    }

    public String getMd5() {
        return md5;
    }


    /**
     * 字符串转换成日期
     * @param str
     * @return date
     */
    public static Date StrToDate(String str) {

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = null;
        try {
            date = format.parse(str);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }
}
