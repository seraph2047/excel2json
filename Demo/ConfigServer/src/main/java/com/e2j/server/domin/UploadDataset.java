package com.e2j.server.domin;

import com.alibaba.fastjson2.JSONObject;
import com.e2j.lib.DefinitionDataset;

import java.util.concurrent.ConcurrentHashMap;

/**
 * TODO
 *
 * @author 杨永良
 * @date 2022/8/3 17:16
 **/
public class UploadDataset {

    private DefinitionDataset definition;
    private ConcurrentHashMap<String, UploadTable> uploadTables;
    private String token;
    private long lastTime;
    private boolean isDone = false;

    public UploadDataset(String token) {
        this.token = token;
        uploadTables = new ConcurrentHashMap<>();
        this.refreshLastTime();
    }

    public DefinitionDataset getDefinition() {
        return definition;
    }

    public void setDefinition(DefinitionDataset definition) {
        this.definition = definition;
    }

    public ConcurrentHashMap<String, UploadTable> getUploadTables() {
        return uploadTables;
    }

    public void setUploadTables(ConcurrentHashMap<String, UploadTable> uploadTables) {
        this.uploadTables = uploadTables;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public long getLastTime() {
        return lastTime;
    }

    public void setLastTime(long lastTime) {
        this.lastTime = lastTime;
    }

    public void refreshLastTime(){
        lastTime = System.currentTimeMillis();
    }

    public boolean isExpire(int timeOutMills){
        return lastTime + timeOutMills < System.currentTimeMillis();
    }

    public boolean isDone() {
        return isDone;
    }

    public void setDone(boolean done) {
        isDone = done;
    }
}
