package com.e2j.server.domin;

/**
 * 上传表数据
 * @author 杨永良
 * @date 2022/8/3 14:40
 **/
public class Login {

    String username;

    String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
