package com.e2j.server;

import com.e2j.server.domin.UploadDataset;
import com.e2j.server.domin.UploadTable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 上传数据管理中心
 *
 * @author 杨永良
 * @date 2022/8/3 18:28
 **/
@Component
public class UploadCenter {
    static final Logger logger = (Logger) LoggerFactory.getLogger(UploadCenter.class);

    private final ReentrantLock lock;
    /**
     * 超时配置：默认120秒
     */
    public static int exprieMills = 120 * 1000;

    public static int checkExprieMills = 120 * 1000;

    private long lastCleanMills;

    private ConcurrentHashMap<String, UploadDataset> uploadDataMap;

    private UploadDataset configData;

    public int publishConfig(UploadDataset configData){
        this.lock.lock();
        try{
            if(!configData.getDefinition().isDone()){
                return Constant.PUBLISH_RESULT_ERROR_WRONG_DEFINTION;
            }
            for (UploadTable table : configData.getUploadTables().values()) {
                if(!table.isDone()){
                    return Constant.PUBLISH_RESULT_ERROR_WRONG_TABLE;
                }
            }
            configData.setDone(true);
            this.configData = configData;
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            this.lock.unlock();
        }
        logger.info("更换配置，版本：" + configData.getDefinition().getHeader().getVersion() + " MD5:"
                + configData.getDefinition().getHeader().getMd5());
        logger.info("更换配置，时间：" + configData.getDefinition().getHeader().getDate().toString());
        return Constant.PUBLISH_RESULT_SUCCESS;
    }

    public UploadDataset getConfigData(){
        UploadDataset data = this.configData;
        return data;
    }

    public UploadCenter() {
        this.lock =  new ReentrantLock(true);
        this.uploadDataMap = new ConcurrentHashMap<String, UploadDataset>();
    }

    /**
     * 根据token获取分批上传数据
     * @param token
     * @return
     */
    public UploadDataset getUploadDataset(String token) {
        UploadDataset data = uploadDataMap.getOrDefault(token, null);
        if(data!=null) {
            if (data.isExpire(exprieMills)) {
                uploadDataMap.remove(token);
                return null;
            }
        }
        return data;
    }

    /**
     * 检查是否有此token
     * @param token
     * @return
     */
    public boolean containsToken(String token){
        UploadDataset uploadDataset = getUploadDataset(token);
        return uploadDataset!=null;
    }

    /**
     * 添加到对应表中
     * @param token
     * @param dataset
     */
    public void putUploadDataset(String token, UploadDataset dataset){
        uploadDataMap.put(token, dataset);
    }

    public void removeExprieData() {
        uploadDataMap.forEach((k, v) -> {
            if (v.isExpire(exprieMills)) {
                uploadDataMap.remove(k);
            }
        });
    }

}
