package com.e2j.server.domin;

/**
 * 上传结果包
 * @author 杨永良
 * @date 2022/8/3 14:38
 **/
public class UploadResult {
    Integer result;

    String Msg;

    public Integer getResult() {
        return result;
    }

    public void setResult(Integer result) {
        this.result = result;
    }

    public String getMsg() {
        return Msg;
    }

    public void setMsg(String msg) {
        Msg = msg;
    }
}
