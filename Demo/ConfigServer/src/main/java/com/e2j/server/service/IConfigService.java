package com.e2j.server.service;

import com.e2j.server.domin.Publish;
import com.e2j.server.domin.UploadDataset;
import com.e2j.server.domin.UploadTable;

/**
 * TODO
 *
 * @author 杨永良
 * @date 2022/8/3 17:12
 **/
public interface IConfigService {


    int startUpload(String token);

    int publishUpload(Publish publish);

    int uploadDefinition(UploadTable uploadTable);

    int uploadTable(UploadTable uploadTable);

    UploadDataset getConfigData();
}
