package com.e2j.server;

/**
 * 常量表
 * @author 杨永良
 * @date 2022/8/3 18:23
 **/
public class Constant {

    public static final int LOGIN_RESULT_SUCCESS = 1;
    public static final int LOGIN_RESULT_USER_OR_PASS_WRONG = -1;



    public static final int PUBLISH_RESULT_SUCCESS = 1;
    public static final int PUBLISH_RESULT_ERROR = -100;
    public static final int PUBLISH_RESULT_ERROR_WRONG_DEFINTION = -120;
    public static final int PUBLISH_RESULT_ERROR_WRONG_TABLE = -121;

    public static final int UPLOAD_COMM_RESULT_SUCCESS = 1;
    public static final int UPLOAD_COMM_RESULT_EXISTED = 2;
    public static final int UPLOAD_COMM_RESULT_ERROR = -1;
    public static final int UPLOAD_COMM_ERROR_TOKEN_NOT_FOUND = -100;
    public static final int UPLOAD_COMM_ERROR_EMPTY = -101;
    public static final int UPLOAD_COMM_ERROR_FORMAT = -102;

    public static final int UPLOAD_TABLE_RESULT_ERROR_EXISTED = 2;
    public static final int UPLOAD_TABLE_RESULT_ERROR_DEFINE_NOT_FOUND = -110;




}
