package com.e2j.server.service.impl;

import com.alibaba.fastjson2.JSONArray;
import com.e2j.lib.DefinitionDataset;
import com.e2j.server.Constant;
import com.e2j.server.UploadCenter;
import com.e2j.server.domin.Publish;
import com.e2j.server.domin.UploadDataset;
import com.e2j.server.domin.UploadTable;
import com.e2j.server.service.IConfigService;
import com.e2j.utils.json.IJsonData;
import com.e2j.utils.json.JsonStringData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * TODO
 *
 * @author 杨永良
 * @date 2022/8/3 17:03
 **/
@Service
public class ConfigServiceImpl implements IConfigService {

    static final Logger logger = (Logger) LoggerFactory.getLogger(ConfigServiceImpl.class);

    @Autowired
    private UploadCenter uploadCenter;


    /**
     * 开始上传，在内存表创建任务
     * @param token
     * @return
     */
    @Override
    public int startUpload(String token){
        if(uploadCenter.containsToken(token)){
            return Constant.UPLOAD_COMM_RESULT_EXISTED;
        }
        uploadCenter.putUploadDataset(token, new UploadDataset(token));
        return Constant.UPLOAD_COMM_RESULT_SUCCESS;
    }

    /**
     * 发布任务，把上传的变成对外下载最新数据
     * @param publish
     * @return
     */
    @Override
    public int publishUpload(Publish publish){
        UploadDataset dataset = uploadCenter.getUploadDataset(publish.getToken());
        if(dataset==null){
            return Constant.UPLOAD_COMM_ERROR_TOKEN_NOT_FOUND;
        }
        int result = uploadCenter.publishConfig(dataset);
        return result;
    }

    /**
     * 上传配置数据文件
     * @param uploadTable
     * @return
     */
    @Override
    public int uploadDefinition(UploadTable uploadTable){
        UploadDataset dataset = uploadCenter.getUploadDataset(uploadTable.getToken());
        if(dataset==null){
            return Constant.UPLOAD_COMM_ERROR_TOKEN_NOT_FOUND;
        }
        dataset.refreshLastTime();
        DefinitionDataset definition = new DefinitionDataset();
        boolean result;
        try {
            result = definition.loadDefinition(uploadTable.getJson());
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
            return Constant.UPLOAD_COMM_ERROR_FORMAT;
        }
        if(!result) {
            return Constant.UPLOAD_COMM_ERROR_FORMAT;
        }
        dataset.setDefinition(definition);
        definition.setDone(true);
        return Constant.UPLOAD_COMM_RESULT_SUCCESS;
    }

    /**
     * 上传数据表
     * @param uploadTable
     * @return
     */
    @Override
    public int uploadTable(UploadTable uploadTable){
        UploadDataset dataset = uploadCenter.getUploadDataset(uploadTable.getToken());
        if(dataset==null){
            return Constant.UPLOAD_COMM_ERROR_TOKEN_NOT_FOUND;
        }
        dataset.refreshLastTime();
        if(!uploadTable.tryLoadJsonData()){
            return Constant.UPLOAD_COMM_ERROR_FORMAT;
        }
        IJsonData jsonData;
        try{
            //检查读取
            jsonData = new JsonStringData(uploadTable.getJson());
            jsonData.readJsonArray("data");
            JSONArray jsonArr = jsonData.getJsonArr();
            logger.info("上传表格，内容行数：" + jsonArr.size());
        }catch (Exception e){
            return Constant.UPLOAD_COMM_ERROR_FORMAT;
        }
        uploadTable.setDone(true);
        dataset.getUploadTables().put(uploadTable.getJsonName(), uploadTable);
        return Constant.UPLOAD_COMM_RESULT_SUCCESS;
    }

    @Override
    public UploadDataset getConfigData(){
        return uploadCenter.getConfigData();
    }

}
