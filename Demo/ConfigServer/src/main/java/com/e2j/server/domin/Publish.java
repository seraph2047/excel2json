package com.e2j.server.domin;

/**
 * 上传结果包
 * @author 杨永良
 * @date 2022/8/3 14:38
 **/
public class Publish {

    /**
     * 操作提交类型
     */
    int opType;

    String token;

    public int getOpType() {
        return opType;
    }

    public void setOpType(int opType) {
        this.opType = opType;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
