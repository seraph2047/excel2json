package com.e2j.server.controller;

import com.e2j.lib.DefinitionDataset;
import com.e2j.server.Constant;
import com.e2j.server.domin.*;
import com.e2j.server.service.IConfigService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

/**
 * TODO
 *
 * @author 杨永良
 * @date 2022/8/3 13:12
 **/
@RestController
@RequestMapping("/sys")
public class ConfigController {

    static final Logger logger = (Logger) LoggerFactory.getLogger(ConfigController.class);

    @Autowired
    IConfigService configService;

    //http://127.0.0.1:8080/sys/login
    @PostMapping("login")
    public LoginResult login(HttpServletResponse response, Login login) {
        LoginResult result = new LoginResult();
        logger.info("账号登陆：" + login.getUsername());
        String token = UUID.randomUUID().toString().replace("-", "").toLowerCase();
        result.setToken(token);
        logger.info("登陆成功，生成Token：" + token);
        if ("user".equals(login.getUsername()) && "10242048".equals(login.getPassword())) {
            int loginResult = configService.startUpload(token);
            if (loginResult < 0) {
                //内部错误
                result.setResult(Constant.UPLOAD_COMM_RESULT_ERROR);
            } else {
                //成功
                result.setResult(Constant.LOGIN_RESULT_SUCCESS);
            }
        } else {
            //账号密码错误
            result.setResult(Constant.LOGIN_RESULT_USER_OR_PASS_WRONG);
        }

        return result;
    }

    //http://127.0.0.1:8080/sys/publish
    @PostMapping("publish")
    public PublishResult publish(HttpServletResponse response, Publish publish) {
        PublishResult result = new PublishResult();
        logger.info("结束上传，开始发布。");
        int publishResult = configService.publishUpload(publish);
        result.setResult(publishResult);
        if(Constant.PUBLISH_RESULT_SUCCESS == publishResult){
            //发布成功
        }
        return result;
    }

    //http://127.0.0.1:8080/sys/upload/definition
    @PostMapping("upload/definition")
    public UploadResult uploadDefinition(HttpServletResponse response, UploadTable uploadTable) {
        UploadResult result = new UploadResult();
        System.out.println(uploadTable);
        logger.info("上传定义文件definition");
        int uploadResult = configService.uploadDefinition(uploadTable);
        result.setResult(uploadResult);
        result.setMsg(uploadTable.getJsonName());
        return result;
    }


    //http://127.0.0.1:8080/sys/upload/table
    @PostMapping("upload/table")
    public UploadResult uploadTable(HttpServletResponse response, UploadTable uploadTable) {
        UploadResult result = new UploadResult();
        System.out.println(uploadTable);
        logger.info("上传表文件:" + uploadTable.getJsonName());
        int uploadResult = configService.uploadTable(uploadTable);
        result.setResult(uploadResult);
        result.setMsg(uploadTable.getJsonName());
        return result;
    }

    //http://127.0.0.1:8080/sys/version
    @GetMapping("version")
    public HeaderData getVersion(){
        if(configService.getConfigData() == null){
            HeaderData header = new HeaderData();
            header.setEmpty();
            return header;
        }
        return configService.getConfigData().getDefinition().getHeader();
    }

    //http://127.0.0.1:8080/sys/definition
    @GetMapping("definition")
    public Object getDefinitions(){
        if(configService.getConfigData() == null){
            return null;
        }
        DefinitionDataset definition = configService.getConfigData().getDefinition();
        if(definition!=null) {
            return definition.getJsonObj();
        }else{
            return null;
        }
    }

    //http://127.0.0.1:8080/sys/getData?name=star.json
    @GetMapping("getData")
    public Object getVersion(@RequestParam("name") String name){
        if(configService.getConfigData() == null){
            return null;
        }
        ConcurrentHashMap<String, UploadTable> tables = configService.getConfigData().getUploadTables();
        UploadTable uploadTable = tables.get(name);
        if(uploadTable!=null) {
            return uploadTable.getJsonObj();
        }else{
            return null;
        }
    }
}
