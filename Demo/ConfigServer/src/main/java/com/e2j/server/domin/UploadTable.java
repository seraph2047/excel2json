package com.e2j.server.domin;

import com.alibaba.fastjson2.JSONObject;

/**
 * 上传表数据
 * @author 杨永良
 * @date 2022/8/3 14:40
 **/
public class UploadTable {

    String token;

    String jsonName;

    String version;

    String json;

    private boolean isDone = false;

    private JSONObject jsonObj;

    /**
     * 尝试加载成为json对象（检测）
     * @return 是否成功
     */
    public boolean tryLoadJsonData(){
        try {
            jsonObj = JSONObject.parseObject(json);
        }catch (Exception e){
            return false;
        }
        return true;
    }


    public JSONObject getJsonObj() {
        return jsonObj;
    }

    public void setJsonObj(JSONObject jsonObj) {
        this.jsonObj = jsonObj;
    }


    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getJsonName() {
        return jsonName;
    }

    public void setJsonName(String jsonName) {
        this.jsonName = jsonName;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getJson() {
        return json;
    }

    public void setJson(String json) {
        this.json = json;
    }

    public boolean isDone() {
        return isDone;
    }

    public void setDone(boolean done) {
        isDone = done;
    }
}
