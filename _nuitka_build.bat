mkdir dist2\
mkdir dist2\Excel2JsonTool.dist\template\
mkdir dist2\Excel2JsonTool.dist\excel\
mkdir dist2\Excel2JsonTool.dist\java\
mkdir dist2\Excel2JsonTool.dist\laya\
mkdir dist2\Excel2JsonTool.dist\output\

del dist2\Excel2JsonTool.dist\template\*.* /Q
copy template dist2\Excel2JsonTool.dist\template\
del dist2\Excel2JsonTool.dist\excel\*.* /Q
copy excel dist2\Excel2JsonTool.dist\excel\
del dist2\Excel2JsonTool.dist\java\*.* /Q
del dist2\Excel2JsonTool.dist\laya\*.* /Q
del dist2\Excel2JsonTool.dist\output\*.* /Q
del dist2\Excel2JsonTool.dist\Excel2JsonSetting.data
del dist2\Excel2JsonTool.dist\Excel2JsonData.data

copy Excel2JsonSetting.data dist2\Excel2JsonTool.dist\
copy Excel2JsonData.data dist2\Excel2JsonTool.dist\
@echo =======================================
@echo         Start Build Exe File
@echo =======================================
@echo nuitka --output-dir=dist2 Excel2JsonTool.py
nuitka --output-dir=dist2 --standalone Excel2JsonTool.py --plugin-enable=pyside2
@pause