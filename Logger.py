from PySide2.QtCore import QRect
from PySide2.QtWidgets import *
from PySide2.QtGui import QColor, QFont

import Util


class Logger:

    logs: QTextEdit
    """日志显示控件 .append()增加日志"""

    window: QWidget


    def __new__(cls, *args, **kwargs):
        if not hasattr(cls, "_instance"):
            cls._instance = super().__new__(cls, *args, **kwargs)
            print("第一次创建EData")
        return cls._instance

    def bindOuttext(self, logs: QTextEdit):
        self.logs = logs
        # self.window = window

    def logSpace(self):
        self.logs.append("\n")
        self.logs.update()
        self.logs.repaint()

    def logNotice(self, text: str, senderFrom:QWidget=None):
        color = QColor(100, 100, 100)
        self.logs.setFontWeight(QFont.Normal)
        self.logs.setTextColor(color)
        self.logs.append(text)
        # self.resetFormPosition(senderFrom)
        self.logs.update()
        self.logs.repaint()


    def logWarning(self, text: str, senderFrom:QWidget=None):
        color = QColor(255, 125, 0)
        self.logs.setFontWeight(QFont.Normal)
        self.logs.setTextColor(color)
        self.logs.append(text)
        # self.resetFormPosition(senderFrom)
        self.logs.update()
        self.logs.repaint()

    def logError(self, text: str, senderFrom:QWidget=None):
        color = QColor(255, 0, 0)
        self.logs.setTextColor(color)
        self.logs.append(text)
        # self.resetFormPosition(senderFrom)
        self.logs.update()
        self.logs.repaint()

    def logSuccess(self, text: str, senderFrom:QWidget=None):
        color = QColor(0, 166, 24)
        self.logs.setFontWeight(QFont.Normal)
        self.logs.setTextColor(color)
        self.logs.append(text)
        # self.resetFormPosition(senderFrom)
        self.logs.update()
        self.logs.repaint()

    def logAllDone(self, text: str, senderFrom:QWidget=None):
        color = QColor(0, 125, 24)
        self.logs.setFontWeight(QFont.Bold)
        self.logs.setTextColor(color)
        self.logs.append(text)
        # self.resetFormPosition(senderFrom)
        self.logs.update()
        self.logs.repaint()

    def clear(self):
        self.logs.clear()


    # def resetFormPosition(self, senderFrom:QWidget, forceTop:bool = False):
    #     # 调整窗口位置，不阻挡logs
    #     if (not self.window.isVisible()) or self.window.isHidden() or forceTop:
    #         Util.setToTop(self.window)
    #     if senderFrom is None:
    #         return
    #     mainRect: QRect = senderFrom.geometry()
    #     # self.window.geometry().moveTo(mainRect.x(), mainRect.y() + mainRect.height())
    #     # self.window.move(senderFrom.x(), senderFrom.y() + senderFrom.height() + 40)
    #     # self.window.geometry().setX(mainRect.x())
    #     # self.window.geometry().setY(mainRect.y() + mainRect.height())
    #     self.window.move(mainRect.x(), mainRect.y() + mainRect.height() + 5)
    #     self.window.setFixedWidth(senderFrom.width())
    #     self.logs.update()
    #     self.logs.repaint()

