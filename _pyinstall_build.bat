mkdir dist\
mkdir dist\template\
mkdir dist\excel\
mkdir dist\java\
mkdir dist\laya\
mkdir dist\output\

del dist\template\*.* /Q
copy template dist\template\
del dist\excel\*.* /Q
copy excel dist\excel\
del dist\java\*.* /Q
del dist\laya\*.* /Q
del dist\output\*.* /Q
del dist\Excel2JsonSetting.data
del dist\Excel2JsonData.data

copy Excel2JsonSetting.data dist\
copy Excel2JsonData.data dist\
@echo =======================================
@echo         Start Build Exe File
@echo =======================================
@echo pyinstaller -F -w Excel2JsonTool.py
pyinstaller -F Excel2JsonTool.py
@pause