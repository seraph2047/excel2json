# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'log.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_LogDialog(object):
    def setupUi(self, LogDialog):
        if not LogDialog.objectName():
            LogDialog.setObjectName(u"LogDialog")
        LogDialog.setWindowModality(Qt.WindowModal)
        LogDialog.resize(1280, 200)
        self.horizontalLayout = QHBoxLayout(LogDialog)
        self.horizontalLayout.setSpacing(1)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalLayout.setContentsMargins(2, 2, 2, 2)
        self.frame = QFrame(LogDialog)
        self.frame.setObjectName(u"frame")
        self.frame.setFrameShape(QFrame.StyledPanel)
        self.frame.setFrameShadow(QFrame.Raised)
        self.verticalLayout = QVBoxLayout(self.frame)
        self.verticalLayout.setSpacing(4)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.chkStayTop = QCheckBox(self.frame)
        self.chkStayTop.setObjectName(u"chkStayTop")
        self.chkStayTop.setLayoutDirection(Qt.LeftToRight)
        self.chkStayTop.setIconSize(QSize(19, 19))
        self.chkStayTop.setChecked(True)

        self.verticalLayout.addWidget(self.chkStayTop, 0, Qt.AlignHCenter)

        self.btnClearLogs = QPushButton(self.frame)
        self.btnClearLogs.setObjectName(u"btnClearLogs")

        self.verticalLayout.addWidget(self.btnClearLogs)

        self.btnEndLogs = QPushButton(self.frame)
        self.btnEndLogs.setObjectName(u"btnEndLogs")

        self.verticalLayout.addWidget(self.btnEndLogs)

        self.btnSaveLogs = QPushButton(self.frame)
        self.btnSaveLogs.setObjectName(u"btnSaveLogs")

        self.verticalLayout.addWidget(self.btnSaveLogs)

        self.verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout.addItem(self.verticalSpacer)


        self.horizontalLayout.addWidget(self.frame)

        self.txtLog = QTextEdit(LogDialog)
        self.txtLog.setObjectName(u"txtLog")
        sizePolicy = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.txtLog.sizePolicy().hasHeightForWidth())
        self.txtLog.setSizePolicy(sizePolicy)
        self.txtLog.setMinimumSize(QSize(0, 50))
        self.txtLog.setSizeIncrement(QSize(0, 100))
        self.txtLog.setBaseSize(QSize(0, 100))
        self.txtLog.setFrameShape(QFrame.WinPanel)
        self.txtLog.setReadOnly(True)

        self.horizontalLayout.addWidget(self.txtLog)


        self.retranslateUi(LogDialog)

        QMetaObject.connectSlotsByName(LogDialog)
    # setupUi

    def retranslateUi(self, LogDialog):
        LogDialog.setWindowTitle(QCoreApplication.translate("LogDialog", u"\u6267\u884c\u8bb0\u5f55", None))
        self.chkStayTop.setText(QCoreApplication.translate("LogDialog", u"\u7f6e\u9876\u663e\u793a", None))
        self.btnClearLogs.setText(QCoreApplication.translate("LogDialog", u"\u6e05\u7a7a\u8bb0\u5f55", None))
        self.btnEndLogs.setText(QCoreApplication.translate("LogDialog", u"\u6eda\u81f3\u6700\u5e95", None))
        self.btnSaveLogs.setText(QCoreApplication.translate("LogDialog", u"\u4fdd\u5b58\u65e5\u5fd7", None))
    # retranslateUi

