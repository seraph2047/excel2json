# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'treeview.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_TreeViewDialog(object):
    def setupUi(self, TreeViewDialog):
        if not TreeViewDialog.objectName():
            TreeViewDialog.setObjectName(u"TreeViewDialog")
        TreeViewDialog.setWindowModality(Qt.WindowModal)
        TreeViewDialog.resize(400, 360)
        self.verticalLayout = QVBoxLayout(TreeViewDialog)
        self.verticalLayout.setSpacing(1)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.verticalLayout.setContentsMargins(1, 1, 1, 1)
        self.treeWidget = QTreeWidget(TreeViewDialog)
        __qtreewidgetitem = QTreeWidgetItem()
        __qtreewidgetitem.setText(0, u"1");
        self.treeWidget.setHeaderItem(__qtreewidgetitem)
        self.treeWidget.setObjectName(u"treeWidget")
        sizePolicy = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(3)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.treeWidget.sizePolicy().hasHeightForWidth())
        self.treeWidget.setSizePolicy(sizePolicy)
        self.treeWidget.setMinimumSize(QSize(202, 0))
        self.treeWidget.setFrameShape(QFrame.WinPanel)
        self.treeWidget.header().setVisible(False)

        self.verticalLayout.addWidget(self.treeWidget)


        self.retranslateUi(TreeViewDialog)

        QMetaObject.connectSlotsByName(TreeViewDialog)
    # setupUi

    def retranslateUi(self, TreeViewDialog):
        TreeViewDialog.setWindowTitle(QCoreApplication.translate("TreeViewDialog", u"\u6570\u636e\u7ed3\u6784", None))
    # retranslateUi

