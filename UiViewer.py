# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'viewer.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_ViewerDialog(object):
    def setupUi(self, ViewerDialog):
        if not ViewerDialog.objectName():
            ViewerDialog.setObjectName(u"ViewerDialog")
        ViewerDialog.resize(1229, 756)
        self.verticalLayout = QVBoxLayout(ViewerDialog)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.verticalLayout.setContentsMargins(2, 2, 2, 2)
        self.listData = QTableWidget(ViewerDialog)
        self.listData.setObjectName(u"listData")
        self.listData.setMinimumSize(QSize(300, 0))
        self.listData.setSizeIncrement(QSize(0, 0))
        self.listData.setToolTipDuration(-1)
        self.listData.setFrameShape(QFrame.WinPanel)
        self.listData.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.listData.setSelectionMode(QAbstractItemView.SingleSelection)
        self.listData.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.listData.horizontalHeader().setMinimumSectionSize(30)
        self.listData.verticalHeader().setVisible(False)
        self.listData.verticalHeader().setMinimumSectionSize(24)
        self.listData.verticalHeader().setDefaultSectionSize(24)

        self.verticalLayout.addWidget(self.listData)


        self.retranslateUi(ViewerDialog)

        QMetaObject.connectSlotsByName(ViewerDialog)
    # setupUi

    def retranslateUi(self, ViewerDialog):
        ViewerDialog.setWindowTitle(QCoreApplication.translate("ViewerDialog", u"\u67e5\u770b\u6570\u636e\u5185\u5bb9", None))
    # retranslateUi

