from abc import abstractmethod

from ExcelData import FieldType
import datetime

class ICodeGenerator:

    @abstractmethod
    def getFieldType(self, fieldType: FieldType):
        """
        获取java字段类型文本
        :param fieldType: FieldType
        :return: getFieldType(FieldType) --> str 对应代码的类型字符串
        """
        pass

    @abstractmethod
    def loadTemplate(self):
        """
        加载代码模板
        :return: loadTemplate() --> bool 是否成功加载
        """
        pass

    @abstractmethod
    def generateCode(self, exportDir:str, exportVersion:str, exportTime:datetime):
        """
        生成代码 在执行前，必须先执行loadTemplate()
        :param exportVersion: str 发布版本号
        :param exportTime: datetime 发布日期
        :return: bool 是否顺利
        """
        pass