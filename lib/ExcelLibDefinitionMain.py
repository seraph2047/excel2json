from typing import List

from lib.ExcelLibDefinition import ExcelLibDefinition
from lib.ExcelLibDefinitionTable import ExcelLibDefinitionTable


class ExcelLibDefinitionMain:
    version: str
    date: str
    author: str
    definitions: List[ExcelLibDefinition]
    tables: List[ExcelLibDefinitionTable]

