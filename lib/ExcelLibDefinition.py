# 数据类型判断常量
from typing import List, Dict

FIELD_TYPE_INTEGER = "Integer";
FIELD_TYPE_LONG = "Long";
FIELD_TYPE_STRING = "String";
FIELD_TYPE_FLOAT = "Float";
FIELD_TYPE_DOUBLE = "Double";
FIELD_TYPE_BOOL = "Boolean";
# 加载索引方式类型判断常量
LOAD_TYPE_LIST = "List";
LOAD_TYPE_MAP = "Map";
LOAD_TYPE_MKMAP = "MultKeyMap";
LOAD_TYPE_MAP2LIST = "Map2List";


class ExcelLibDefinition:
    keys: List[Dict[str, str]]
    """主键   Map<键名,主键类型(ExcelLibDefinition.FIELD_TYPE_*)>"""

    mainKey: str
    """索引主键名"""

    mainKeyType: str
    """主键类型，对应 ExcelLibDefinition.FIELD_TYPE_*"""

    loadType: str
    """ 加载索引方式类型，对应 ExcelLibDefinition.LOAD_TYPE_* """

    platforms: List[str]
    """本包工具生成时支持的平台类型（展示用，在开发代码中无实际逻辑）"""

    loadWeight: int
    """加载权重，数值越小加载越早，相同权重随机排列"""

    loadJson: str
    """需要加载的表格"""

    className: str
    """需要加载的类名"""

    comment: str
    """注释"""

    dataName: str
    """数据表名，含索引方式命名"""

    group: str
    """组别名"""

    loadRelations: List[str]
    """关联加载"""

    dataFullName: str
    """数据表名，含索引方式命名+索引字段命名"""

    mainKeys: List[str] = None

    def getMainKeys(self, forceLoad: bool = False):
        """
        多个主键调用这个方法提取
        :param forceLoad: 是否强制更新
        :return: List[str]
        """
        if self.mainKeys is None or forceLoad:
            self.mainKeys = [""] * len(self.keys)  # mainKeys = new String[keys.size()];
            for i in range(0, len(self.keys)):
                for key, val in self.keys[i].items(): # mainKeys[i] = keys.get(i).get("name");
                    self.mainKeys[i] = key
        return self.mainKeys

