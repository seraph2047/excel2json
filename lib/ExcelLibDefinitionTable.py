import datetime

import Util


class ExcelLibDefinitionTable:
    jsonName: str
    comment: str
    md5: str
    date: datetime
    version: str
    author: str

