from typing import List, Dict

import Util
from lib.ExcelLibDefinition import ExcelLibDefinition
from lib.ExcelLibDefinitionMain import ExcelLibDefinitionMain
from lib.ExcelLibDefinitionTable import ExcelLibDefinitionTable


def parseDefinitionFromJson(json: dict):
    """
    用json数据创建ExcelLibDefinition
    :param json:
    :return: ExcelLibDefinition
    """
    e: ExcelLibDefinition = ExcelLibDefinition()
    e.className = json["className"]
    e.comment = json["comment"]
    e.dataFullName = json["dataFullName"]
    e.dataName = json["dataName"]
    e.group = json["group"]
    e.loadJson = json["loadJson"]
    e.loadWeight = json["loadWeight"]
    e.mainKey = json["mainKey"]
    e.mainKeyType = json["mainKeyType"]
    e.loadType = json["type"]
    e.keys = []
    keysJson: List[Dict] = json["keys"]
    for keyJson in keysJson:
        name: str = keyJson["name"]
        loadType: str = keyJson["type"]
        keyMap: Dict[str, str] = dict()
        keyMap[name] = loadType
        e.keys.append(keyMap)
    e.loadRelations = []
    relJson: List[str] = json["loadRelations"]
    for relation in relJson:
        e.loadRelations.append(relJson)

    e.platforms = []
    platformsJson: List[str] = json["platforms"]
    for platform in platformsJson:
        e.platforms.append(platform)

    return e

def parseDefinitionTableFromJson(json: dict):
    """
    用json数据创建ExcelLibDefinition
    :param json:
    :return: ExcelLibDefinitionTable
    """
    e: ExcelLibDefinitionTable = ExcelLibDefinitionTable()
    e.jsonName = json["jsonName"]
    e.comment = json["comment"]
    e.md5 = json["md5"]
    e.date = Util.getDataTimeFromString(json["date"])
    e.version = json["version"]
    e.author = json["author"]
    return e


def parseDefinitionMainFromJson(json: dict):
    """
    用json数据创建ExcelLibDefinitionMain
    :param json:
    :return: ExcelLibDefinitionMain
    """
    e: ExcelLibDefinitionMain = ExcelLibDefinitionMain()
    headData: dict = json.get("header")
    e.version = headData["version"]
    e.date = headData["date"]
    e.author = headData["author"]

    jsonDefinitions: [] = json.get("defines")
    jsonTables: [] = json.get("tables")
    e.definitions = []
    for define in jsonDefinitions:
        ldf: ExcelLibDefinition = parseDefinitionFromJson(define)
        e.definitions.append(ldf)
    e.tables = []
    for table in jsonTables:
        ltb: ExcelLibDefinitionTable = parseDefinitionTableFromJson(table)
        e.tables.append(ltb)
    return e