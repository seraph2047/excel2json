import json
import requests

import Util
from Logger import Logger

LOGIN_RESULT_SUCCESS:int = 1
LOGIN_RESULT_USER_OR_PASS_WRONG:int = -99
LOGIN_RESULT_NOT_IN_WHITE_LIST:int  = -401
"""不在白名单"""
LOGIN_RESULT_REJECT:int  = -403
"""权限不足"""
LOGIN_RESULT_NOT_FOUND_TAG:int  = -404
"""未找到tag"""

PUBLISH_RESULT_SUCCESS:int = 1
PUBLISH_RESULT_ERROR:int = -100
PUBLISH_RESULT_ERROR_WRONG_DEFINTION:int = -120
PUBLISH_RESULT_ERROR_WRONG_TABLE:int = -121

UPLOAD_COMM_RESULT_SUCCESS:int = 1
UPLOAD_COMM_RESULT_EXISTED:int = 2
UPLOAD_COMM_RESULT_ERROR:int = -1
UPLOAD_COMM_ERROR_TOKEN_NOT_FOUND:int = -100
UPLOAD_COMM_ERROR_EMPTY:int = -101
UPLOAD_COMM_ERROR_FORMAT:int = -102

UPLOAD_TABLE_RESULT_ERROR_EXISTED:int = 2
UPLOAD_TABLE_RESULT_ERROR_DEFINE_NOT_FOUND:int = -110



def getHeader():
    headers = {
        'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Mobile Safari/537.36',
        'Content-Type': 'application/x-www-form-urlencoded'
    }
    return headers

def loginConfigServer(host: str, tag: str, username: str, password: str):
    req: str = "/sys/login"
    postData = {
        "tag": tag,
        "username": username,
        "password": password
    }
    headers = getHeader()
    url: str = Util.fixUrlBackslash(host + req)
    print("publish post:" + url)
    try:
        r = requests.post(url, data=postData, headers=headers)
    except Exception as e:
        Logger().logError("连接服务器出错！")
        Logger().logWarning(str(e))
        return None
    result = r.json()
    return result


def postDefinition(host: str, tag: str, token: str, version: str, jsonStr: str):
    postData: {} = {}
    postData["tag"] = tag
    postData["token"] = token
    postData["version"] = version
    postData["json"] = jsonStr
    headers = getHeader()
    url: str = Util.fixUrlBackslash(host + "/sys/upload/definition")
    try:
        r = requests.post(url, data=postData, headers=headers)
    except Exception as e:
        Logger().logError("连接服务器出错！")
        Logger().logWarning(str(e))
        return None
    result = r.json()
    return result


def postTable(host: str, tag:str, token: str, jsonName: str, version: str, jsonStr: str):
    # jsonStr = json.dumps(jsonData, ensure_ascii=False, indent=2)
    postData: {} = {}
    postData["tag"] = tag
    postData["token"] = token
    postData["jsonName"] = jsonName
    postData["version"] = version
    postData["json"] = jsonStr
    headers = getHeader()
    url: str = Util.fixUrlBackslash(host + "/sys/upload/table")
    try:
        r = requests.post(url, data=postData, headers=headers)
    except Exception as e:
        Logger().logError("连接服务器出错！")
        Logger().logWarning(str(e))
        return None
    result = r.json()
    print(result)
    return result

def publishData(host: str, tag: str, token: str, opType: int, isClearOldTable:bool, totalSize: int):
    postData: {} = {}
    postData["tag"] = tag
    postData["opType"] = opType
    postData["token"] = token
    postData["clear"] = isClearOldTable
    postData["totalSize"] = totalSize
    headers = getHeader()
    url: str = Util.fixUrlBackslash(host + "/sys/publish")
    try:
        r = requests.post(url, data=postData, headers=headers)
    except Exception as e:
        Logger().logError("连接服务器出错！")
        Logger().logWarning(str(e))
        return None
    result = r.json()
    print(result)
    return result

def getLastVersion(host: str, tag: str):
    headers = getHeader()
    url: str = Util.fixUrlBackslash(host + "/sys/version?tag=" + tag)
    try:
        r = requests.get(url, headers=headers)
    except Exception as e:
        Logger().logError("连接服务器出错！")
        Logger().logWarning(str(e))
        return None
    result = r.json()
    print(result)
    return result
