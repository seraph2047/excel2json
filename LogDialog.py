from PySide2 import QtCore
from PySide2.QtWidgets import QWidget, QDialog

import Logger
from UiLog import Ui_LogDialog

class LogDialog(QWidget, Ui_LogDialog):
    logger: Logger

    noTop: QtCore.Qt.WindowFlags

    def __init__(self):
        super(LogDialog, self).__init__()

        self.ui = Ui_LogDialog()
        self.ui.setupUi(self)

        self.logger = Logger.Logger()
        self.logger.bindOuttext(self, self.ui.txtLog)

        self.ui.btnClearLogs.clicked.connect(self.onBtnClearLogsClick)  # 全部删除
        self.ui.btnEndLogs.clicked.connect(self.onBtnEndLogs)
        self.ui.btnSaveLogs.clicked.connect(self.onBtnSaveLogs)
        self.ui.chkStayTop.clicked.connect(self.onChkStayTopClick) #置顶
        # 把窗体固定最前
        self.noTop = self.windowFlags();
        # self.setWindowFlags(QtCore.Qt.WindowStaysOnTopHint)
        self.setWindowFlags(self.noTop)
        self.ui.chkStayTop.setChecked(False)

    def show(self):
        print("打开日志UI")
        super(LogDialog, self).show()

    def reShow(self):
        self.setWindowFlags(QtCore.Qt.WindowStaysOnTopHint)
        self.show()
        if not self.ui.chkStayTop.isChecked():
            self.setWindowFlags(self.noTop)
            self.show()

    def onChkStayTopClick(self):
        if self.ui.chkStayTop.isChecked():
            self.setWindowFlags(QtCore.Qt.WindowStaysOnTopHint)
            self.show()
        else:
            self.setWindowFlags(self.noTop)
            self.show()


    def onBtnClearLogsClick(self):
        """清除控制台内容"""
        self.logger.clear()

    def onBtnEndLogs(self):
        """滚到屏底"""
        pass

    def onBtnSaveLogs(self):
        """保存日志"""
        pass