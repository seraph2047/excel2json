# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'publish.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_PublishDialog(object):
    def setupUi(self, PublishDialog):
        if not PublishDialog.objectName():
            PublishDialog.setObjectName(u"PublishDialog")
        PublishDialog.setWindowModality(Qt.WindowModal)
        PublishDialog.resize(1280, 720)
        self.verticalLayout_3 = QVBoxLayout(PublishDialog)
        self.verticalLayout_3.setObjectName(u"verticalLayout_3")
        self.line = QFrame(PublishDialog)
        self.line.setObjectName(u"line")
        self.line.setFrameShape(QFrame.HLine)
        self.line.setFrameShadow(QFrame.Sunken)

        self.verticalLayout_3.addWidget(self.line)

        self.tabWidget = QTabWidget(PublishDialog)
        self.tabWidget.setObjectName(u"tabWidget")
        font = QFont()
        font.setFamily(u"\u5fae\u8f6f\u96c5\u9ed1")
        font.setPointSize(11)
        font.setBold(True)
        font.setWeight(75)
        self.tabWidget.setFont(font)
        self.tabWidget.setAutoFillBackground(True)
        self.tab = QWidget()
        self.tab.setObjectName(u"tab")
        self.tab.setAutoFillBackground(True)
        self.verticalLayout_4 = QVBoxLayout(self.tab)
        self.verticalLayout_4.setObjectName(u"verticalLayout_4")
        self.splitter = QSplitter(self.tab)
        self.splitter.setObjectName(u"splitter")
        sizePolicy = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.splitter.sizePolicy().hasHeightForWidth())
        self.splitter.setSizePolicy(sizePolicy)
        self.splitter.setMaximumSize(QSize(16777215, 16777215))
        self.splitter.setOrientation(Qt.Horizontal)
        self.splitter.setHandleWidth(6)
        self.splitter.setChildrenCollapsible(False)
        self.groupBox = QGroupBox(self.splitter)
        self.groupBox.setObjectName(u"groupBox")
        sizePolicy1 = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Preferred)
        sizePolicy1.setHorizontalStretch(3)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.groupBox.sizePolicy().hasHeightForWidth())
        self.groupBox.setSizePolicy(sizePolicy1)
        self.groupBox.setFont(font)
        self.verticalLayout = QVBoxLayout(self.groupBox)
        self.verticalLayout.setSpacing(7)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.listJson = QTableWidget(self.groupBox)
        self.listJson.setObjectName(u"listJson")
        sizePolicy2 = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        sizePolicy2.setHorizontalStretch(7)
        sizePolicy2.setVerticalStretch(0)
        sizePolicy2.setHeightForWidth(self.listJson.sizePolicy().hasHeightForWidth())
        self.listJson.setSizePolicy(sizePolicy2)
        self.listJson.setMinimumSize(QSize(200, 0))
        self.listJson.setSizeIncrement(QSize(800, 0))
        self.listJson.setBaseSize(QSize(800, 0))
        font1 = QFont()
        font1.setFamily(u"\u5fae\u8f6f\u96c5\u9ed1")
        font1.setPointSize(9)
        font1.setBold(False)
        font1.setWeight(50)
        self.listJson.setFont(font1)
        self.listJson.setToolTipDuration(0)
        self.listJson.setFrameShape(QFrame.WinPanel)
        self.listJson.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.listJson.setSelectionMode(QAbstractItemView.SingleSelection)
        self.listJson.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.listJson.setColumnCount(0)
        self.listJson.horizontalHeader().setVisible(False)
        self.listJson.horizontalHeader().setMinimumSectionSize(30)
        self.listJson.verticalHeader().setVisible(False)
        self.listJson.verticalHeader().setMinimumSectionSize(24)
        self.listJson.verticalHeader().setDefaultSectionSize(24)

        self.verticalLayout.addWidget(self.listJson)

        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.btnSelectModi = QPushButton(self.groupBox)
        self.btnSelectModi.setObjectName(u"btnSelectModi")
        self.btnSelectModi.setMinimumSize(QSize(0, 30))
        self.btnSelectModi.setFont(font1)

        self.horizontalLayout.addWidget(self.btnSelectModi)

        self.btnSelectAll = QPushButton(self.groupBox)
        self.btnSelectAll.setObjectName(u"btnSelectAll")
        self.btnSelectAll.setMinimumSize(QSize(0, 30))
        self.btnSelectAll.setFont(font1)

        self.horizontalLayout.addWidget(self.btnSelectAll)

        self.btnSelectNone = QPushButton(self.groupBox)
        self.btnSelectNone.setObjectName(u"btnSelectNone")
        self.btnSelectNone.setMinimumSize(QSize(0, 30))
        self.btnSelectNone.setFont(font1)

        self.horizontalLayout.addWidget(self.btnSelectNone)


        self.verticalLayout.addLayout(self.horizontalLayout)

        self.splitter.addWidget(self.groupBox)
        self.groupBox_2 = QGroupBox(self.splitter)
        self.groupBox_2.setObjectName(u"groupBox_2")
        sizePolicy3 = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Preferred)
        sizePolicy3.setHorizontalStretch(2)
        sizePolicy3.setVerticalStretch(0)
        sizePolicy3.setHeightForWidth(self.groupBox_2.sizePolicy().hasHeightForWidth())
        self.groupBox_2.setSizePolicy(sizePolicy3)
        self.groupBox_2.setBaseSize(QSize(0, 0))
        self.groupBox_2.setFont(font)
        self.verticalLayout_2 = QVBoxLayout(self.groupBox_2)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.verticalLayout_6 = QVBoxLayout()
        self.verticalLayout_6.setObjectName(u"verticalLayout_6")
        self.listServer = QTableWidget(self.groupBox_2)
        self.listServer.setObjectName(u"listServer")
        sizePolicy4 = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        sizePolicy4.setHorizontalStretch(7)
        sizePolicy4.setVerticalStretch(3)
        sizePolicy4.setHeightForWidth(self.listServer.sizePolicy().hasHeightForWidth())
        self.listServer.setSizePolicy(sizePolicy4)
        self.listServer.setMinimumSize(QSize(200, 0))
        self.listServer.setSizeIncrement(QSize(800, 0))
        self.listServer.setBaseSize(QSize(800, 0))
        self.listServer.setFont(font1)
        self.listServer.setToolTipDuration(0)
        self.listServer.setFrameShape(QFrame.WinPanel)
        self.listServer.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.listServer.setSelectionMode(QAbstractItemView.SingleSelection)
        self.listServer.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.listServer.setColumnCount(0)
        self.listServer.horizontalHeader().setVisible(False)
        self.listServer.horizontalHeader().setMinimumSectionSize(30)
        self.listServer.verticalHeader().setVisible(False)
        self.listServer.verticalHeader().setMinimumSectionSize(24)
        self.listServer.verticalHeader().setDefaultSectionSize(24)

        self.verticalLayout_6.addWidget(self.listServer)

        self.gridLayout = QGridLayout()
        self.gridLayout.setObjectName(u"gridLayout")
        self.btnSetting = QPushButton(self.groupBox_2)
        self.btnSetting.setObjectName(u"btnSetting")
        self.btnSetting.setMinimumSize(QSize(0, 30))
        self.btnSetting.setFont(font1)

        self.gridLayout.addWidget(self.btnSetting, 0, 2, 1, 1)

        self.label = QLabel(self.groupBox_2)
        self.label.setObjectName(u"label")
        self.label.setFont(font1)

        self.gridLayout.addWidget(self.label, 0, 0, 1, 1)

        self.label_2 = QLabel(self.groupBox_2)
        self.label_2.setObjectName(u"label_2")
        self.label_2.setFont(font1)

        self.gridLayout.addWidget(self.label_2, 2, 0, 1, 1)

        self.txtPassword = QLineEdit(self.groupBox_2)
        self.txtPassword.setObjectName(u"txtPassword")
        sizePolicy5 = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
        sizePolicy5.setHorizontalStretch(0)
        sizePolicy5.setVerticalStretch(0)
        sizePolicy5.setHeightForWidth(self.txtPassword.sizePolicy().hasHeightForWidth())
        self.txtPassword.setSizePolicy(sizePolicy5)
        self.txtPassword.setFont(font1)
        self.txtPassword.setMaxLength(32767)
        self.txtPassword.setEchoMode(QLineEdit.PasswordEchoOnEdit)
        self.txtPassword.setPlaceholderText(u"")

        self.gridLayout.addWidget(self.txtPassword, 2, 1, 1, 1)

        self.txtUsername = QLineEdit(self.groupBox_2)
        self.txtUsername.setObjectName(u"txtUsername")
        sizePolicy5.setHeightForWidth(self.txtUsername.sizePolicy().hasHeightForWidth())
        self.txtUsername.setSizePolicy(sizePolicy5)
        self.txtUsername.setFont(font1)
        self.txtUsername.setMaxLength(16)
        self.txtUsername.setPlaceholderText(u"")

        self.gridLayout.addWidget(self.txtUsername, 0, 1, 1, 1)

        self.btnSave = QPushButton(self.groupBox_2)
        self.btnSave.setObjectName(u"btnSave")
        self.btnSave.setFont(font1)

        self.gridLayout.addWidget(self.btnSave, 2, 2, 1, 1)


        self.verticalLayout_6.addLayout(self.gridLayout)


        self.verticalLayout_2.addLayout(self.verticalLayout_6)

        self.horizontalLayout_5 = QHBoxLayout()
        self.horizontalLayout_5.setObjectName(u"horizontalLayout_5")
        self.chkClearOldJson = QCheckBox(self.groupBox_2)
        self.chkClearOldJson.setObjectName(u"chkClearOldJson")
        self.chkClearOldJson.setFont(font1)

        self.horizontalLayout_5.addWidget(self.chkClearOldJson)

        self.btnPublishJson = QPushButton(self.groupBox_2)
        self.btnPublishJson.setObjectName(u"btnPublishJson")
        sizePolicy5.setHeightForWidth(self.btnPublishJson.sizePolicy().hasHeightForWidth())
        self.btnPublishJson.setSizePolicy(sizePolicy5)
        self.btnPublishJson.setMinimumSize(QSize(0, 30))
        font2 = QFont()
        font2.setFamily(u"\u5fae\u8f6f\u96c5\u9ed1")
        font2.setPointSize(9)
        font2.setBold(True)
        font2.setWeight(75)
        self.btnPublishJson.setFont(font2)

        self.horizontalLayout_5.addWidget(self.btnPublishJson)


        self.verticalLayout_2.addLayout(self.horizontalLayout_5)

        self.splitter.addWidget(self.groupBox_2)

        self.verticalLayout_4.addWidget(self.splitter)

        self.tabWidget.addTab(self.tab, "")
        self.tab_2 = QWidget()
        self.tab_2.setObjectName(u"tab_2")
        self.tab_2.setAutoFillBackground(True)
        self.verticalLayout_5 = QVBoxLayout(self.tab_2)
        self.verticalLayout_5.setObjectName(u"verticalLayout_5")
        self.splitter_2 = QSplitter(self.tab_2)
        self.splitter_2.setObjectName(u"splitter_2")
        sizePolicy.setHeightForWidth(self.splitter_2.sizePolicy().hasHeightForWidth())
        self.splitter_2.setSizePolicy(sizePolicy)
        self.splitter_2.setOrientation(Qt.Horizontal)
        self.splitter_2.setChildrenCollapsible(False)
        self.groupBox_4 = QGroupBox(self.splitter_2)
        self.groupBox_4.setObjectName(u"groupBox_4")
        sizePolicy6 = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        sizePolicy6.setHorizontalStretch(2)
        sizePolicy6.setVerticalStretch(0)
        sizePolicy6.setHeightForWidth(self.groupBox_4.sizePolicy().hasHeightForWidth())
        self.groupBox_4.setSizePolicy(sizePolicy6)
        self.verticalLayout_9 = QVBoxLayout(self.groupBox_4)
        self.verticalLayout_9.setObjectName(u"verticalLayout_9")
        self.splitter_3 = QSplitter(self.groupBox_4)
        self.splitter_3.setObjectName(u"splitter_3")
        self.splitter_3.setOrientation(Qt.Vertical)
        self.txtFtpLog = QTextEdit(self.splitter_3)
        self.txtFtpLog.setObjectName(u"txtFtpLog")
        sizePolicy.setHeightForWidth(self.txtFtpLog.sizePolicy().hasHeightForWidth())
        self.txtFtpLog.setSizePolicy(sizePolicy)
        self.txtFtpLog.setMinimumSize(QSize(0, 50))
        self.txtFtpLog.setSizeIncrement(QSize(0, 100))
        self.txtFtpLog.setBaseSize(QSize(0, 100))
        self.txtFtpLog.setFont(font1)
        self.txtFtpLog.setFrameShape(QFrame.WinPanel)
        self.txtFtpLog.setReadOnly(True)
        self.splitter_3.addWidget(self.txtFtpLog)
        self.txtFtpConfigFile = QTextEdit(self.splitter_3)
        self.txtFtpConfigFile.setObjectName(u"txtFtpConfigFile")
        sizePolicy.setHeightForWidth(self.txtFtpConfigFile.sizePolicy().hasHeightForWidth())
        self.txtFtpConfigFile.setSizePolicy(sizePolicy)
        self.txtFtpConfigFile.setMinimumSize(QSize(0, 50))
        self.txtFtpConfigFile.setSizeIncrement(QSize(0, 100))
        self.txtFtpConfigFile.setBaseSize(QSize(0, 100))
        self.txtFtpConfigFile.setFont(font1)
        self.txtFtpConfigFile.setFrameShape(QFrame.WinPanel)
        self.txtFtpConfigFile.setReadOnly(False)
        self.splitter_3.addWidget(self.txtFtpConfigFile)

        self.verticalLayout_9.addWidget(self.splitter_3)

        self.splitter_2.addWidget(self.groupBox_4)
        self.groupBox_3 = QGroupBox(self.splitter_2)
        self.groupBox_3.setObjectName(u"groupBox_3")
        sizePolicy3.setHeightForWidth(self.groupBox_3.sizePolicy().hasHeightForWidth())
        self.groupBox_3.setSizePolicy(sizePolicy3)
        self.groupBox_3.setMinimumSize(QSize(0, 0))
        self.groupBox_3.setBaseSize(QSize(0, 0))
        self.verticalLayout_8 = QVBoxLayout(self.groupBox_3)
        self.verticalLayout_8.setObjectName(u"verticalLayout_8")
        self.verticalLayout_7 = QVBoxLayout()
        self.verticalLayout_7.setObjectName(u"verticalLayout_7")
        self.horizontalLayout_9 = QHBoxLayout()
        self.horizontalLayout_9.setObjectName(u"horizontalLayout_9")
        self.listFtpServer = QTableWidget(self.groupBox_3)
        self.listFtpServer.setObjectName(u"listFtpServer")
        self.listFtpServer.setMinimumSize(QSize(400, 0))
        self.listFtpServer.setSizeIncrement(QSize(0, 0))
        self.listFtpServer.setBaseSize(QSize(0, 0))
        self.listFtpServer.setFont(font1)
        self.listFtpServer.setSelectionMode(QAbstractItemView.SingleSelection)
        self.listFtpServer.setSelectionBehavior(QAbstractItemView.SelectRows)

        self.horizontalLayout_9.addWidget(self.listFtpServer)

        self.verticalLayout_10 = QVBoxLayout()
        self.verticalLayout_10.setObjectName(u"verticalLayout_10")
        self.verticalLayout_10.setContentsMargins(-1, -1, 0, -1)
        self.btnFtpAdd = QPushButton(self.groupBox_3)
        self.btnFtpAdd.setObjectName(u"btnFtpAdd")
        sizePolicy7 = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        sizePolicy7.setHorizontalStretch(0)
        sizePolicy7.setVerticalStretch(0)
        sizePolicy7.setHeightForWidth(self.btnFtpAdd.sizePolicy().hasHeightForWidth())
        self.btnFtpAdd.setSizePolicy(sizePolicy7)
        self.btnFtpAdd.setMaximumSize(QSize(50, 16777215))
        self.btnFtpAdd.setSizeIncrement(QSize(0, 0))
        self.btnFtpAdd.setBaseSize(QSize(0, 0))
        self.btnFtpAdd.setFont(font1)

        self.verticalLayout_10.addWidget(self.btnFtpAdd)

        self.btnFtpDel = QPushButton(self.groupBox_3)
        self.btnFtpDel.setObjectName(u"btnFtpDel")
        sizePolicy7.setHeightForWidth(self.btnFtpDel.sizePolicy().hasHeightForWidth())
        self.btnFtpDel.setSizePolicy(sizePolicy7)
        self.btnFtpDel.setMaximumSize(QSize(50, 16777215))
        self.btnFtpDel.setSizeIncrement(QSize(0, 0))
        self.btnFtpDel.setBaseSize(QSize(0, 0))
        self.btnFtpDel.setFont(font1)

        self.verticalLayout_10.addWidget(self.btnFtpDel)

        self.verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_10.addItem(self.verticalSpacer)


        self.horizontalLayout_9.addLayout(self.verticalLayout_10)


        self.verticalLayout_7.addLayout(self.horizontalLayout_9)

        self.gridLayout_2 = QGridLayout()
        self.gridLayout_2.setObjectName(u"gridLayout_2")
        self.horizontalLayout_3 = QHBoxLayout()
        self.horizontalLayout_3.setObjectName(u"horizontalLayout_3")
        self.txtFtpUsername = QLineEdit(self.groupBox_3)
        self.txtFtpUsername.setObjectName(u"txtFtpUsername")
        self.txtFtpUsername.setFont(font1)

        self.horizontalLayout_3.addWidget(self.txtFtpUsername)

        self.label_6 = QLabel(self.groupBox_3)
        self.label_6.setObjectName(u"label_6")
        self.label_6.setMinimumSize(QSize(80, 0))
        self.label_6.setFont(font1)
        self.label_6.setTextFormat(Qt.RichText)
        self.label_6.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.horizontalLayout_3.addWidget(self.label_6)

        self.txtFtpPassword = QLineEdit(self.groupBox_3)
        self.txtFtpPassword.setObjectName(u"txtFtpPassword")
        self.txtFtpPassword.setFont(font1)
        self.txtFtpPassword.setInputMethodHints(Qt.ImhNoAutoUppercase|Qt.ImhNoPredictiveText|Qt.ImhSensitiveData)
        self.txtFtpPassword.setEchoMode(QLineEdit.PasswordEchoOnEdit)

        self.horizontalLayout_3.addWidget(self.txtFtpPassword)


        self.gridLayout_2.addLayout(self.horizontalLayout_3, 6, 1, 1, 1)

        self.label_3 = QLabel(self.groupBox_3)
        self.label_3.setObjectName(u"label_3")
        self.label_3.setFont(font1)
        self.label_3.setTextFormat(Qt.RichText)
        self.label_3.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout_2.addWidget(self.label_3, 4, 0, 1, 1)

        self.horizontalLayout_7 = QHBoxLayout()
        self.horizontalLayout_7.setObjectName(u"horizontalLayout_7")
        self.btnPublishSave = QPushButton(self.groupBox_3)
        self.btnPublishSave.setObjectName(u"btnPublishSave")
        sizePolicy8 = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Fixed)
        sizePolicy8.setHorizontalStretch(0)
        sizePolicy8.setVerticalStretch(0)
        sizePolicy8.setHeightForWidth(self.btnPublishSave.sizePolicy().hasHeightForWidth())
        self.btnPublishSave.setSizePolicy(sizePolicy8)
        self.btnPublishSave.setMinimumSize(QSize(0, 0))
        font3 = QFont()
        font3.setFamily(u"\u5fae\u8f6f\u96c5\u9ed1")
        font3.setPointSize(10)
        font3.setBold(False)
        font3.setWeight(50)
        self.btnPublishSave.setFont(font3)

        self.horizontalLayout_7.addWidget(self.btnPublishSave)

        self.btnFtpGenConfig = QPushButton(self.groupBox_3)
        self.btnFtpGenConfig.setObjectName(u"btnFtpGenConfig")
        sizePolicy8.setHeightForWidth(self.btnFtpGenConfig.sizePolicy().hasHeightForWidth())
        self.btnFtpGenConfig.setSizePolicy(sizePolicy8)
        self.btnFtpGenConfig.setMinimumSize(QSize(0, 0))
        font4 = QFont()
        font4.setFamily(u"\u5fae\u8f6f\u96c5\u9ed1")
        font4.setPointSize(11)
        font4.setBold(False)
        font4.setWeight(50)
        self.btnFtpGenConfig.setFont(font4)

        self.horizontalLayout_7.addWidget(self.btnFtpGenConfig)

        self.btnPublishFtp = QPushButton(self.groupBox_3)
        self.btnPublishFtp.setObjectName(u"btnPublishFtp")
        sizePolicy8.setHeightForWidth(self.btnPublishFtp.sizePolicy().hasHeightForWidth())
        self.btnPublishFtp.setSizePolicy(sizePolicy8)
        self.btnPublishFtp.setMinimumSize(QSize(0, 0))
        font5 = QFont()
        font5.setFamily(u"\u5fae\u8f6f\u96c5\u9ed1")
        font5.setPointSize(10)
        font5.setBold(True)
        font5.setWeight(75)
        self.btnPublishFtp.setFont(font5)

        self.horizontalLayout_7.addWidget(self.btnPublishFtp)

        self.horizontalLayout_7.setStretch(0, 5)
        self.horizontalLayout_7.setStretch(1, 5)
        self.horizontalLayout_7.setStretch(2, 8)

        self.gridLayout_2.addLayout(self.horizontalLayout_7, 14, 1, 1, 1)

        self.label_10 = QLabel(self.groupBox_3)
        self.label_10.setObjectName(u"label_10")
        self.label_10.setFont(font1)

        self.gridLayout_2.addWidget(self.label_10, 0, 0, 1, 1)

        self.horizontalLayout_8 = QHBoxLayout()
        self.horizontalLayout_8.setObjectName(u"horizontalLayout_8")
        self.txtFtpIp = QLineEdit(self.groupBox_3)
        self.txtFtpIp.setObjectName(u"txtFtpIp")
        self.txtFtpIp.setFont(font1)

        self.horizontalLayout_8.addWidget(self.txtFtpIp)

        self.label_9 = QLabel(self.groupBox_3)
        self.label_9.setObjectName(u"label_9")
        self.label_9.setFont(font1)
        self.label_9.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.horizontalLayout_8.addWidget(self.label_9)

        self.txtFtpPort = QLineEdit(self.groupBox_3)
        self.txtFtpPort.setObjectName(u"txtFtpPort")
        self.txtFtpPort.setFont(font1)

        self.horizontalLayout_8.addWidget(self.txtFtpPort)

        self.horizontalLayout_8.setStretch(0, 6)
        self.horizontalLayout_8.setStretch(1, 1)
        self.horizontalLayout_8.setStretch(2, 2)

        self.gridLayout_2.addLayout(self.horizontalLayout_8, 1, 1, 1, 1)

        self.txtFtpCaption = QLineEdit(self.groupBox_3)
        self.txtFtpCaption.setObjectName(u"txtFtpCaption")
        self.txtFtpCaption.setFont(font1)
        self.txtFtpCaption.setReadOnly(True)

        self.gridLayout_2.addWidget(self.txtFtpCaption, 0, 1, 1, 1)

        self.horizontalLayout_10 = QHBoxLayout()
        self.horizontalLayout_10.setObjectName(u"horizontalLayout_10")
        self.txtFtpVersion = QLineEdit(self.groupBox_3)
        self.txtFtpVersion.setObjectName(u"txtFtpVersion")
        self.txtFtpVersion.setFont(font1)

        self.horizontalLayout_10.addWidget(self.txtFtpVersion)

        self.label_12 = QLabel(self.groupBox_3)
        self.label_12.setObjectName(u"label_12")
        self.label_12.setMinimumSize(QSize(100, 0))
        self.label_12.setFont(font1)
        self.label_12.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.horizontalLayout_10.addWidget(self.label_12)

        self.txtFtpZipName = QLineEdit(self.groupBox_3)
        self.txtFtpZipName.setObjectName(u"txtFtpZipName")
        self.txtFtpZipName.setFont(font1)

        self.horizontalLayout_10.addWidget(self.txtFtpZipName)


        self.gridLayout_2.addLayout(self.horizontalLayout_10, 10, 1, 1, 1)

        self.label_8 = QLabel(self.groupBox_3)
        self.label_8.setObjectName(u"label_8")
        self.label_8.setFont(font1)
        self.label_8.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout_2.addWidget(self.label_8, 3, 0, 1, 1)

        self.horizontalLayout_4 = QHBoxLayout()
        self.horizontalLayout_4.setObjectName(u"horizontalLayout_4")
        self.txtFtpCertPath = QLineEdit(self.groupBox_3)
        self.txtFtpCertPath.setObjectName(u"txtFtpCertPath")
        self.txtFtpCertPath.setFont(font1)

        self.horizontalLayout_4.addWidget(self.txtFtpCertPath)

        self.btnSelectCert = QPushButton(self.groupBox_3)
        self.btnSelectCert.setObjectName(u"btnSelectCert")
        self.btnSelectCert.setFont(font1)

        self.horizontalLayout_4.addWidget(self.btnSelectCert)


        self.gridLayout_2.addLayout(self.horizontalLayout_4, 4, 1, 1, 1)

        self.label_14 = QLabel(self.groupBox_3)
        self.label_14.setObjectName(u"label_14")
        self.label_14.setFont(font1)

        self.gridLayout_2.addWidget(self.label_14, 11, 0, 1, 1)

        self.label_4 = QLabel(self.groupBox_3)
        self.label_4.setObjectName(u"label_4")
        self.label_4.setFont(font1)
        self.label_4.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout_2.addWidget(self.label_4, 9, 0, 1, 1)

        self.txtFtpRemoteDir = QLineEdit(self.groupBox_3)
        self.txtFtpRemoteDir.setObjectName(u"txtFtpRemoteDir")
        self.txtFtpRemoteDir.setFont(font1)

        self.gridLayout_2.addWidget(self.txtFtpRemoteDir, 3, 1, 1, 1)

        self.label_7 = QLabel(self.groupBox_3)
        self.label_7.setObjectName(u"label_7")
        self.label_7.setFont(font1)
        self.label_7.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout_2.addWidget(self.label_7, 1, 0, 1, 1)

        self.label_11 = QLabel(self.groupBox_3)
        self.label_11.setObjectName(u"label_11")
        self.label_11.setFont(font1)

        self.gridLayout_2.addWidget(self.label_11, 10, 0, 1, 1)

        self.label_5 = QLabel(self.groupBox_3)
        self.label_5.setObjectName(u"label_5")
        self.label_5.setFont(font1)
        self.label_5.setTextFormat(Qt.RichText)
        self.label_5.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout_2.addWidget(self.label_5, 6, 0, 1, 1)

        self.horizontalLayout_6 = QHBoxLayout()
        self.horizontalLayout_6.setObjectName(u"horizontalLayout_6")
        self.txtFtpVer = QLineEdit(self.groupBox_3)
        self.txtFtpVer.setObjectName(u"txtFtpVer")
        self.txtFtpVer.setMaximumSize(QSize(16777215, 16777215))
        self.txtFtpVer.setFont(font1)
        self.txtFtpVer.setMaxLength(5)

        self.horizontalLayout_6.addWidget(self.txtFtpVer)

        self.btnCheckFtpVer = QPushButton(self.groupBox_3)
        self.btnCheckFtpVer.setObjectName(u"btnCheckFtpVer")
        self.btnCheckFtpVer.setMaximumSize(QSize(100, 16777215))
        self.btnCheckFtpVer.setSizeIncrement(QSize(0, 0))
        self.btnCheckFtpVer.setBaseSize(QSize(0, 0))
        self.btnCheckFtpVer.setFont(font1)

        self.horizontalLayout_6.addWidget(self.btnCheckFtpVer)

        self.label_13 = QLabel(self.groupBox_3)
        self.label_13.setObjectName(u"label_13")
        self.label_13.setMinimumSize(QSize(130, 0))
        self.label_13.setFont(font4)
        self.label_13.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.horizontalLayout_6.addWidget(self.label_13)

        self.txtFtpResVer = QLineEdit(self.groupBox_3)
        self.txtFtpResVer.setObjectName(u"txtFtpResVer")
        self.txtFtpResVer.setFont(font4)

        self.horizontalLayout_6.addWidget(self.txtFtpResVer)

        self.horizontalLayout_6.setStretch(0, 1)
        self.horizontalLayout_6.setStretch(1, 1)

        self.gridLayout_2.addLayout(self.horizontalLayout_6, 9, 1, 1, 1)

        self.horizontalLayout_11 = QHBoxLayout()
        self.horizontalLayout_11.setObjectName(u"horizontalLayout_11")
        self.labConfigName = QLabel(self.groupBox_3)
        self.labConfigName.setObjectName(u"labConfigName")
        self.labConfigName.setMinimumSize(QSize(0, 0))
        self.labConfigName.setBaseSize(QSize(0, 0))
        self.labConfigName.setFont(font1)

        self.horizontalLayout_11.addWidget(self.labConfigName)

        self.labFtpDefinition = QLabel(self.groupBox_3)
        self.labFtpDefinition.setObjectName(u"labFtpDefinition")
        self.labFtpDefinition.setFont(font1)

        self.horizontalLayout_11.addWidget(self.labFtpDefinition)


        self.gridLayout_2.addLayout(self.horizontalLayout_11, 11, 1, 1, 1)


        self.verticalLayout_7.addLayout(self.gridLayout_2)


        self.verticalLayout_8.addLayout(self.verticalLayout_7)

        self.splitter_2.addWidget(self.groupBox_3)

        self.verticalLayout_5.addWidget(self.splitter_2)

        self.tabWidget.addTab(self.tab_2, "")

        self.verticalLayout_3.addWidget(self.tabWidget)

        self.widget = QWidget(PublishDialog)
        self.widget.setObjectName(u"widget")
        sizePolicy8.setHeightForWidth(self.widget.sizePolicy().hasHeightForWidth())
        self.widget.setSizePolicy(sizePolicy8)
        self.widget.setMinimumSize(QSize(0, 40))
        self.horizontalLayout_2 = QHBoxLayout(self.widget)
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.horizontalLayout_2.setContentsMargins(-1, 1, -1, 1)
        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_2.addItem(self.horizontalSpacer)

        self.btnClose = QPushButton(self.widget)
        self.btnClose.setObjectName(u"btnClose")
        self.btnClose.setMinimumSize(QSize(0, 35))
        self.btnClose.setFont(font1)

        self.horizontalLayout_2.addWidget(self.btnClose)


        self.verticalLayout_3.addWidget(self.widget)


        self.retranslateUi(PublishDialog)

        self.tabWidget.setCurrentIndex(1)


        QMetaObject.connectSlotsByName(PublishDialog)
    # setupUi

    def retranslateUi(self, PublishDialog):
        PublishDialog.setWindowTitle(QCoreApplication.translate("PublishDialog", u"\u9884\u89c8\u4e0e\u53d1\u5e03", None))
        self.groupBox.setTitle(QCoreApplication.translate("PublishDialog", u"\u9009\u62e9\u53d1\u5e03\u6587\u4ef6", None))
        self.btnSelectModi.setText(QCoreApplication.translate("PublishDialog", u"\u9009\u53d8\u66f4", None))
        self.btnSelectAll.setText(QCoreApplication.translate("PublishDialog", u"\u5168\u9009", None))
        self.btnSelectNone.setText(QCoreApplication.translate("PublishDialog", u"\u5168\u4e0d\u9009", None))
        self.groupBox_2.setTitle(QCoreApplication.translate("PublishDialog", u"\u53d1\u5e03\u5230\u670d\u52a1\u5668", None))
        self.btnSetting.setText(QCoreApplication.translate("PublishDialog", u"\u914d\u7f6e\u670d\u52a1\u5668", None))
        self.label.setText(QCoreApplication.translate("PublishDialog", u"\u53d1\u5e03\u8d26\u53f7:", None))
        self.label_2.setText(QCoreApplication.translate("PublishDialog", u"\u53d1\u5e03\u5bc6\u7801:", None))
        self.txtPassword.setInputMask("")
        self.txtPassword.setText(QCoreApplication.translate("PublishDialog", u"12345", None))
        self.txtUsername.setText(QCoreApplication.translate("PublishDialog", u"user", None))
        self.btnSave.setText(QCoreApplication.translate("PublishDialog", u"\u4fdd\u5b58\u6743\u9274", None))
        self.chkClearOldJson.setText(QCoreApplication.translate("PublishDialog", u"\u53d1\u5e03\u65f6\u6e05\u7406\u6240\u6709\u65e7\u914d\u7f6e", None))
        self.btnPublishJson.setText(QCoreApplication.translate("PublishDialog", u"\u53d1\u5e03\u5230\u670d\u52a1\u5668", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab), QCoreApplication.translate("PublishDialog", u"\u53d1\u5e03\u4e13\u7528\u670d\u52a1\u5668", None))
        self.groupBox_4.setTitle(QCoreApplication.translate("PublishDialog", u"\u8fdc\u7a0b\u4fe1\u606f", None))
        self.groupBox_3.setTitle(QCoreApplication.translate("PublishDialog", u"FTP\u670d\u52a1\u5668\u5217\u8868", None))
        self.btnFtpAdd.setText(QCoreApplication.translate("PublishDialog", u"\u6dfb\u52a0", None))
        self.btnFtpDel.setText(QCoreApplication.translate("PublishDialog", u"\u5220\u9664", None))
        self.label_6.setText(QCoreApplication.translate("PublishDialog", u"\u5bc6\u7801\uff1a", None))
        self.txtFtpPassword.setText("")
        self.label_3.setText(QCoreApplication.translate("PublishDialog", u"\u8bc1\u4e66\uff1a", None))
        self.btnPublishSave.setText(QCoreApplication.translate("PublishDialog", u"\u4fdd\u5b58\u4fe1\u606f", None))
        self.btnFtpGenConfig.setText(QCoreApplication.translate("PublishDialog", u"\u7248\u672c\u6587\u4ef6\u751f\u6210", None))
        self.btnPublishFtp.setText(QCoreApplication.translate("PublishDialog", u"\u53d1\u5e03\u5230\u670d\u52a1\u5668", None))
        self.label_10.setText(QCoreApplication.translate("PublishDialog", u"\u552f\u4e00\u6807\u8bc6", None))
        self.label_9.setText(QCoreApplication.translate("PublishDialog", u"\u7aef\u53e3\uff1a", None))
        self.label_12.setText(QCoreApplication.translate("PublishDialog", u"\u6570\u636e\u5305\u540d\uff1a", None))
        self.label_8.setText(QCoreApplication.translate("PublishDialog", u"\u8fdc\u7a0b\u8def\u5f84\uff1a", None))
        self.btnSelectCert.setText(QCoreApplication.translate("PublishDialog", u"\u9009\u62e9\u8bc1\u4e66", None))
        self.label_14.setText(QCoreApplication.translate("PublishDialog", u"\u6570\u636e\u6587\u4ef6\uff1a", None))
        self.label_4.setText(QCoreApplication.translate("PublishDialog", u"\u5927\u7248\u672c\u53f7\uff1a", None))
        self.label_7.setText(QCoreApplication.translate("PublishDialog", u"FTP\u5730\u5740\uff1a", None))
        self.label_11.setText(QCoreApplication.translate("PublishDialog", u"\u663e\u793a\u7248\u672c\uff1a", None))
        self.label_5.setText(QCoreApplication.translate("PublishDialog", u"\u8d26\u53f7\uff1a", None))
        self.txtFtpVer.setText("")
        self.btnCheckFtpVer.setText(QCoreApplication.translate("PublishDialog", u"\u68c0\u67e5\u7248\u672c", None))
        self.label_13.setText(QCoreApplication.translate("PublishDialog", u"\u8d44\u6e90\u7248\u672c\u53f7\uff1a", None))
        self.labConfigName.setText(QCoreApplication.translate("PublishDialog", u"config.json", None))
        self.labFtpDefinition.setText(QCoreApplication.translate("PublishDialog", u"_definition.json", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_2), QCoreApplication.translate("PublishDialog", u"\u53d1\u5e03FTP", None))
        self.btnClose.setText(QCoreApplication.translate("PublishDialog", u"\u5173\u95ed", None))
    # retranslateUi

