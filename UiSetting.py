# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'setting.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_SettingDialog(object):
    def setupUi(self, SettingDialog):
        if not SettingDialog.objectName():
            SettingDialog.setObjectName(u"SettingDialog")
        SettingDialog.setWindowModality(Qt.WindowModal)
        SettingDialog.resize(1280, 1000)
        self.verticalLayout_4 = QVBoxLayout(SettingDialog)
        self.verticalLayout_4.setObjectName(u"verticalLayout_4")
        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.groupBox_2 = QGroupBox(SettingDialog)
        self.groupBox_2.setObjectName(u"groupBox_2")
        sizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.groupBox_2.sizePolicy().hasHeightForWidth())
        self.groupBox_2.setSizePolicy(sizePolicy)
        self.groupBox_2.setMaximumSize(QSize(16777215, 200))
        self.groupBox_2.setSizeIncrement(QSize(0, 200))
        font = QFont()
        font.setFamily(u"\u5fae\u8f6f\u96c5\u9ed1")
        self.groupBox_2.setFont(font)
        self.verticalLayout_3 = QVBoxLayout(self.groupBox_2)
        self.verticalLayout_3.setObjectName(u"verticalLayout_3")
        self.formLayout_2 = QFormLayout()
        self.formLayout_2.setObjectName(u"formLayout_2")
        self.label_3 = QLabel(self.groupBox_2)
        self.label_3.setObjectName(u"label_3")
        self.label_3.setFont(font)
        self.label_3.setLayoutDirection(Qt.LeftToRight)
        self.label_3.setAutoFillBackground(False)
        self.label_3.setScaledContents(False)
        self.label_3.setAlignment(Qt.AlignLeading|Qt.AlignLeft|Qt.AlignVCenter)

        self.formLayout_2.setWidget(0, QFormLayout.LabelRole, self.label_3)

        self.txtCfgSheetName = QLineEdit(self.groupBox_2)
        self.txtCfgSheetName.setObjectName(u"txtCfgSheetName")

        self.formLayout_2.setWidget(0, QFormLayout.FieldRole, self.txtCfgSheetName)

        self.label_9 = QLabel(self.groupBox_2)
        self.label_9.setObjectName(u"label_9")
        self.label_9.setFont(font)
        self.label_9.setLayoutDirection(Qt.LeftToRight)
        self.label_9.setAutoFillBackground(False)
        self.label_9.setScaledContents(False)
        self.label_9.setAlignment(Qt.AlignLeading|Qt.AlignLeft|Qt.AlignVCenter)

        self.formLayout_2.setWidget(1, QFormLayout.LabelRole, self.label_9)

        self.txtTagImportSheets = QLineEdit(self.groupBox_2)
        self.txtTagImportSheets.setObjectName(u"txtTagImportSheets")

        self.formLayout_2.setWidget(1, QFormLayout.FieldRole, self.txtTagImportSheets)

        self.label_4 = QLabel(self.groupBox_2)
        self.label_4.setObjectName(u"label_4")
        self.label_4.setFont(font)
        self.label_4.setToolTipDuration(1000)
        self.label_4.setLayoutDirection(Qt.LeftToRight)
        self.label_4.setAutoFillBackground(False)
        self.label_4.setScaledContents(False)
        self.label_4.setAlignment(Qt.AlignLeading|Qt.AlignLeft|Qt.AlignVCenter)

        self.formLayout_2.setWidget(2, QFormLayout.LabelRole, self.label_4)

        self.txtCfgIgnoreColumn = QLineEdit(self.groupBox_2)
        self.txtCfgIgnoreColumn.setObjectName(u"txtCfgIgnoreColumn")
        self.txtCfgIgnoreColumn.setTabletTracking(False)
        self.txtCfgIgnoreColumn.setToolTipDuration(-1)
#if QT_CONFIG(statustip)
        self.txtCfgIgnoreColumn.setStatusTip(u"")
#endif // QT_CONFIG(statustip)
#if QT_CONFIG(whatsthis)
        self.txtCfgIgnoreColumn.setWhatsThis(u"")
#endif // QT_CONFIG(whatsthis)
#if QT_CONFIG(accessibility)
        self.txtCfgIgnoreColumn.setAccessibleName(u"")
#endif // QT_CONFIG(accessibility)
#if QT_CONFIG(accessibility)
        self.txtCfgIgnoreColumn.setAccessibleDescription(u"")
#endif // QT_CONFIG(accessibility)
        self.txtCfgIgnoreColumn.setPlaceholderText(u"")

        self.formLayout_2.setWidget(2, QFormLayout.FieldRole, self.txtCfgIgnoreColumn)

        self.verticalSpacer_4 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.formLayout_2.setItem(3, QFormLayout.FieldRole, self.verticalSpacer_4)


        self.verticalLayout_3.addLayout(self.formLayout_2)


        self.horizontalLayout.addWidget(self.groupBox_2)

        self.groupBox = QGroupBox(SettingDialog)
        self.groupBox.setObjectName(u"groupBox")
        sizePolicy.setHeightForWidth(self.groupBox.sizePolicy().hasHeightForWidth())
        self.groupBox.setSizePolicy(sizePolicy)
        self.groupBox.setMinimumSize(QSize(0, 0))
        self.groupBox.setMaximumSize(QSize(16777215, 199))
        self.groupBox.setSizeIncrement(QSize(0, 200))
        self.groupBox.setFont(font)
        self.groupBox.setFocusPolicy(Qt.NoFocus)
        self.verticalLayout_2 = QVBoxLayout(self.groupBox)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.formLayout = QFormLayout()
        self.formLayout.setObjectName(u"formLayout")
        self.label = QLabel(self.groupBox)
        self.label.setObjectName(u"label")
        self.label.setFont(font)

        self.formLayout.setWidget(0, QFormLayout.LabelRole, self.label)

        self.txtDirExcelFile = QLineEdit(self.groupBox)
        self.txtDirExcelFile.setObjectName(u"txtDirExcelFile")

        self.formLayout.setWidget(0, QFormLayout.FieldRole, self.txtDirExcelFile)

        self.label_10 = QLabel(self.groupBox)
        self.label_10.setObjectName(u"label_10")
        self.label_10.setFont(font)

        self.formLayout.setWidget(1, QFormLayout.LabelRole, self.label_10)

        self.txtDefineJsonFile = QLineEdit(self.groupBox)
        self.txtDefineJsonFile.setObjectName(u"txtDefineJsonFile")

        self.formLayout.setWidget(1, QFormLayout.FieldRole, self.txtDefineJsonFile)

        self.label_2 = QLabel(self.groupBox)
        self.label_2.setObjectName(u"label_2")
        self.label_2.setFont(font)

        self.formLayout.setWidget(2, QFormLayout.LabelRole, self.label_2)

        self.txtDirJsonFile = QLineEdit(self.groupBox)
        self.txtDirJsonFile.setObjectName(u"txtDirJsonFile")

        self.formLayout.setWidget(2, QFormLayout.FieldRole, self.txtDirJsonFile)

        self.label_6 = QLabel(self.groupBox)
        self.label_6.setObjectName(u"label_6")
        self.label_6.setFont(font)
        self.label_6.setFocusPolicy(Qt.ClickFocus)

        self.formLayout.setWidget(3, QFormLayout.LabelRole, self.label_6)

        self.txtDirJavaFile = QLineEdit(self.groupBox)
        self.txtDirJavaFile.setObjectName(u"txtDirJavaFile")

        self.formLayout.setWidget(3, QFormLayout.FieldRole, self.txtDirJavaFile)

        self.label_7 = QLabel(self.groupBox)
        self.label_7.setObjectName(u"label_7")
        self.label_7.setFont(font)

        self.formLayout.setWidget(4, QFormLayout.LabelRole, self.label_7)

        self.txtDirLayaFile = QLineEdit(self.groupBox)
        self.txtDirLayaFile.setObjectName(u"txtDirLayaFile")

        self.formLayout.setWidget(4, QFormLayout.FieldRole, self.txtDirLayaFile)


        self.verticalLayout_2.addLayout(self.formLayout)


        self.horizontalLayout.addWidget(self.groupBox)

        self.horizontalLayout.setStretch(0, 3)
        self.horizontalLayout.setStretch(1, 5)

        self.verticalLayout_4.addLayout(self.horizontalLayout)

        self.horizontalLayout_6 = QHBoxLayout()
        self.horizontalLayout_6.setObjectName(u"horizontalLayout_6")
        self.groupBox_3 = QGroupBox(SettingDialog)
        self.groupBox_3.setObjectName(u"groupBox_3")
        sizePolicy1 = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Expanding)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.groupBox_3.sizePolicy().hasHeightForWidth())
        self.groupBox_3.setSizePolicy(sizePolicy1)
        self.groupBox_3.setFont(font)
        self.verticalLayout_7 = QVBoxLayout(self.groupBox_3)
        self.verticalLayout_7.setObjectName(u"verticalLayout_7")
        self.formLayout_6 = QFormLayout()
        self.formLayout_6.setObjectName(u"formLayout_6")
        self.label_16 = QLabel(self.groupBox_3)
        self.label_16.setObjectName(u"label_16")
        sizePolicy2 = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Preferred)
        sizePolicy2.setHorizontalStretch(120)
        sizePolicy2.setVerticalStretch(0)
        sizePolicy2.setHeightForWidth(self.label_16.sizePolicy().hasHeightForWidth())
        self.label_16.setSizePolicy(sizePolicy2)
        self.label_16.setFont(font)
        self.label_16.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.formLayout_6.setWidget(2, QFormLayout.LabelRole, self.label_16)

        self.txtLibPrefix = QLineEdit(self.groupBox_3)
        self.txtLibPrefix.setObjectName(u"txtLibPrefix")

        self.formLayout_6.setWidget(2, QFormLayout.FieldRole, self.txtLibPrefix)

        self.txtLibEasyAccessClass = QLineEdit(self.groupBox_3)
        self.txtLibEasyAccessClass.setObjectName(u"txtLibEasyAccessClass")

        self.formLayout_6.setWidget(1, QFormLayout.FieldRole, self.txtLibEasyAccessClass)

        self.label_17 = QLabel(self.groupBox_3)
        self.label_17.setObjectName(u"label_17")
        sizePolicy2.setHeightForWidth(self.label_17.sizePolicy().hasHeightForWidth())
        self.label_17.setSizePolicy(sizePolicy2)
        self.label_17.setFont(font)
        self.label_17.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.formLayout_6.setWidget(1, QFormLayout.LabelRole, self.label_17)

        self.label_12 = QLabel(self.groupBox_3)
        self.label_12.setObjectName(u"label_12")
        sizePolicy2.setHeightForWidth(self.label_12.sizePolicy().hasHeightForWidth())
        self.label_12.setSizePolicy(sizePolicy2)
        self.label_12.setFont(font)
        self.label_12.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.formLayout_6.setWidget(0, QFormLayout.LabelRole, self.label_12)

        self.txtLibConstantClass = QLineEdit(self.groupBox_3)
        self.txtLibConstantClass.setObjectName(u"txtLibConstantClass")

        self.formLayout_6.setWidget(0, QFormLayout.FieldRole, self.txtLibConstantClass)

        self.chkAutoOpenFolderGenJson = QCheckBox(self.groupBox_3)
        self.chkAutoOpenFolderGenJson.setObjectName(u"chkAutoOpenFolderGenJson")
        self.chkAutoOpenFolderGenJson.setFont(font)

        self.formLayout_6.setWidget(3, QFormLayout.FieldRole, self.chkAutoOpenFolderGenJson)

        self.chkAutoOpenFolderCopy = QCheckBox(self.groupBox_3)
        self.chkAutoOpenFolderCopy.setObjectName(u"chkAutoOpenFolderCopy")
        self.chkAutoOpenFolderCopy.setFont(font)

        self.formLayout_6.setWidget(4, QFormLayout.FieldRole, self.chkAutoOpenFolderCopy)

        self.verticalSpacer_3 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.formLayout_6.setItem(6, QFormLayout.FieldRole, self.verticalSpacer_3)

        self.txtZipFile = QLineEdit(self.groupBox_3)
        self.txtZipFile.setObjectName(u"txtZipFile")

        self.formLayout_6.setWidget(5, QFormLayout.FieldRole, self.txtZipFile)

        self.label_11 = QLabel(self.groupBox_3)
        self.label_11.setObjectName(u"label_11")
        sizePolicy3 = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Preferred)
        sizePolicy3.setHorizontalStretch(0)
        sizePolicy3.setVerticalStretch(0)
        sizePolicy3.setHeightForWidth(self.label_11.sizePolicy().hasHeightForWidth())
        self.label_11.setSizePolicy(sizePolicy3)
        self.label_11.setMinimumSize(QSize(120, 0))
        self.label_11.setSizeIncrement(QSize(0, 0))
        self.label_11.setBaseSize(QSize(0, 0))
        self.label_11.setLayoutDirection(Qt.LeftToRight)
        self.label_11.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.formLayout_6.setWidget(5, QFormLayout.LabelRole, self.label_11)


        self.verticalLayout_7.addLayout(self.formLayout_6)


        self.horizontalLayout_6.addWidget(self.groupBox_3)

        self.groupBox_5 = QGroupBox(SettingDialog)
        self.groupBox_5.setObjectName(u"groupBox_5")
        sizePolicy1.setHeightForWidth(self.groupBox_5.sizePolicy().hasHeightForWidth())
        self.groupBox_5.setSizePolicy(sizePolicy1)
        self.groupBox_5.setFont(font)
        self.verticalLayout_6 = QVBoxLayout(self.groupBox_5)
        self.verticalLayout_6.setObjectName(u"verticalLayout_6")
        self.horizontalLayout_7 = QHBoxLayout()
        self.horizontalLayout_7.setSpacing(2)
        self.horizontalLayout_7.setObjectName(u"horizontalLayout_7")
        self.listCopyDir = QTableWidget(self.groupBox_5)
        self.listCopyDir.setObjectName(u"listCopyDir")
        sizePolicy4 = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        sizePolicy4.setHorizontalStretch(7)
        sizePolicy4.setVerticalStretch(0)
        sizePolicy4.setHeightForWidth(self.listCopyDir.sizePolicy().hasHeightForWidth())
        self.listCopyDir.setSizePolicy(sizePolicy4)
        self.listCopyDir.setMinimumSize(QSize(200, 0))
        self.listCopyDir.setSizeIncrement(QSize(800, 0))
        self.listCopyDir.setBaseSize(QSize(800, 0))
        self.listCopyDir.setToolTipDuration(0)
        self.listCopyDir.setFrameShape(QFrame.WinPanel)
        self.listCopyDir.setAutoScroll(False)
        self.listCopyDir.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.listCopyDir.setSelectionMode(QAbstractItemView.SingleSelection)
        self.listCopyDir.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.listCopyDir.setColumnCount(0)
        self.listCopyDir.horizontalHeader().setVisible(True)
        self.listCopyDir.horizontalHeader().setMinimumSectionSize(30)
        self.listCopyDir.verticalHeader().setVisible(False)
        self.listCopyDir.verticalHeader().setMinimumSectionSize(24)
        self.listCopyDir.verticalHeader().setDefaultSectionSize(24)

        self.horizontalLayout_7.addWidget(self.listCopyDir)

        self.verticalLayout_10 = QVBoxLayout()
        self.verticalLayout_10.setSpacing(3)
        self.verticalLayout_10.setObjectName(u"verticalLayout_10")
        self.verticalLayout_10.setContentsMargins(2, -1, -1, -1)
        self.btnMoveUp1 = QPushButton(self.groupBox_5)
        self.btnMoveUp1.setObjectName(u"btnMoveUp1")
        sizePolicy5 = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Expanding)
        sizePolicy5.setHorizontalStretch(0)
        sizePolicy5.setVerticalStretch(0)
        sizePolicy5.setHeightForWidth(self.btnMoveUp1.sizePolicy().hasHeightForWidth())
        self.btnMoveUp1.setSizePolicy(sizePolicy5)
        self.btnMoveUp1.setMinimumSize(QSize(0, 0))
        self.btnMoveUp1.setMaximumSize(QSize(20, 16777215))
        font1 = QFont()
        font1.setFamily(u"Calibri")
        self.btnMoveUp1.setFont(font1)

        self.verticalLayout_10.addWidget(self.btnMoveUp1)

        self.btnMoveDown1 = QPushButton(self.groupBox_5)
        self.btnMoveDown1.setObjectName(u"btnMoveDown1")
        sizePolicy5.setHeightForWidth(self.btnMoveDown1.sizePolicy().hasHeightForWidth())
        self.btnMoveDown1.setSizePolicy(sizePolicy5)
        self.btnMoveDown1.setMinimumSize(QSize(0, 0))
        self.btnMoveDown1.setMaximumSize(QSize(20, 16777215))
        self.btnMoveDown1.setFont(font1)

        self.verticalLayout_10.addWidget(self.btnMoveDown1)


        self.horizontalLayout_7.addLayout(self.verticalLayout_10)

        self.verticalLayout = QVBoxLayout()
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.verticalLayout.setContentsMargins(2, 0, 0, 0)
        self.btnAddCopyDir = QPushButton(self.groupBox_5)
        self.btnAddCopyDir.setObjectName(u"btnAddCopyDir")
        self.btnAddCopyDir.setFont(font)

        self.verticalLayout.addWidget(self.btnAddCopyDir)

        self.btnDelCopyDir = QPushButton(self.groupBox_5)
        self.btnDelCopyDir.setObjectName(u"btnDelCopyDir")
        self.btnDelCopyDir.setFont(font)

        self.verticalLayout.addWidget(self.btnDelCopyDir)

        self.btnClearCopyDir = QPushButton(self.groupBox_5)
        self.btnClearCopyDir.setObjectName(u"btnClearCopyDir")
        self.btnClearCopyDir.setFont(font)

        self.verticalLayout.addWidget(self.btnClearCopyDir)

        self.verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout.addItem(self.verticalSpacer)


        self.horizontalLayout_7.addLayout(self.verticalLayout)


        self.verticalLayout_6.addLayout(self.horizontalLayout_7)

        self.gridLayout_2 = QGridLayout()
        self.gridLayout_2.setObjectName(u"gridLayout_2")
        self.txtDirJavaFileCopy = QLineEdit(self.groupBox_5)
        self.txtDirJavaFileCopy.setObjectName(u"txtDirJavaFileCopy")
        self.txtDirJavaFileCopy.setReadOnly(True)

        self.gridLayout_2.addWidget(self.txtDirJavaFileCopy, 1, 1, 1, 1)

        self.txtDirLayaFileCopy = QLineEdit(self.groupBox_5)
        self.txtDirLayaFileCopy.setObjectName(u"txtDirLayaFileCopy")

        self.gridLayout_2.addWidget(self.txtDirLayaFileCopy, 3, 1, 1, 1)

        self.btnSelectDirJavaFileCopy = QPushButton(self.groupBox_5)
        self.btnSelectDirJavaFileCopy.setObjectName(u"btnSelectDirJavaFileCopy")
        self.btnSelectDirJavaFileCopy.setMaximumSize(QSize(75, 16777215))
        self.btnSelectDirJavaFileCopy.setFont(font)

        self.gridLayout_2.addWidget(self.btnSelectDirJavaFileCopy, 1, 3, 1, 1)

        self.btnSelectDirLayaFileCopy = QPushButton(self.groupBox_5)
        self.btnSelectDirLayaFileCopy.setObjectName(u"btnSelectDirLayaFileCopy")
        self.btnSelectDirLayaFileCopy.setMaximumSize(QSize(75, 16777215))
        self.btnSelectDirLayaFileCopy.setFont(font)

        self.gridLayout_2.addWidget(self.btnSelectDirLayaFileCopy, 3, 3, 1, 1)

        self.label_15 = QLabel(self.groupBox_5)
        self.label_15.setObjectName(u"label_15")
        self.label_15.setMinimumSize(QSize(120, 0))
        self.label_15.setFont(font)
        self.label_15.setAlignment(Qt.AlignLeading|Qt.AlignLeft|Qt.AlignVCenter)

        self.gridLayout_2.addWidget(self.label_15, 3, 0, 1, 1)

        self.label_8 = QLabel(self.groupBox_5)
        self.label_8.setObjectName(u"label_8")
        self.label_8.setMinimumSize(QSize(120, 0))
        self.label_8.setFont(font)
        self.label_8.setAlignment(Qt.AlignLeading|Qt.AlignLeft|Qt.AlignVCenter)

        self.gridLayout_2.addWidget(self.label_8, 1, 0, 1, 1)


        self.verticalLayout_6.addLayout(self.gridLayout_2)


        self.horizontalLayout_6.addWidget(self.groupBox_5)

        self.horizontalLayout_6.setStretch(0, 3)
        self.horizontalLayout_6.setStretch(1, 5)

        self.verticalLayout_4.addLayout(self.horizontalLayout_6)

        self.groupBox_6 = QGroupBox(SettingDialog)
        self.groupBox_6.setObjectName(u"groupBox_6")
        self.groupBox_6.setMinimumSize(QSize(0, 0))
        self.groupBox_6.setFont(font)
        self.verticalLayout_9 = QVBoxLayout(self.groupBox_6)
        self.verticalLayout_9.setObjectName(u"verticalLayout_9")
        self.gridLayout_3 = QGridLayout()
        self.gridLayout_3.setObjectName(u"gridLayout_3")
        self.gridLayout_3.setHorizontalSpacing(5)
        self.txtSvnPwd = QLineEdit(self.groupBox_6)
        self.txtSvnPwd.setObjectName(u"txtSvnPwd")
        self.txtSvnPwd.setMaxLength(30)
        self.txtSvnPwd.setEchoMode(QLineEdit.PasswordEchoOnEdit)

        self.gridLayout_3.addWidget(self.txtSvnPwd, 1, 6, 1, 1)

        self.label_14 = QLabel(self.groupBox_6)
        self.label_14.setObjectName(u"label_14")
        self.label_14.setFont(font)

        self.gridLayout_3.addWidget(self.label_14, 1, 3, 1, 1)

        self.txtSvnUser = QLineEdit(self.groupBox_6)
        self.txtSvnUser.setObjectName(u"txtSvnUser")
        self.txtSvnUser.setMaxLength(30)

        self.gridLayout_3.addWidget(self.txtSvnUser, 1, 4, 1, 1)

        self.btnCheckSvnAddr = QPushButton(self.groupBox_6)
        self.btnCheckSvnAddr.setObjectName(u"btnCheckSvnAddr")
        self.btnCheckSvnAddr.setMaximumSize(QSize(75, 16777215))
        self.btnCheckSvnAddr.setFont(font)

        self.gridLayout_3.addWidget(self.btnCheckSvnAddr, 1, 7, 1, 1)

        self.txtSvnUrl = QLineEdit(self.groupBox_6)
        self.txtSvnUrl.setObjectName(u"txtSvnUrl")
        self.txtSvnUrl.setEnabled(True)
        self.txtSvnUrl.setMinimumSize(QSize(600, 0))

        self.gridLayout_3.addWidget(self.txtSvnUrl, 1, 1, 1, 1)

        self.label_13 = QLabel(self.groupBox_6)
        self.label_13.setObjectName(u"label_13")
        self.label_13.setFont(font)

        self.gridLayout_3.addWidget(self.label_13, 1, 5, 1, 1)

        self.label_20 = QLabel(self.groupBox_6)
        self.label_20.setObjectName(u"label_20")
        self.label_20.setEnabled(True)
        self.label_20.setMinimumSize(QSize(120, 0))
        self.label_20.setSizeIncrement(QSize(0, 0))
        self.label_20.setBaseSize(QSize(0, 0))
        self.label_20.setFont(font)

        self.gridLayout_3.addWidget(self.label_20, 1, 0, 1, 1)


        self.verticalLayout_9.addLayout(self.gridLayout_3)


        self.verticalLayout_4.addWidget(self.groupBox_6)

        self.groupBox_4 = QGroupBox(SettingDialog)
        self.groupBox_4.setObjectName(u"groupBox_4")
        sizePolicy1.setHeightForWidth(self.groupBox_4.sizePolicy().hasHeightForWidth())
        self.groupBox_4.setSizePolicy(sizePolicy1)
        self.verticalLayout_5 = QVBoxLayout(self.groupBox_4)
        self.verticalLayout_5.setObjectName(u"verticalLayout_5")
        self.label_5 = QLabel(self.groupBox_4)
        self.label_5.setObjectName(u"label_5")
        self.label_5.setMinimumSize(QSize(120, 0))
        self.label_5.setFont(font)
        self.label_5.setAlignment(Qt.AlignCenter)

        self.verticalLayout_5.addWidget(self.label_5)

        self.horizontalLayout_3 = QHBoxLayout()
        self.horizontalLayout_3.setSpacing(2)
        self.horizontalLayout_3.setObjectName(u"horizontalLayout_3")
        self.listServer = QTableWidget(self.groupBox_4)
        self.listServer.setObjectName(u"listServer")
        sizePolicy4.setHeightForWidth(self.listServer.sizePolicy().hasHeightForWidth())
        self.listServer.setSizePolicy(sizePolicy4)
        self.listServer.setMinimumSize(QSize(200, 0))
        self.listServer.setBaseSize(QSize(800, 0))
        self.listServer.setEditTriggers(QAbstractItemView.DoubleClicked)
        self.listServer.setSelectionMode(QAbstractItemView.SingleSelection)

        self.horizontalLayout_3.addWidget(self.listServer)

        self.verticalLayout_11 = QVBoxLayout()
        self.verticalLayout_11.setSpacing(3)
        self.verticalLayout_11.setObjectName(u"verticalLayout_11")
        self.verticalLayout_11.setContentsMargins(2, -1, -1, -1)
        self.btnMoveUp2 = QPushButton(self.groupBox_4)
        self.btnMoveUp2.setObjectName(u"btnMoveUp2")
        sizePolicy5.setHeightForWidth(self.btnMoveUp2.sizePolicy().hasHeightForWidth())
        self.btnMoveUp2.setSizePolicy(sizePolicy5)
        self.btnMoveUp2.setMaximumSize(QSize(20, 999))
        self.btnMoveUp2.setSizeIncrement(QSize(50, 0))
        self.btnMoveUp2.setFont(font1)

        self.verticalLayout_11.addWidget(self.btnMoveUp2)

        self.btnMoveDown2 = QPushButton(self.groupBox_4)
        self.btnMoveDown2.setObjectName(u"btnMoveDown2")
        sizePolicy5.setHeightForWidth(self.btnMoveDown2.sizePolicy().hasHeightForWidth())
        self.btnMoveDown2.setSizePolicy(sizePolicy5)
        self.btnMoveDown2.setMaximumSize(QSize(20, 999))
        self.btnMoveDown2.setFont(font1)

        self.verticalLayout_11.addWidget(self.btnMoveDown2)


        self.horizontalLayout_3.addLayout(self.verticalLayout_11)

        self.verticalLayout_8 = QVBoxLayout()
        self.verticalLayout_8.setObjectName(u"verticalLayout_8")
        self.verticalLayout_8.setContentsMargins(2, 0, 0, 0)
        self.btnAddServer = QPushButton(self.groupBox_4)
        self.btnAddServer.setObjectName(u"btnAddServer")
        self.btnAddServer.setFont(font)

        self.verticalLayout_8.addWidget(self.btnAddServer)

        self.btnDelServer = QPushButton(self.groupBox_4)
        self.btnDelServer.setObjectName(u"btnDelServer")
        self.btnDelServer.setFont(font)

        self.verticalLayout_8.addWidget(self.btnDelServer)

        self.btnClearServer = QPushButton(self.groupBox_4)
        self.btnClearServer.setObjectName(u"btnClearServer")
        self.btnClearServer.setFont(font)

        self.verticalLayout_8.addWidget(self.btnClearServer)

        self.verticalSpacer_2 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_8.addItem(self.verticalSpacer_2)

        self.btnImport = QPushButton(self.groupBox_4)
        self.btnImport.setObjectName(u"btnImport")
        self.btnImport.setFont(font)

        self.verticalLayout_8.addWidget(self.btnImport)

        self.btnExport = QPushButton(self.groupBox_4)
        self.btnExport.setObjectName(u"btnExport")
        self.btnExport.setFont(font)

        self.verticalLayout_8.addWidget(self.btnExport)


        self.horizontalLayout_3.addLayout(self.verticalLayout_8)


        self.verticalLayout_5.addLayout(self.horizontalLayout_3)


        self.verticalLayout_4.addWidget(self.groupBox_4)

        self.widget = QWidget(SettingDialog)
        self.widget.setObjectName(u"widget")
        self.widget.setMinimumSize(QSize(0, 50))
        self.horizontalLayout_2 = QHBoxLayout(self.widget)
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.btnDefault = QPushButton(self.widget)
        self.btnDefault.setObjectName(u"btnDefault")
        self.btnDefault.setFont(font)

        self.horizontalLayout_2.addWidget(self.btnDefault)

        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_2.addItem(self.horizontalSpacer)

        self.btnClose = QPushButton(self.widget)
        self.btnClose.setObjectName(u"btnClose")
        self.btnClose.setFont(font)

        self.horizontalLayout_2.addWidget(self.btnClose)

        self.btnOk = QPushButton(self.widget)
        self.btnOk.setObjectName(u"btnOk")
        self.btnOk.setFont(font)

        self.horizontalLayout_2.addWidget(self.btnOk)


        self.verticalLayout_4.addWidget(self.widget)


        self.retranslateUi(SettingDialog)

        QMetaObject.connectSlotsByName(SettingDialog)
    # setupUi

    def retranslateUi(self, SettingDialog):
        SettingDialog.setWindowTitle(QCoreApplication.translate("SettingDialog", u"\u8bbe\u5b9a", None))
        self.groupBox_2.setTitle(QCoreApplication.translate("SettingDialog", u"\u57fa\u7840\u914d\u7f6e", None))
        self.label_3.setText(QCoreApplication.translate("SettingDialog", u"\u914d\u7f6e\u9875Sheet\u540d\uff1a", None))
        self.label_9.setText(QCoreApplication.translate("SettingDialog", u"TAG-\u5bfc\u5165\u8868\u540d:", None))
#if QT_CONFIG(tooltip)
        self.label_4.setToolTip("")
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(statustip)
        self.label_4.setStatusTip("")
#endif // QT_CONFIG(statustip)
#if QT_CONFIG(whatsthis)
        self.label_4.setWhatsThis("")
#endif // QT_CONFIG(whatsthis)
#if QT_CONFIG(accessibility)
        self.label_4.setAccessibleDescription("")
#endif // QT_CONFIG(accessibility)
        self.label_4.setText(QCoreApplication.translate("SettingDialog", u"\u4e0d\u5bfc\u51fa\u6807\u8bc6\uff1a", None))
#if QT_CONFIG(tooltip)
        self.txtCfgIgnoreColumn.setToolTip("")
#endif // QT_CONFIG(tooltip)
        self.groupBox.setTitle(QCoreApplication.translate("SettingDialog", u"\u6587\u4ef6(\u8bfb\u53d6/\u751f\u6210)\u76ee\u5f55\u914d\u7f6e", None))
        self.label.setText(QCoreApplication.translate("SettingDialog", u"Excel\u9ed8\u8ba4\u76ee\u5f55\uff1a", None))
        self.label_10.setText(QCoreApplication.translate("SettingDialog", u"Json\u5b9a\u4e49\u8868\u6587\u4ef6\u540d\uff1a", None))
        self.label_2.setText(QCoreApplication.translate("SettingDialog", u"\u5bfc\u51faJson\u76ee\u5f55\uff1a", None))
        self.label_6.setText(QCoreApplication.translate("SettingDialog", u"Java\u4ee3\u7801\u751f\u6210\u76ee\u5f55\uff1a", None))
        self.label_7.setText(QCoreApplication.translate("SettingDialog", u"Laya\u4ee3\u7801\u751f\u6210\u76ee\u5f55\uff1a", None))
        self.groupBox_3.setTitle(QCoreApplication.translate("SettingDialog", u"\u89e3\u6790\u914d\u7f6e", None))
        self.label_16.setText(QCoreApplication.translate("SettingDialog", u"\u5b9e\u4f53\u7c7b\u9644\u52a0\u5b57\u5934\uff1a", None))
        self.label_17.setText(QCoreApplication.translate("SettingDialog", u"\u4fbf\u6377\u52a0\u8f7d\u7c7b\u7c7b\u540d\uff1a", None))
        self.label_12.setText(QCoreApplication.translate("SettingDialog", u"\u751f\u6210\u5e38\u91cf\u8868\u7c7b\u540d\uff1a", None))
        self.chkAutoOpenFolderGenJson.setText(QCoreApplication.translate("SettingDialog", u"\u751f\u6210Json\u6570\u636e\u540e\u81ea\u52a8\u6253\u5f00\u76ee\u5f55", None))
        self.chkAutoOpenFolderCopy.setText(QCoreApplication.translate("SettingDialog", u"\u540c\u6b65\u62f7\u8d1d\u6570\u636e\u540e\u81ea\u52a8\u6253\u5f00\u76ee\u5f55", None))
        self.label_11.setText(QCoreApplication.translate("SettingDialog", u"\u538b\u7f29\u6587\u4ef6\u540d\uff1a", None))
        self.groupBox_5.setTitle(QCoreApplication.translate("SettingDialog", u"\u540c\u6b65\u6570\u636e\u76ee\u5f55", None))
        self.btnMoveUp1.setText(QCoreApplication.translate("SettingDialog", u"\u2191", None))
        self.btnMoveDown1.setText(QCoreApplication.translate("SettingDialog", u"\u2193", None))
        self.btnAddCopyDir.setText(QCoreApplication.translate("SettingDialog", u"\u6dfb\u52a0\u65b0\u76ee\u5f55", None))
        self.btnDelCopyDir.setText(QCoreApplication.translate("SettingDialog", u"\u5220\u9664\u9009\u4e2d", None))
        self.btnClearCopyDir.setText(QCoreApplication.translate("SettingDialog", u"\u6e05\u9664\u6240\u6709", None))
        self.btnSelectDirJavaFileCopy.setText(QCoreApplication.translate("SettingDialog", u"\u9009\u62e9\u76ee\u5f55", None))
        self.btnSelectDirLayaFileCopy.setText(QCoreApplication.translate("SettingDialog", u"\u9009\u62e9\u76ee\u5f55", None))
        self.label_15.setText(QCoreApplication.translate("SettingDialog", u"Laya\u540c\u6b65\u62f7\u8d1d\u76ee\u5f55\uff1a", None))
        self.label_8.setText(QCoreApplication.translate("SettingDialog", u"Java\u540c\u6b65\u62f7\u8d1d\u76ee\u5f55\uff1a", None))
        self.groupBox_6.setTitle(QCoreApplication.translate("SettingDialog", u"SVN\u914d\u7f6e", None))
        self.txtSvnPwd.setText(QCoreApplication.translate("SettingDialog", u"12345", None))
        self.label_14.setText(QCoreApplication.translate("SettingDialog", u"\u8d26\u53f7\uff1a", None))
        self.txtSvnUser.setText(QCoreApplication.translate("SettingDialog", u"user", None))
        self.btnCheckSvnAddr.setText(QCoreApplication.translate("SettingDialog", u"\u68c0\u67e5\u5730\u5740", None))
        self.label_13.setText(QCoreApplication.translate("SettingDialog", u"\u5bc6\u7801\uff1a", None))
        self.label_20.setText(QCoreApplication.translate("SettingDialog", u"SVN\u670d\u52a1\u5668\u5730\u5740\uff1a", None))
        self.groupBox_4.setTitle(QCoreApplication.translate("SettingDialog", u"\u670d\u52a1\u5668\u914d\u7f6e", None))
        self.label_5.setText(QCoreApplication.translate("SettingDialog", u"-= \u53d1\u5e03\u670d\u52a1\u5668\u5730\u5740\u914d\u7f6e\u8868 =-                              ", None))
        self.btnMoveUp2.setText(QCoreApplication.translate("SettingDialog", u"\u2191", None))
        self.btnMoveDown2.setText(QCoreApplication.translate("SettingDialog", u"\u2193", None))
        self.btnAddServer.setText(QCoreApplication.translate("SettingDialog", u"\u6dfb\u52a0\u53d1\u5e03\u670d\u52a1\u5668", None))
        self.btnDelServer.setText(QCoreApplication.translate("SettingDialog", u"\u5220\u9664\u9009\u4e2d", None))
        self.btnClearServer.setText(QCoreApplication.translate("SettingDialog", u"\u6e05\u9664\u6240\u6709", None))
        self.btnImport.setText(QCoreApplication.translate("SettingDialog", u"\u5bfc\u5165", None))
        self.btnExport.setText(QCoreApplication.translate("SettingDialog", u"\u5bfc\u51fa", None))
        self.btnDefault.setText(QCoreApplication.translate("SettingDialog", u"\u6062\u590d\u9ed8\u8ba4\u503c", None))
        self.btnClose.setText(QCoreApplication.translate("SettingDialog", u"\u53d6\u6d88", None))
        self.btnOk.setText(QCoreApplication.translate("SettingDialog", u"\u4fdd\u5b58", None))
    # retranslateUi

